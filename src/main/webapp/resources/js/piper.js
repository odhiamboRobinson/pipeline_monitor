var dockID, graphData;
function graphPlotter() {
    var dt = JSON.parse(this.responseText);
        var chart = new CanvasJS.Chart("chartContainer", {
            theme: "theme1",
            title:{
                text: dt.legendText
            },
            animationEnabled: true,   // change to true
            data:dt
        });
        chart.render();
}

//graph plotter dynamic function
//variable/argument definition
var graphUrlPath, postParams, dockingGraph;

//logic execution
function genPlotter(graphUrlPath, postParams, dockingGraph) {
    graphPlotter(dockingGraph, passData(graphUrlPath, postParams))
}
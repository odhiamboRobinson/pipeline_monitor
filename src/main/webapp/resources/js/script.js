//docking the main content
var dockC = '';
function dockContent(dockC){
    $.ajax({
        url:dockC,
        type:"GET",
        complete: function(xhr, status){
            $('.loadingOverlay').fadeOut('slow');
            $('.dockContent').animate({opacity:0.0},200).html(xhr.responseText).delay(400).animate({opacity:1.0},500);
        }
    });
}

//initialize routing variables
var dock = '/signin';

//on ready instance
$(document).ready(function(){
    $('#initDock').load(dock); //load the login form to the appropriate div
});

//route on initdock
function initDock (dock){
    $('#initDock').load(dock); //overlay sign up with login form to the appropriate div
}

//create overlay
var ov = false;

function ovLay(ov){
    if(ov == false){
        var struct = '';
    }else if(ov == true){
        var struct = "<div id = 'ovDock' class = 'ovDock' style = 'background:rgba(0,0,0,0.8); position:absolute; top:0; bottom:0; right:0; left:0; z-index:300000'>"
            + "</div>";
    }

    document.getElementById('overlayDock').innerHTML = struct;
}

//editing on overlay per report
var edForm, edUrlPath, dockForm, uniqueId, uniqueID;

function popEdit(edForm){
    ovLay(true);
    $('.ovDock').load(edForm);
}

//formDocking
var dockOf = '';

function getForm(dockOf){
    $('.miniNavDock').load(dockOf); //returns a form
}

function getStructure(dockOf){
    $('.miniNavDock').load(dockOf); //returns a html structure
}

/*function users() {
    dockContent('/user/view/table');
    userData();
}*/

//add new entity
var newEnt;
function newEntity(newEnt){    
    ovLay(true);
    $.post(
        '/view/addNew/mininav',
        {
            formPath:newEnt.toString()
        },
        function(data, status){
            //$('.ovDock').load(data);
            document.getElementById('ovDock').innerHTML = data;
        }
    );
}

//alfa posting script snippet
//function variables
var postLoc, formContent, respo;
function passData(postLoc, formContent){
    $.ajax({
      url:postLoc,
      type:"POST",
      data:JSON.stringify(formContent),
      contentType:"application/json; charset=utf-8",
      dataType:"json",
      beforeSend: function(){
          $('.loadingOverlay').css('visibility' , 'visible');
      },
      success: function(data, status){
        $('.loadingOverlay').css('visibility' , 'hidden');
        message = data.message;
        url = data.url;
        type = data.type;
        dockContent(url);
        popStatus(message, 'success');
      }
    });    
}


function call (getLoc,txt,handler){
    $.ajax({
        url:getLoc,
        type:"GET",
        beforeSend: function(){
            $('#loadingText').html(txt);
            $('.loadingOverlay').css('visibility' , 'visible');
        },
        success: function(data, status){
            $('.loadingOverlay').css('visibility' , 'hidden');
            handler.call(data);
        }
    });
}

//generic function for calling the Nav
var urlEdit, urlInfo;
function infoEnt(urlEdit, urlInfo){    
    ovLay(true);
    $.post(
        '/view/mininav',
        {
            urlEdit:urlEdit.toString(),
            urlInfo:urlInfo.toString()
        },
        function(data, status){
            //$('.ovDock').load(data);
            document.getElementById('ovDock').innerHTML = data;
        }
    );
}

//validation script
//declare input fields/parameters
var inputData, response, dType, styleID;
function validate(inputData, response, dType, styleID){

    //check if null
    if(inputData == null || inputData == ''){
        illAlert(styleID, 'red');
        alert('Appropriately Fill Out the : ' + response);
        //return false;
        throw '';
        //stop(true, true);

    }else{

        //return state of validation
        illAlert(styleID, 'grey');
        return inputData;

    }

}

//illuminate red on wrong input fields
function illAlert(styleID, bcolor){
    var stId = '#' + styleID;
    $(stId).css('border', '1px solid ' + bcolor);
    //$('.loadingOverlay').css('visibility' , 'visible');
}

//validate onchange function call
function valOnChange(ref){
    reContent = ref.value;
    if(reContent == null || reContent == '' || reContent.length < 2){
        alert('Appropriately fill out the highlighted entity');
        $(ref).css('border', '1px solid red');
    }else{
        $(ref).css('border', '1px solid grey');
    }
}


function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function callPost (getLoc,txt,param,handler){

    $.ajax({
        url:getLoc,
        type:"POST",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        data:JSON.stringify(param),
        beforeSend: function(){
            $('#loadingText').html(txt);
            $('.loadingOverlay').css('visibility' , 'visible');
        },
        complete: function(data, status){
            $('.loadingOverlay').css('visibility' , 'hidden');
            handler.call(data);
        }
    });
}

//initiating poping of action status
function popStatus(popMsg, decision){
    var execHead, execFoot;
    execHead = "<div class = 'alert alert-" + decision + "'>";
    execFoot = "</div>";

    if(decision == null){
        document.getElementById('statusBar').innerHTML = popMsg;
        //alert(popMsg);
    }else{
        document.getElementById('statusBar').innerHTML = execHead + popMsg + execFoot;
        setTimeout(function(){
            popStatus('');
        }, 3000);
    }
}
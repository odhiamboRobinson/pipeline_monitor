/**
 * Created by robinson on 2/5/16.
 */

function getContent (url) {
    $('#ajax-content').load(url);
}

function getUsers(element,url) {

    $.getq ('Queue', url, function(result){
        $('#ajax-content').html(result);
    });

    $.getq ('Queue', 'user/list', function(result){
        table = $('#usersTable').DataTable({
            destroy: true,
            data: result,
            columns: [
                { data: 'id'},
                { data: 'firstname' },
                { data: 'lastname' },
                { data: 'email' },
                { data: 'enabled' }
            ]
        })
    });

}

function getUserGroups(element,url) {

    $.getq ('Queue', url, function(result){
        $('#ajax-content').html(result);
    });

    $.getq ('Queue', 'user/list', function(result){
        table = $('#usersTable').DataTable({
            destroy: true,
            data: result,
            columns: [
                { data: 'id'},
                { data: 'firstname' },
                { data: 'lastname' },
                { data: 'email' },
                { data: 'enabled' }
            ]
        })
    });

}

function getUserGroupAuthority(element,url) {

    $.getq ('Queue', url, function(result){
        $('#ajax-content').html(result);
    });

    $.getq ('Queue', 'user/list', function(result){
        table = $('#usersTable').DataTable({
            destroy: true,
            data: result,
            columns: [
                { data: 'id'},
                { data: 'firstname' },
                { data: 'lastname' },
                { data: 'email' },
                { data: 'enabled' }
            ]
        })
    });

}


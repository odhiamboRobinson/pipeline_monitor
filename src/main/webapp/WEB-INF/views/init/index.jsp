<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp"/>
<body class="flat-blue">
<div class="app-container">
    <div class="row content-container">
        <nav class="navbar navbar-default navbar-fixed-top navbar-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-expand-toggle">
                        <i class="fa fa-bars icon"></i>
                    </button>
                    <ol class="breadcrumb navbar-breadcrumb">
                        <li class="active">${programName}</li>
                    </ol>
                    <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                        <i class="fa fa-th icon"></i>
                    </button>
                </div>
                <ul class="nav navbar-nav navbar-right controlNav">
                    <sec:authentication var="user" property="principal" />
                    <li class="dropdown profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">${user.username} <span class="caret"></span></a>
                        <ul class="dropdown-menu animated fadeInDown">
                            <li>
                                <div class="profile-info">
                                    <p></p>

                                    <div class="btn-group margin-bottom-2x" role="group">
                                        <button type="button" class="btn btn-default"><i class="fa fa-key"></i> Change password
                                        </button>&nbsp&nbsp&nbsp
                                        <a href="/j_spring_security_logout"><button type="button" class="btn btn-default"><i class="fa fa-sign-out"></i>
                                            Logout
                                        </button></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <c:if test="${programs == null}">
        <div class="side-menu sidebar-inverse">
            <nav class="navbar navbar-default" role="navigation">
                <div class="side-menu-container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">
                            <div class="icon fa fa-product-hunt"></div>
                            <div class="title">PMT</div>
                        </a>
                        <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                            <i class="fa fa-times icon"></i>
                        </button>
                    </div>
                    <ul class="nav navbar-nav controlNav">
                        <li onclick="location.reload()"> <!--  getUsers(this,'user/view/table') -->
                            <a>
                                <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                            </a>
                        </li>
                        <sec:authorize access="hasAnyRole('ROLE_VIEW_USER')">
                            <li onclick="$('#loadingText').html('loading users structure');dockContent('/user/view/table','loading users');"> <!--  getUsers(this,'user/view/table') -->
                                <a>
                                    <span class="icon fa fa-user"></span><span class="title">Users</span>
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_VIEW_USER_GROUP')">
                            <li onclick="$('#loadingText').html('loading user groups structure');dockContent('/group/view/table','loading user groups')"><!--  getUserGroups(this,'usergroup/view/table') -->
                                <a>
                                    <span class="icon fa fa-users"></span><span class="title">Groups</span>
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_VIEW_PROGRAM')">
                            <li onclick="$('#loadingText').html('loading programs structure');dockContent('program/view/table')">
                                <a>
                                    <span class="icon fa fa-puzzle-piece"></span><span class="title">Programs</span>
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_VIEW_PRODUCT')">
                            <li onclick="$('#loadingText').html('loading products structure');dockContent('/product/view/table');">
                                <a>
                                    <span class="icon fa fa-shopping-cart"></span><span class="title">Products</span>
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_VIEW_FORECAST')">
                            <li onclick="$('#loadingText').html('loading forecast structure');dockContent('forecast/view/table')">
                                <a>
                                    <span class="icon fa fa-hourglass"></span><span class="title">Forecast</span>
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_VIEW_SUPPLY_PLAN')">
                            <li onclick="$('#loadingText').html('loading supply plan structure');dockContent('supplyplan/view/table')">
                                <a>
                                    <span class="icon fa fa-archive"></span><span class="title">Supply plan</span>
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_VIEW_ANALYSIS')">
                            <li onclick="$('#loadingText').html('loading analytics structure');dockContent('analysis/view/composite')">
                                <a>
                                    <span class="icon fa fa-bar-chart"></span><span class="title">Analystics</span>
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_USER')">
                            <li onclick="$('#loadingText').html('loading documentation');dockContent('documentation/')">
                                <a>
                                    <span class="icon fa fa-book"></span><span class="title">Documentation</span>
                                </a>
                            </li>
                        </sec:authorize>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
        </div>
        </c:if>
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">
                <div class="row" id="ajax-content">
                    <div id = 'statusBar' class = 'col-md-12'>
                        <!-- <div class = 'alert alert-success'>Working well</div> -->
                    </div>
                    <c:if test="${programs == null}">
                        <div class = 'dockContent'><jsp:include page="../init/dashboard.jsp"/></div>
                    </c:if>
                    <c:if test="${programs != null}">
                        <div class = 'dockContent'>
                            <div class="col-md-6 col-md-offset-3 ">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-md-12 card-title">
                                            <div class="col-md-2 title">SELECT PROGRAM</div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                                <label>PLEASE SELECT A PROGRAM</label>
                                                <select class="form-control" id="program" onchange="setProgram()">
                                                    <option value=''>click to select a program</option>
                                                    <c:forEach items="${programs}" var="program">
                                                        <option value="${program.id}">${program.name}</option>
                                                    </c:forEach>
                                                </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
    <script>
        function setProgram() {
           var programId =  $("#program").val();
            if(programId != '') {
                $("#program").prop("disabled",true);
                $.post("program/activate",{programId: programId},function(data){window.location.reload();});
            }
        }
    </script>
    <!-- creating the overlay -->
    <div id = 'overlayDock'></div>
    <div class = 'loadingOverlay'>
        <div class="card summary-inline" style="background-color: #ec971f;">
            <div class="card-body">
                <img class="icon" src = '<c:url value="resources/img/logo/overlayLoader.gif"/>' >
                <span style="font-size: medium;color: white;padding-left:5px;" id="loadingText"></span>
                <div class="clear-both"></div>
            </div>
        </div>
    </div>
<jsp:include page="footer.jsp"/>
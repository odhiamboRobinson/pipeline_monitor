<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp"/>
<body class = 'container' id = 'home'>
<div class = 'docker'>
    <div class = 'col-xs-12 col-sm-6 col-md-6 col-xs-6 space space-logo'>
        <div class = 'logo'>
            <img src = '<c:url value="resources/img/logo/pmt.png"/>' class = '' />
        </div>
    </div>
    <div class = 'col-xs-12 col-sm-6 col-md-6 col-xs-6'>
        <div id = 'initDock'>
            <!-- loading content in initDock -->
            <img style = 'margin-top:37%; width:auto; height:auto; border-radius:10px;' src = '<c:url value = "resources/img/logo/loader.gif"/>' class = 'col-md-offset-4 col-md-4 col-md-offset-4' />
        </div>
        <div id = 'dockMsg' style = 'border-radius:2px; background:rgba(255,255,255,0.1); margin-top:90%' class = ''></div>
    </div>

</div>
<jsp:include page="footer.jsp"/>
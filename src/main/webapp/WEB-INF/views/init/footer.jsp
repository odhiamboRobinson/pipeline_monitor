<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- Javascript Libs -->
<script type="text/javascript" src='<c:url value="resources/js/jquery.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/bootstrap.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/bootstrap-switch.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/sol.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/spin.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/jquery.matchHeight-min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/jquery.dataTables.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/jquery.modal.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/dataTables.bootstrap.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/select2.full.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/editable.min.js"/>'></script>
<!-- Javascript -->
<script type="text/javascript" src='<c:url value="resources/js/app.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/pipeline.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/script.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/canvasjs.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="resources/js/piper.js"/>'></script>
<script>
    $(document).ajaxSend(function(e, xhr, opt){
        $('.loadingOverlay').fadeIn('slow');
    });
</script>
</body>

</html>
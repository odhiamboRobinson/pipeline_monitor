<%--
  Created by IntelliJ IDEA.
  User: robinson
  Date: 2/7/16
  Time: 2:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
  <title>PMT</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
  <!-- CSS Libs -->
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/bootstrap.min.css"/>' >
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/font-awesome.min.css"/>' >
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/animate.min.css"/>' >
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/bootstrap-switch.min.css"/>'>
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/jquery.dataTables.min.css"/>'>
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/dataTables.bootstrap.css"/>'>
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/select2.min.css"/>'>
  <link rel="stylesheet" type="text/css" href=<c:url value="resources/css/sol.css"/>>
  <!-- CSS App -->
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/style.css"/>' >
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/themes/flat-blue.css"/>'>
  <link rel="stylesheet" type="text/css" href='<c:url value="resources/css/pipeline.css"/>'>
  <link rel="stylesheet" type="text/css" href=<c:url value="resources/css/editable-table.css"/>>

  <style>
    .graphView{
      width:100%;
      height: 500px;
      background: rgba(255, 255, 255, 0.3);
      border-radius: 12px;
      margin: 3px;
    }
  </style>
</head>
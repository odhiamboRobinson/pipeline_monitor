<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class='col-md-6 space-login login'>
    <form action="/j_spring_security_check" method="post">
        <div class="form-group">
            <input type="text" class="form-control" name="j_username" id="username" placeholder="Enter Email">
        </div>

        <div class="form-group">
            <input type="password" class="form-control" name="j_password" id="password" placeholder="Enter Password">
        </div>
        <div class="col-md-12 form-group">
            <input type="submit" class="col-md-12 btn btn-warning" value='Sign In' >
        </div>
    </form>
    <span class="pull-right" style="color: white;font-size: medium;" onclick="dockContent('/changepassword/form')">forgot password ? </span>
</div>
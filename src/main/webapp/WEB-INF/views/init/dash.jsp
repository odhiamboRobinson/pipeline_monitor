    <div class = 'card'>
		<div class = 'card-header'>
			<div class = 'card-title'>
				<div class = 'title'>DashBoard</div>
			</div>
		</div>
		<div class = 'card-body'>
				<div class = 'col-md-12'>
					<div class = 'col-md-6 pull-left'>
						<div id = 'chartContain' class = 'graphView'></div>
					</div>
					<div class = 'col-md-6 pull-right'>
						<div id = 'chartContainer' class = 'graphView'></div>
					</div>
				</div>
		</div>
	</div>

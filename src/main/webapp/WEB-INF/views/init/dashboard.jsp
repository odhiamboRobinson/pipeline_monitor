<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div onclick="dockContent('supplyplan/view/table')" class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="card red summary-inline">
            <div class="card-body">
                <i class="icon fa fa-inbox fa-4x"></i>

                <div class="content">
                    <div class="title">${tiles.stockOut}</div>
                    <div class="sub-title">Out of Stock</div>
                </div>
                <div class="clear-both"></div>
            </div>
        </div>
    </div>
    <div onclick="dockContent('supplyplan/view/table')" class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="card yellow summary-inline">
            <div class="card-body">
                <i class="icon fa fa-inbox fa-4x"></i>

                <div class="content">
                    <div class="title">${tiles.belowMinMOS}</div>
                    <div class="sub-title">MOS Below Min Level</div>
                </div>
                <div class="clear-both"></div>
            </div>
        </div>
    </div>
    <div onclick="dockContent('supplyplan/view/table')" class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="card green summary-inline">
            <div class="card-body">
                <i class="icon fa fa-inbox fa-4x"></i>

                <div class="content">
                    <div class="title">${tiles.withinMinAndMaxMOS}</div>
                    <div class="sub-title">MOS within Min-Max Level</div>
                </div>
                <div class="clear-both"></div>
            </div>
        </div>
    </div>
    <div onclick="dockContent('supplyplan/view/table')" class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="card brown summary-inline">
            <div class="card-body">
                <i class="icon fa fa-inbox fa-4x"></i>

                <div class="content">
                    <div class="title">${tiles.aboveMaxMOS}</div>
                    <div class="sub-title">MOS Above Max Level</div>
                </div>
                <div class="clear-both"></div>
            </div>
        </div>
    </div>
</div>
<div class = 'row  col-md-12'>
    <jsp:include page="../init/dash.jsp"/>
</div>
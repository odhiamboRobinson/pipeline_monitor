<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: robinson
  Date: 2/17/16
  Time: 9:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>
<div>
<table class="table table-striped" style="background-color: #e7e4e4 ">
  <tr>
    <th colspan="4"> GENERAL INFO </th>
  </tr>
  <tr>
    <td>Surname    : </td><td>${user.sirName}</td>
    <td>Other names : </td><td>${user.otherNames}</td>
  </tr>
  <tr>
    <td>National ID : </td><td>${user.idNumber}</td>
    <td>E-mail : </td><td>${user.email}</td>
  </tr>
  <tr>
    <td>Phone number : </td><td>${user.phoneNumber}</td>
    <%--<td>Enabled : </td><td>${user.enabled}</td>--%>
  </tr>
  <tr>
    <th colspan="4"> USER GROUPS </th>
  </tr>
  <c:forEach items="${user.userGroups}" var="userGroup" >
    <tr>
      <td colspan="2">User Group </td> <td colspan="2"><c:out value="${userGroup.name}"/></td>
    </tr>
  </c:forEach>
  <tr>
    <th colspan="4"> USER ACCOUNT TRAIL </th>
  </tr>
  <tr>
    <td>Updated by    : </td><td>${updatedBy}</td>
    <c:set value="${user.updatedOn}" var="updatedOn" />
    <c:if test="${updatedOn ne null }" >
        <jsp:setProperty name="dateValue" property="time" value="${user.updatedOn.time}"/>
        <td>Updated On : </td><td><fmt:formatDate value="${dateValue}" pattern="dd/MM/yyyy HH:mm"/></td>
    </c:if>
     <c:if test="${updatedOn eq null }" >
          <td>Updated On : </td><td>N/A</td>
     </c:if>
  </tr>
  <tr>
    <td>Created by : </td><td>${createdBy}</td>
    <jsp:setProperty name="dateValue" property="time" value="${user.createdOn.time}"/>
    <td>Created On : </td><td><fmt:formatDate value="${dateValue}" pattern="dd/MM/yyyy HH:mm"/></td>
  </tr>
</table>
</div>
<div><button class = 'btn btn-warning' onclick = "getForm('user/${user.id}/delete');">Delete</button></div>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: robinson
  Date: 2/17/16
  Time: 9:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>
<div>
  <table class="table table-striped" style="background-color: #e7e4e4 ">
    <tr>
      <th colspan="4"> GENERAL INFO </th>
    </tr>
    <tr>
      <td>name    : </td><td>${product.name}</td>
    </tr>
    <tr>
      <td>MIN MOS : </td><td>${product.minimumMonthsOfStock}</td>
      <td>MAX MOS : </td><td>${product.maximumMonthsOfStock}</td>
    </tr>
    <tr>
      <th colspan="4"> PROGRAM TRAIL </th>
    </tr>
    <tr>
      <td>Updated by    : </td><td>${updatedBy}</td>
      <c:set value="${product.updatedOn}" var="updatedOn" />
      <c:if test="${updatedOn ne null }" >
        <jsp:setProperty name="dateValue" property="time" value="${product.updatedOn.time}"/>
        <td>Updated On : </td><td><fmt:formatDate value="${dateValue}" pattern="dd/MM/yyyy HH:mm"/></td>
      </c:if>
      <c:if test="${updatedOn eq null }" >
        <td>Updated On : </td><td>N/A</td>
      </c:if>
    </tr>
    <tr>
      <td>Created by : </td><td>${createdBy}</td>
      <jsp:setProperty name="dateValue" property="time" value="${product.createdOn.time}"/>
      <td>Created On : </td><td><fmt:formatDate value="${dateValue}" pattern="dd/MM/yyyy HH:mm"/></td>
    </tr>
  </table>
</div>
<div>
  <button class = 'btn btn-danger' onclick = "getForm('program/${product.id}/delete');">Delete</button>
</div>

<%--
  Created by IntelliJ IDEA.
  User: robinson
  Date: 3/8/16
  Time: 8:44 AM
  To change this template use File | Settings | File Templates.
--%>
<div>
  <div class="chartOptions col-xs-3 col-md-3 pull-left">
    <div class="card">
      <div class="card-header">

        <div class="col-md-12 card-title">
          <div class="col-md-12 title">Analysis | Chart Options</div>
        </div>
      </div>
      <div class="card-body col-md-12">
            <div class="form-group">
              <select class="form-control" id="chartType" onchange="if(this.value != ''){getChartOptions('analysis/view/'+this.value)}">
                <option value=''>Select A Chart</option>
                <option value="products/periods">MOS by product</option>
                <option value="stores/periods">MOS by central & peripheral stores</option>
                <option value="forecast/periods">MOS by Forecast error</option>
                <option value="products">MOS of product over time</option>
              </select>
            </div>
            <div id="selector">
            </div>
      </div>
    </div>
  </div>
  <div class="col-xs-8 col-md-8 pull-right">
    <div class="card">
      <div class="card-header">

        <div class="col-md-12 card-title">
          <div class="col-md-12 title">Analysis | Charts</div>
        </div>
      </div>
      <div class="card-body col-md-12">
        <div id = 'chartContainer' class = 'graphView'></div>
      </div>
    </div>
  </div>
</div>
<script>
  function getChartOptions(url) {
    $.ajax({
      url:url,
      type:"GET",
      complete: function(xhr, status){
        $('.loadingOverlay').fadeOut('slow');
        $('#selector').animate({opacity:0.0},200).html(xhr.responseText).delay(400).animate({opacity:1.0},500);
      }
    });
  }
</script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class = 'miniNav col-md-offset-3 col-md-6'>
    <ul class = 'nav nav-tabs'>
        <li data-toggle = 'tab'><a href="#Edit" onclick = "getForm('${urlEdit}');">Edit</a></li>
        <li data-toggle = 'tab'><a href="#Delete" onclick = "getStructure('${urlInfo}');">Info</a></li>
        <li data-toggle = 'tab'><a href="#Close" onclick = "ovLay(false);">Close</a></li>
    </ul>
    <div style = "margin:4%; background:rgba(255,255,255,0.9); border-radius:7px;" class = 'miniNavDock' id = 'miniNavDock'>
        <span>Use the navigation bar to access different options</span>
    </div>
</div>
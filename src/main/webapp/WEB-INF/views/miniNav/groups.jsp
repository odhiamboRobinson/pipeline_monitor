<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class = 'miniNav col-md-offset-3 col-md-6'>
    <ul class = 'nav nav-tabs'>
        <li data-toggle = 'tab'><a href="#Edit" onclick = "getForm('/group/view/${id}/edit/form');">Edit</a></li>
        <li data-toggle = 'tab'><a href="#Delete" onclick = "getStructure('/group/view/${id}/info');">Info</a></li>
        <li data-toggle = 'tab'><a href="#Close" onclick = "ovLay(false);"><!--<span class = 'glyphicon glyphicon-remove'></span>-->Close</a></li>
    </ul>
    <div style = "margin:4%; background:rgba(0,0,0,0.9); border-radius:7px;" class = 'miniNavDock'>
        <span>Use the navigation bar to access different options</span>
    </div>
</div>
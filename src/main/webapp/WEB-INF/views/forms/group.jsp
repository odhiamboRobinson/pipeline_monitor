<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: robinson
  Date: 2/25/16
  Time: 12:49 PM
  To change this template use File | Settings | File Templates.
--%>
<div class="col-xs-12">
    <div class="card">
        <div class="card-header">

            <div class="col-md-12 card-title">
                <div class="col-md-2 title">Groups</div>
                <div style = 'text-align:right;' class="col-md-10"><button onclick = "dockContent('/group/view/table');" class = 'btn btn-primary'>Back</button></div>
            </div>
        </div>
        <div class="card-body">
            <div class="col-md-12"><span id="dockMsg"></span></div>
            <div class = 'col-md-12'>
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="name" value="${userGroup.name}" placeholder="Enter name">
              </div>

              <div class="form-group col-md-6">
                <select class = 'form-control' id = 'enabled'>
                  <c:if test="${userGroup != null}">
                    <option value = "true" <c:if test="${userGroup.enabled eq true}">selected="selected" </c:if>>Enable</option>
                    <option value = "false" <c:if test="${userGroup.enabled eq false}">selected="selected" </c:if>>Disable</option>
                  </c:if>
                    <c:if test="${userGroup == null}">
                        <option value = "true" selected="selected">Enable</option>
                        <option value = "false" >Disable</option>
                    </c:if>
                </select>
              </div>

                <div class="col-md-12">
                    <table id = 'authID' border="1" class="table table-hover table-striped ">
                        <thead>
                            <tr><th colspan="4">PRIVILEGES</th></tr>
                        </thead>
                        <c:forEach items="${authorities}" var="authItem" varStatus="status">
                            <c:set var="idx" value="${status.index +1}"/>
                            <c:if test="${idx % 4 == 1}">
                                <tr>
                            </c:if>
                                <td><input type="checkbox" value="${authItem.id}" class="authorityIds" id="authItem${status.index}"
                                    <c:forEach items="${userGroup.authorities}" var="auth">
                                        <c:if test="${authItem.id == auth.id}"> checked</c:if>
                                    </c:forEach>
                                >${authItem.description}</td>
                            <c:if test="${idx % 4 == 0}">
                                </tr>
                            </c:if>
                            <c:set var="counter"  value="${status.count}"/>
                        </c:forEach>
                    </table>

              </div>
              <div class="col-md-12 form-group">
                <button class="col-md-offset-10 col-md-2 btn btn-primary" onclick="userGroup();" >Submit</button>
              </div>
            </div>
        </div>
    </div>
</div>
<script>
  function userGroup(){
    var url = '${route}';
    length = ${counter};
    var formContent= {};
    var choiceTest = null;
    formContent.authorityIds = [];

    for(var idx = 0;idx < length;idx++){
        if(typeof $('#authItem'+idx+":checked").val() != 'undefined') {
            choiceTest = 'select read';
            formContent.authorityIds.push(JSON.parse($('#authItem'+idx).val()));
        }
    }
    validate(choiceTest, 'Authorities', '', 'authID');
   formContent.name = validate(document.getElementById('name').value, 'Group Name', '', 'name');
   formContent.enabled = JSON.parse(document.getElementById('enabled').value);
   document.getElementById('dockMsg').innerHTML = passData(url, formContent);
  }


</script>

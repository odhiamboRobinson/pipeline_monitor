<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class = 'col-md-12'>

  <div class="form-group">
    <input type="text" class="form-control" id="name" value="${program.name}" placeholder="Enter Name">
  </div>

  <div class = 'row'>
    <div class="form-group col-md-6">
      <input type="text" class="form-control" id="minimumMonthsOfStock" value="${program.minimumMonthsOfStock}" placeholder="Enter Min MOS">
    </div>

    <div class="form-group col-md-6">
      <input type="text" class="form-control" id="maximumMonthsOfStock" value="${program.maximumMonthsOfStock}" placeholder="Enter Max MOS">
    </div>
  </div>
   <div class="form-group col-md-6">
      <select class = 'form-control' id = 'enabled'>
        <c:if test="${program != null}">
          <option value = "true" <c:if test="${program.enabled eq true}">selected="selected" </c:if>>Enable</option>
          <option value = "false" <c:if test="${program.enabled eq false}">selected="selected" </c:if>>Disable</option>
        </c:if>
        <c:if test="${program == null}">
          <option value = "true" >Enable</option>
          <option value = "false">Disable</option>
        </c:if>
      </select>
    </div>
  </div>


  <div class="col-md-12 form-group">
    <input type="submit" class="col-md-offset-8 col-md-4 btn btn-primary" value = 'Submit' onclick = "program();">
  </div>

</div>
<script>
  function program(){
    var url = '${route}';
    var formContent = {};
    formContent.name = validate(document.getElementById('name').value, 'Name', '', 'name');
    formContent.minimumMonthsOfStock = validate(document.getElementById('minimumMonthsOfStock').value, 'MinimumMonthsOfStock', '', 'minimumMonthsOfStock');
    formContent.maximumMonthsOfStock = validate(document.getElementById('maximumMonthsOfStock').value, 'MaximumMonthsOfStock', '', 'maximumMonthsOfStock');
    formContent.enabled = JSON.parse(document.getElementById('enabled').value);

   passData(url, formContent);
  }
</script>
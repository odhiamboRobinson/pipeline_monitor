<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-xs-12 col-md-12">
    <div class="card">
        <div class="card-header">

            <div class="col-md-12 card-title">
                <div class="col-md-2 title">Supply Plan Tracker</div>
                <img class="pull-right" id="loader" style="display:none" style="width:50px;height:50px;"
                     src="<c:url value="resources/img/logo/tablePreloader.gif"/>" alt="loading..."/>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group col-md-12">
                <select class="form-control" onchange="dockContent('supplyplan/view/' + this.value + '/form');$('.loadingOverlay').fadeOut('slow');">
                    <c:forEach items="${supplyPlanProducts}" var="supplyPlanProduct" >
                        <option value="${supplyPlanProduct.id}">${supplyPlanProduct.productName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-md-3">
                <table class="table">
                    <thead>
                    <tr>
                        <td style="visibility: hidden">Years<br></td>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan" >
                            <c:set var="product" value="${supplyPlan.product.name}"/>
                        </c:forEach>
                        <td>${product}</td>
                    </tr>
                    <tr>
                        <td>Forecast Consumption</td>
                    </tr>
                    <tr>
                        <td>Opening Stock Balance</td>
                    </tr>
                    <tr>
                        <td>Expected Deliveries</td>
                    </tr>
                    <tr>
                        <td>Central Store Issues</td>
                    </tr>
                    <tr>
                        <td>Actual Consumption</td>
                    </tr>
                    <tr>
                        <td>Desired Additional quantities</td>
                    </tr>
                    <tr>
                        <td>Calculated Closing Stock : Total</td>
                    </tr>
                    <tr>
                        <td>Actual Closing Stock: Central Stores</td>
                    </tr>
                    <tr>
                        <td>Actual Closing Stock: Peripheral Stores</td>
                    </tr>
                    <tr>
                        <td>MOS: Central Stores</td>
                    </tr>
                    <tr>
                        <td>MOS: Peripheral Stores</td>
                    </tr>
                    <tr>
                        <td>MOS: Total</td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="col-md-9" style="overflow-x: scroll;">
                <table class="table table-bordered table-supplyPlanner">
                    <tr>
                        <c:set var="count" value="1"/>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <c:if test="${supplyPlan.month == 12}">
                                <td colspan="${count}"> ${supplyPlan.year}</td>
                                <c:set var="count" value="1"/></c:if>
                            <c:set var="count" value="${count+1}"/>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${availableMonths}" var="availableMonth">
                            <c:forEach items="${months}" var="month">
                                <c:if test="${availableMonth eq month.index}">
                                    <td> ${month.name}</td>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td> ${supplyPlan.forecastConsumption}</td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td> ${supplyPlan.openingStockBalance}</td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td><input class="table-input" value="${supplyPlan.expectedDeliveries}" type="text"
                                       onchange="submitStockPlan('expectedDeliveries',this.value,${supplyPlan.id})">
                            </td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td><input class="table-input" value="${supplyPlan.centralIssues}" type="text"
                                       onchange="submitStockPlan('centralIssues',this.value,${supplyPlan.id})"></td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td><input class="table-input" value="${supplyPlan.actualConsumption}" type="text"
                                       onchange="submitStockPlan('actualConsumption',this.value,${supplyPlan.id})"></td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td>${supplyPlan.desiredAdditionalQuantity}</td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td>${supplyPlan.calculatedClosingStock}</td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td><input class="table-input" value="${supplyPlan. actualClosingStockCentralStores}"
                                       type="text"
                                       onchange="submitStockPlan('actualClosingStockCentralStores',this.value,${supplyPlan.id})">
                            </td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td><input class="table-input" value="${supplyPlan. actualClosingStockPeripheralStores}"
                                       type="text"
                                       onchange="submitStockPlan('actualClosingStockPeripheralStores',this.value,${supplyPlan.id})">
                            </td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td><c:if test="${supplyPlan.monthsOfStockCentralStores == null}">-</c:if><c:if
                                    test="${supplyPlan.monthsOfStockCentralStores != null}">${supplyPlan.monthsOfStockCentralStores}</c:if></td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                            <td><c:if test="${supplyPlan.monthsOfStockPeripheralStores == null}">-</c:if><c:if
                                    test="${supplyPlan.monthsOfStockPeripheralStores != null}">${supplyPlan.monthsOfStockPeripheralStores}</c:if></td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <c:forEach items="${supplyPlans}" var="supplyPlan">
                                 <c:if test="${supplyPlan.product.minimumMonthsOfStock != null}" >
                                     <c:set var="minimumMonthsOfStock" value="${supplyPlan.product.minimumMonthsOfStock}"/>
                                 </c:if>
                                <c:if test="${supplyPlan.product.maximumMonthsOfStock != null}" >
                                    <c:set var="maximumMonthsOfStock" value="${supplyPlan.product.maximumMonthsOfStock}"/>
                                </c:if>
                                <c:if test="${program.minimumMonthsOfStock != null}" >
                                    <c:set var="minimumMonthsOfStock" value="${program.minimumMonthsOfStock}"/>
                                </c:if>
                                <c:if test="${program.maximumMonthsOfStock != null}" >
                                    <c:set var="maximumMonthsOfStock" value="${program.maximumMonthsOfStock}"/>
                                </c:if>
                            <td
                                    <c:if test="${supplyPlan.totalMonthsOfStock <= 0 }" >
                                        style="background-color:red;color: white;"
                                    </c:if>
                                    <c:if test="${supplyPlan.totalMonthsOfStock > 0 and supplyPlan.totalMonthsOfStock <= minimumMonthsOfStock}" >
                                        style="background-color:yellow;color: black;"
                                    </c:if>
                                    <c:if test="${supplyPlan.totalMonthsOfStock > 3 and supplyPlan.totalMonthsOfStock < maximumMonthsOfStock}" >
                                        style="background-color:green;color: white;"
                                    </c:if>
                                    <c:if test="${ supplyPlan.totalMonthsOfStock > maximumMonthsOfStock}" >
                                        style="background-color:sandybrown;color:black;"
                                    </c:if>
                                    >
                                ${supplyPlan.totalMonthsOfStock}
                            </td>
                        </c:forEach>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function submitStockPlan(input, value, supplyPlanId) {
        formContent = {};
        if (value == '' || value == null) {
            value = null;
        }
        if (value == null || (isNumeric(value) && parseFloat(value) > 0.0)) {
            if (input == "expectedDeliveries") {
                formContent.expectedDeliveries = (value == null)? null:parseFloat(value);
            } else {
                formContent.expectedDeliveries = -1.0
            }
            if (input == 'actualConsumption') {
                formContent.actualConsumption = (value == null)? null:parseFloat(value)
            } else {
                formContent.actualConsumption = -1.0
            }

            if (input == 'actualClosingStockCentralStores') {
                formContent.actualClosingStockCentralStores = (value == null)? null:parseFloat(value)
            } else {
                formContent.actualClosingStockCentralStores = -1.0
            }

            if (input == 'actualClosingStockPeripheralStores') {
                formContent.actualClosingStockPeripheralStores = (value == null)? null:parseFloat(value)
            } else {
                formContent.actualClosingStockPeripheralStores = -1.0
            }

            if (input == 'centralIssues') {
                formContent.centralIssues = (value == null)? null:parseFloat(value)
            } else {
                formContent.centralIssues = -1.0
            }


            $.ajax({
                url: "/supplyplan/" + supplyPlanId + "/edit",
                type: "POST",
                data: JSON.stringify(formContent),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                complete: function (xhr, status) {
                    dockContent('supplyplan/view/' + supplyPlanId + '/form');
                    $('.loadingOverlay').fadeOut('slow');
                }
            });
        }

    }
</script>
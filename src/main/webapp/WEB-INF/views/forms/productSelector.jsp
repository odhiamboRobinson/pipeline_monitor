<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: robinson
  Date: 3/8/16
  Time: 2:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="form-group">
  <select onchange="getChart('${loadingText}')" class="form-control" id="product"  >
    <c:forEach items="${products}" var="product" >
      <option value="${product.id}" > ${product.name}</option>
    </c:forEach>
  </select>
</div>
<script>
  function getChart(txt) {
    var formContent = {};

    $('#period :selected').each(function(i, selected) {
      formContent = {productId:$(selected).val()};
    });

    callPost('${route}',txt,formContent,graphPlotter);
  }
</script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-xs-12" onload="productData()">
    <div class="card">
        <div class="card-header">

            <div class="col-md-12 card-title">
                <div class="col-md-2 title">Products</div>
            </div>
        </div>
        <div class="card-body">
            <table data-editable data-editable-spy data-navigable-spy class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Min Mos</th>
                    <th>Max Mos</th>
                    <th>Enabled</th>
                </tr>
                </thead>
                <tbody>
                <!-- The last <tr> in the <tbody> will be used as template for new rows -->
                <tr>
                    <td><input onchange="valOnChange(this)" name="name" placeholder="Product name" type="text"></td>
                    <td><input onchange="valOnChange(this)" name="unit" placeholder="e.g gm " type="text"></td>
                    <td><input onchange="valOnChange(this)" name="quantity" placeholder="quantity" type="text"></td>
                    <td><input onchange="valOnChange(this)" name="unitPrice" placeholder="unit price" type="text"></td>
                    <td><input onchange="valOnChange(this)" name="minMos" placeholder="min Mos" type="number"></td>
                    <td><input onchange="valOnChange(this)" name="maxMos" placeholder="max Mos" type="number"></td>
                    <td>
                        <select class='form-control' name='enabled'>
                                <option value="true" selected="selected">Enabled</option>
                                <option value="false">Disabled</option>
                        </select>
                    </td>
                    <td>
                        <span class="glyphicon glyphicon-remove" data-remove></span>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="col-md-12 card-title">
                <button class="col-md-2 pull-right btn btn-primary" onclick="addProduct()">Add Products</button>
            </div>
        </div>
    </div>
</div>
<script>

    function addProduct() {

    var $table = $('table');
    $table.editableTable('get', function(records) {
        url ="product/add";
        var idx = 0;
        productForms = [];
        for(idx = 0;idx < records.length; idx++) {
            productForms.push({
                name : records[idx].name,
                unit : records[idx].unit,
                quantity : JSON.parse(parseFloat(records[idx].quantity)),
                unitPrice : JSON.parse(parseFloat(records[idx].unitPrice)),
                minimumMonthsOfStock : JSON.parse(records[idx].minMos),
                maximumMonthsOfStock : JSON.parse(records[idx].maxMos),
                enabled : JSON.parse(records[idx].enabled)
            });
        }

        formContent = {};
        formContent.productForms = productForms;
        document.getElementById('dockMsg').innerHTML = passData(url, formContent);
    });

    }
</script>
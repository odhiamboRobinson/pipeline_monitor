<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class = 'col-md-12'>
    <div class="form-group">
        <input type="text" class="form-control" id="idNumber" value="${user.idNumber}" placeholder="Enter National ID">
    </div>

    <div class = 'row'>
        <div class="form-group col-md-6">
            <input type="text" class="form-control" id="sirName" value="${user.sirName}" placeholder="Enter Your Surname">
        </div>

        <div class="form-group col-md-6">
            <input type="text" class="form-control" id="otherNames" value="${user.otherNames}" placeholder="Enter Other Names">
        </div>
    </div>

    <div class="form-group">
        <input type="text" class="form-control" id="email" value="${user.email}" placeholder="Enter email">
    </div>

    <div class="form-group">
        <input type="text" class="form-control" id="phoneNumber" value="${user.phoneNumber}" placeholder="Enter Phone Number">
    </div>
    <div class="form-group">
        <select class = 'form-control' id = 'enabled'>
            <option value ="true" >Enable</option>
            <option value ="false" >Disable</option>
        </select>
    </div>
    <div class = 'row'>
            <div class = 'form-group col-md-6'>
                <select  class="form-control" id = 'groupIds' multiple="multiple" name = 'groupIds'>
                    <c:if test="${user != null }" >
                        <c:forEach items="${userGroups}" var="group2">
                            <option value='<c:out value="${group2.id}"/>'
                                <c:forEach items="${user.userGroups}" var="group" > <c:if test="${group.id == group2.id }" > selected="selected" </c:if> </c:forEach> >
                                <c:out value="${group2.name}"/>
                            </option>
                        </c:forEach>
                    </c:if>
                    <c:if test="${user == null}" >
                        <c:forEach items="${userGroups}" var="group">
                            <option value='<c:out value="${group.id}" />'> ${group.name} </option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
            <div class = 'form-group col-md-6'>
                <select class = 'form-control' name = 'programIds' multiple="multiple" id = 'programIds'>
                    <c:if test="${user != null }" >
                        <c:forEach items="${programs}" var="program" >
                            <option value='<c:out value="${program.id}"/>'
                                <c:forEach items="${user.programs}" var="program2">  <c:if test="${program.id == program2.id }" >selected="selected" </c:if> </c:forEach> > <c:out value="${program.name}"/>
                            </option>
                        </c:forEach>
                    </c:if>
                    <c:if test="${user == null}" >
                        <c:forEach items="${programs}" var="program">
                            <option value='<c:out value="${program.id}" />'> ${program.name} </option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
    </div>
</div>
<div class="col-md-12 form-group">
    <input type="submit"  class="col-md-offset-8 col-md-4 btn btn-primary ladda-button" data-color="green" data-style="expand-left" value = 'Submit' onclick = "submitUser();">
</div>
<script>

    function submitUser(){
        var url = '${route}';
        var formContent= {};

        formContent.programIds = [];
        var ids = validate($('select#programIds' ).val(), 'Program ID', '', 'programIds');
        for (var i = 0, len = ids.length; i < len; i++) {
            formContent.programIds.push(parseInt(ids[i]));
        }

        formContent.userGroupIds = [];
        var ids = validate($('select#groupIds' ).val(), 'Group ID', '', 'groupIds');
        for (var i = 0, len = ids.length; i < len; i++) {
            formContent.userGroupIds.push(parseInt(ids[i]));
        }

        formContent.idNumber = validate(document.getElementById('idNumber').value, 'ID/Passport Number', '', 'idNumber');
        formContent.sirName = validate(document.getElementById('sirName').value, '', 'Sur Name', '', 'sirName');
        formContent.otherNames = validate(document.getElementById('otherNames').value, 'Other Names appropriately', '', 'otherNames');
        formContent.email = validate(document.getElementById('email').value, 'Email Address', 'email', 'email');
        formContent.phoneNumber = validate(document.getElementById('phoneNumber').value, 'Phone Number', '', 'phoneNumber');
        formContent.enabled = JSON.parse(document.getElementById('enabled').value);

        document.getElementById('miniNavDock').innerHTML = passData(url, formContent);

    }
</script>
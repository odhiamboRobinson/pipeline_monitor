<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: robinson
  Date: 3/8/16
  Time: 9:55 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="form-group">
  <select onchange="getChart('${loadingText}')" class="form-control" id="period" <c:if test="${multiple == true}"> style="min-height:290px;" multiple="multiple"  </c:if> >
    <c:forEach items="${monthYears}" var="monthYear" >
      <option value="${monthYear.month},${monthYear.year}" > <c:forEach items="${months}" var="month"><c:if test="${month.index == monthYear.month}">${month.name}</c:if> </c:forEach>, ${monthYear.year} </option>
    </c:forEach>
  </select>
</div>
<script>
  function getChart(txt) {
    var formContent = {};
    <c:if test="${multiple == true}" >
    formContent.monthYears= [];

    $('#period :selected').each(function(i, selected) {

      var val = $(selected).val().split(',');
      formContent.monthYears.push({month:val[0],year:val[1]});
    });
    </c:if>
    <c:if test="${multiple == false}">

    $('#period :selected').each(function(i, selected) {

      var val = $(selected).val().split(',');
      formContent = {month:val[0],year:val[1]};
    });
    </c:if>

    callPost('${route}',txt,formContent,graphPlotter);
  }
</script>
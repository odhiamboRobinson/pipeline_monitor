<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-xs-12">
  <div class="card">
    <div class="card-header">

      <div class="col-md-12 card-title">
        <div class="col-md-2 title">Forecast </div>
      </div>
    </div>
    <div class="card-body">
      <table class="table table-hover table-striped">
        <tbody>
          <tr>
            <td>Starting Month </td><td>
            <select id="startingMonth">
              <option value="">Select a Month</option>
              <option value="1">January</option>
              <option value="2">February</option>
              <option value="3">March</option>
              <option value="4">April</option>
              <option value="5">May</option>
              <option value="6">June</option>
              <option value="7">July</option>
              <option value="8">August</option>
              <option value="9">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select></td>
            <td>Starting Year </td><td><input type="text" id="startingYear" ></td>
            <td>Enabled </td><td><input type="checkbox" value="true" id="enabled" ></td>
          </tr>
          <tr>
            <td>Ending Month </td><td>
            <select id="endingMonth" >
              <option value="">Select a Month</option>
              <option value="1">January</option>
            <option value="2">February</option>
            <option value="3">March</option>
            <option value="4">April</option>
            <option value="5">May</option>
            <option value="6">June</option>
            <option value="7">July</option>
            <option value="8">August</option>
            <option value="9">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
          </select></td>
            <td>Ending Year </td><td><input type="text" id="endingYear"></td>
            <td>Current </td><td><input type="checkbox" value="true" id="current" ></td>
          </tr>
        </tbody>
      </table>
      <table class="table table-hover table-striped">
        <tbody>
        <tr>
          <th>Product</th>
          <th>Forecast factor</th>
          <th>Forecast Consumption</th>
          <th>Initial SOH National Stores</th>
          <th>Initial SOH Sub National Stores</th>
        </tr>
          <c:forEach items="${products}" var="product" varStatus="productLoop">
              <c:forEach items="${forecastForm.productForecasts}" var="productForecast">
                <c:if test="${productForecast.productId == product.id}">
                    <tr>
                      <td style="display: none"><input type="text" class='<c:out value="product${productLoop.index}"/>' value="${product.id}" ></td>
                      <td>${product.name}&nbsp&nbsp&nbsp${product.quantity} ${product.unit}</td>
                      <td><input type="text" class='<c:out value="product${productLoop.index}"/>' value="${productForecast.forecastFactor}"></td>
                      <td><input type="text" class='<c:out value="product${productLoop.index}"/>' value="${productForecast.forecastConsumption}" ></td>
                      <td><input type="text" class='<c:out value="product${productLoop.index}"/>' value="${productForecast.initialStockOnHandNationalStores}" > </td>
                      <td><input type="text" class='<c:out value="product${productLoop.index}"/>' value="${productForecast.initialStockOnHandSubNationalStores}"> </td>
                    </tr>
                </c:if>
              </c:forEach>
              <c:if test="${forecastForm == null}" >
                <tr>
                  <td style="display: none"><input type="text" class='<c:out value="product${productLoop.index}"/>' value="${product.id}" ></td>
                  <td>${product.name}&nbsp&nbsp&nbsp${product.quantity} ${product.unit}</td>
                  <td><input type="text" class='<c:out value="product${productLoop.index}"/>' ></td>
                  <td><input type="text" class='<c:out value="product${productLoop.index}"/>' ></td>
                  <td><input type="text" class='<c:out value="product${productLoop.index}"/>' > </td>
                  <td><input type="text" class='<c:out value="product${productLoop.index}"/>' > </td>
                </tr>
              </c:if>
              <c:set var="counter"  value="${productLoop.count}"/>
          </c:forEach>
        </tbody>
      </table>
    </div>
    <div class="col-md-12 card-title">
      <button class="col-md-2 pull-right btn btn-primary" data-color="green" data-style="expand-left" onclick="submitForecast(${counter})">Submit</button>
    </div>
  </div>
</div>
<script>
  function submitForecast(length) {
    var formContent = {};
    formContent.startingMonth = JSON.parse($("#startingMonth").val());
    formContent.startingYear = JSON.parse($("#startingYear").val());
    formContent.endingMonth = JSON.parse($("#endingMonth").val());
    formContent.endingYear = JSON.parse($("#endingYear").val());
    formContent.productForecasts =[];
    formContent.enabled = ($("#enabled:checked ").val()) ? true : false;
    formContent.current = ($("#current:checked ").val()) ? true : false;
    formContent.id = 0;
    var idx;
    for(idx = 0;idx <length;idx++){
      var productForecast= [];
      $('.product'+idx).each(function() {
        if($(this).val() != '') {
          productForecast.push(JSON.parse($(this).val()));
        }

      });

      formContent.productForecasts.push({
        productId:productForecast[0],
        forecastFactor:(productForecast[1]).toFixed(2),
        forecastConsumption:(productForecast[2]).toFixed(2),
        initialStockOnHandNationalStores:(productForecast[3]).toFixed(),
        initialStockOnHandSubNationalStores:(productForecast[4]).toFixed()
      });
    }
    passData('forecast/add',formContent);

  }

</script>
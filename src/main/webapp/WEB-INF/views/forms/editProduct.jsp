<%--
  Created by IntelliJ IDEA.
  User: robinson
  Date: 3/3/16
  Time: 12:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class = 'col-md-12'>
  <div class="form-group">
    <input type="text" class="form-control" id="name" value="${product.name}" placeholder="Enter Name">
  </div>

  <div class = 'row'>
    <div class="form-group col-md-6">
      <input type="text" class="form-control" id="unit" value="${product.unit}" placeholder="Enter unit">
    </div>

    <div class="form-group col-md-6">
      <input type="text" class="form-control" id="quantity" value="${product.quantity}" placeholder="Enter Quantity">
    </div>
  </div>

  <div class="form-group">
    <input type="text" class="form-control" id="unitPrice" value="${product.unitPrice}" placeholder="Enter unit price">
  </div>

  <div class="form-group">
    <input type="text" class="form-control" id="minimumMonthsOfStock" value="${product.minimumMonthsOfStock}" placeholder="Enter minimum months of stock">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="maximumMonthsOfStock" value="${product.maximumMonthsOfStock}" placeholder="Enter maximum months of stock">
  </div>

  <div class="form-group">
    <select class = 'form-control' id = 'enabled'>
      <option value ="true" >Enable</option>
      <option value ="false" >Disable</option>
    </select>
  </div>
</div>
<div class="col-md-12 form-group">
  <input type="submit"  class="col-md-offset-8 col-md-4 btn btn-primary ladda-button" data-color="green" data-style="expand-left" value = 'Submit' onclick = "submitProduct();">
</div>
<script>

  function submitProduct(){
    var url = '${route}';
    var formContent= {};

    formContent.name = validate(document.getElementById('name').value, 'name', '');
    formContent.unit = validate(document.getElementById('unit').value, 'Unit','');
    formContent.quantity = JSON.parse(validate(document.getElementById('quantity').value, 'Quantity', ''));
    formContent.unitPrice = JSON.parse(validate(document.getElementById('unitPrice').value, 'unit price',''));
    formContent.minimumMonthsOfStock = JSON.parse(validate(document.getElementById('minimumMonthsOfStock').value, 'minimum Months Of Stock', ''));
    formContent.maximumMonthsOfStock = JSON.parse(validate(document.getElementById('maximumMonthsOfStock').value, 'maximum Months Of Stock', ''));
    formContent.enabled = JSON.parse(document.getElementById('enabled').value);

    document.getElementById('miniNavDock').innerHTML = passData(url, formContent);

  }
</script>
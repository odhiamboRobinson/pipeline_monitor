<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-xs-12">
    <div class="card">
        <div class="card-header">

            <div class="col-md-12 card-title ">
                <div class="col-md-2 title">Supply Plan</div>
            </div>
        </div>
        <div class="card-body">
            <table id="supplyplanTable" class="datatable table table-striped table-hover" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Product</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${supplyPlans}" var="supplyPlan">
                    <tr onclick="dockContent('supplyplan/view/${supplyPlan.id}/form')">
                        <td>${supplyPlan.productName}</td>
                    </tr>
                </c:forEach>
                </tbody>
                <tfoot>
                <tr>
                    <th>Product</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
$('#supplyplanTable').DataTable();
</script>

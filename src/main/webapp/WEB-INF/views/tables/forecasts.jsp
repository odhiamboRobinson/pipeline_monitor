<div class="col-xs-12 structure">
  <div class="card">
    <div class="card-header">

      <div class="col-md-12 card-title">
        <div class="col-md-2 title">Forecasts</div>
        <div style = 'text-align:right;' class="col-md-10"><button onclick = "dockContent('/forecast/view/add/form');" class = 'btn btn-primary'>Add Forecast</button></div>
      </div>
    </div>
    <div class="card-body">
      <table id="forecastTable" class="datatable table table-striped table-hover" cellspacing="0" width="100%">
        <thead>
        <tr>
          <th>ID</th>
          <th>Starting Month</th>
          <th>Starting Year</th>
          <th>Ending Month</th>
          <th>Ending Year</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
        <tr>
          <th>ID</th>
          <th>Starting Month</th>
          <th>Starting Year</th>
          <th>Ending Month</th>
          <th>Ending Year</th>
          <th>Active</th>
        </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script>
  function forecastTable() {
      table = $('#forecastTable').DataTable({
          destroy: true,
          data: this,
          columns: [
              {data:'id'},
              {data:'startingMonth'},
              {data:'startingYear'},
              {data:'endingMonth'},
              {data:'endingYear'}
          ],
          columnDefs: [
              {
                  "targets": [ 0 ],
                  "visible": false,
                  "searchable": false
              },
              {

                  "render":function(data,type,row){if(row.inActive){return "active"} else {return "click to activate"}},
                  "targets": 5
              }
          ]
      });

      $('#forecastTable tbody').on('click','tr', function () {
          call('/forecast/'+table.row(this).data().id+'/activate','activating forecast',forecastTable);
      });

      $('.loadingOverlay').css('visibility' , 'hidden');
      $('.structure').animate({opacity:1.0},500);

  }
  call('/forecast/list','loading forecast data',forecastTable);
</script>
<div class="col-xs-12 structure">
    <div class="card">
        <div class="card-header">

            <div class="col-md-12 card-title">
                <div class="col-md-2 title">Users</div>
                <div style = 'text-align:right;' class="col-md-10"><button onclick = "newEntity('/user/view/add/form');" class = 'btn btn-primary'>Add User</button></div>
            </div>
        </div>
        <div class="card-body">
            <table id="usersTable" class="datatable table table-striped table-hover" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>User Id</th>
                    <th>Sur Name</th>
                    <th>Other Names</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Enabled</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                    <th>Id</th>
                    <th>User Id</th>
                    <th>Sur Name</th>
                    <th>Other Names</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Enabled</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    function usersTable() {
            var table = $('#usersTable').DataTable({
                destroy: true,
                data: this,
                columns: [
                    {data: 'id'},
                    {data: 'idNumber'},
                    {data: 'sirName'},
                    {data: 'otherNames'},
                    {data: 'email'},
                    {data: 'phoneNumber'},
                    {data: 'enabled'}
                ]
            });

            $('#usersTable tbody').on('click', 'tr', function () {
                var idNumber = this.cells[1].innerHTML;
                popEdit('/user/view/'+idNumber+'/mininav');
            });
        $('.structure').animate({opacity:1.0},500);
    }
    call('/user/list','loading user data',usersTable);
</script>
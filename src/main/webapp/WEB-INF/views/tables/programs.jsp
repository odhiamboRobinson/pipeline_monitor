<div class="col-xs-12 structure" >
  <div class="card">
    <div class="card-header">

      <div class="col-md-12 card-title">
        <div class="col-md-2 title">Programs</div>
        <div style = 'text-align:right;' class="col-md-10"><button onclick = "newEntity('/program/view/add/form');" class = 'btn btn-primary'>Add Program</button></div>
      </div>
    </div>
    <div class="card-body">
      <table id="programsTable" class="datatable table table-striped table-hover" cellspacing="0" width="100%">
        <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>MIN MOS </th>
          <th>MAX MOS</th>
          <th>Enabled</th>
          <th>Date Created</th>
          <th>Date Updated</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>MIN MOS </th>
          <th>MAX MOS</th>
          <th>Enabled</th>
          <th>Date Created</th>
          <th>Date Updated</th>
        </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script>

  function programsTable() {
      var table = $('#programsTable').DataTable({
          destroy: true,
          data: this,
          columns: [
              {data: 'id'},
              {data: 'name'},
              {data: 'minimumMonthsOfStock'},
              {data: 'maximumMonthsOfStock'},
              {data: 'enabled'},
              {data: 'createdOn'},
              {data: 'updatedOn'}
          ],
          columnDefs: [
              {
                  "targets": [ 0 ],
                  "visible": false,
                  "searchable": false
              },
              {
                  "targets":[ 4 ],
                  "searchable":false
              },
              {
                  "render":function(data,type,row){return new Date(row.createdOn).toDateString()},
                  "targets": 5
              },
              {
                  "render":function(data,type,row){if(row.updatedOn != null){return new Date(row.updatedOn).toDateString()}else {return "N/A"}},
                  "targets": 6
              }

          ],
          "pagingType": "simple"
      });

      $('#programsTable tbody').on('click', 'tr', function (){
          infoEnt('/program/view/'+table.row( this ).data().id+'/edit/form','/program/view/'+table.row( this ).data().id+'/info');
      });
      $('.structure').animate({opacity:1.0},500);
  }
  call('/program/list','loading program data',programsTable);
</script>
<div class="col-xs-12 structure">
    <div class="card">
        <div class="card-header">

            <div class="col-md-12 card-title">
                <div class="col-md-2 title">Groups</div>
                <div style = 'text-align:right;' class="col-md-10"><button onclick = "dockContent('/group/view/add/form');" class = 'btn btn-primary'>Add Group</button></div>
            </div>
        </div>
        <div class="card-body">
            <table id="groupsTable" class="datatable table table-striped table-hover" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Group ID</th>                
                    <th>Group Name</th>
                    <th>Created On</th>
                    <th>Enabled</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                    <th>Group ID</th>
                    <th>Group Name</th>
                    <th>Created On</th>
                    <th>Enabled</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>

        function groupsTable() {
            var table = $('#groupsTable').DataTable({
                destroy: true,
                data: this,
                columns: [
                    {data: 'id'},
                    {data: 'name'},
                    {data: 'createdOn'},
                    {data: 'enabled'}
                ],
                columnDefs: [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "render": function (data, type, row) {
                            return new Date(row.createdOn).toDateString()
                        },
                        "targets": 2
                    }
                ]
            });

            $('#groupsTable tbody').on('click', 'tr', function () {
                $('#loadingText').html('loading user group form');
                dockContent('/group/view/' + table.row(this).data().id + '/edit/form')
            });
            $('.structure').animate({opacity:1.0},500);
        }
        call('/group/list','loading user group data',groupsTable);
</script>
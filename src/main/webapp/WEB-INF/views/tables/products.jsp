<div class="col-xs-12" onload ="productData()">
    <div class="card">
        <div class="card-header">

            <div class="col-md-12 card-title">
                <div class="col-md-2 title">Products</div>
                <div style = 'text-align:right;' class="col-md-10"><button onclick = "dockContent('/product/view/add/form');" class = 'btn btn-primary'>Add Product</button></div>
            </div>
        </div>
        <div class="card-body">
            <table id="productsTable" class="datatable table table-striped table-hover" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Product</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Enabled</th>
                    <th>Date Created</th>
                    <th>Date Updated</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Product</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Enabled</th>
                    <th>Date Created</th>
                    <th>Date Updated</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    function productsTable() {
        var table = $('#productsTable').DataTable({
            destroy: true,
            data: this,
            columns: [
                {data: 'id'},
                {data: 'name'},
                {data: 'unit'},
                {data: 'quantity'},
                {data: 'unitPrice'},
                {data: 'enabled'},
                {data: 'createdOn'},
                {data: 'updatedOn'}
            ],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets":[ 5 ],
                    "searchable":false
                },
                {
                    "render":function(data,type,row){return new Date(row.createdOn).toDateString()},
                    "targets": 6
                },
                {
                    "render":function(data,type,row){if(row.updatedOn != null){return new Date(row.updatedOn).toDateString()}else {return "N/A"}},
                    "targets": 7
                }

            ]
        });

        $('#productsTable tbody').on('click', 'tr', function () {
            infoEnt('/product/view/'+table.row( this ).data().id+'/edit/form','/product/view/'+table.row( this ).data().id+'/info');
        });
    }

    call('/product/list','loading program data',productsTable);

</script>
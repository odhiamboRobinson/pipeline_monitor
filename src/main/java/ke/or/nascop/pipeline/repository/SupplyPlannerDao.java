package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.SupplyPlan;
import ke.or.nascop.pipeline.wrappers.MonthYear;
import ke.or.nascop.pipeline.wrappers.MosGraph;
import ke.or.nascop.pipeline.wrappers.SupplyPlanJson;
import ke.or.nascop.pipeline.wrappers.SupplyPlanProduct;

import java.util.List;

/**
 * Created by robinson on 2/20/16.
 */
/*The SupplyPlannerDao interface declares methods
 *that must be implemented by a class interacting with the data source
 */
public interface SupplyPlannerDao {
    void add (SupplyPlan supplyPlan);
    void add (List<SupplyPlan> supplyPlans);
    SupplyPlan getById (Integer id);
    List<SupplyPlanJson> getAll();
    SupplyPlanProduct getSummary(Integer forecastId,Integer minMos,Integer maxMos);
    List<SupplyPlanProduct> getSummaryPerProduct(Integer forecastId);
    List<SupplyPlan> search(String pattern);
    List<Object[]> mosByProduct(Integer productId,Integer programId,Integer forecastId,Integer period,Boolean programOverride);
    void update (SupplyPlan supplyPlan);
    void update (List<SupplyPlan> supplyPlans);
    void delete (SupplyPlan supplyPlan);
}

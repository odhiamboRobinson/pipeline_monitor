package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.Product;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by robinson on 2/20/16.
 */
/*
 *The ProductDaoImpl interacts with the datasource
 * to perform crud operations on it.
 * (It implements the methods defined in the ProductDao interface)
 */
@Repository
public class ProductDaoImpl implements ProductDao {
    @PersistenceContext
    private EntityManager entityManager;

    public void add (Product product) {
        entityManager.persist(product);
        entityManager.flush();
    }

    public Product getById (Integer id) {
        TypedQuery<Product> query = entityManager.createQuery("SELECT p FROM Product p WHERE p.id = :id AND (p.deletedBy IS NULL AND p.deletedOn IS NULL) ",Product.class);
        query.setParameter("id", id);
        List<Product> result = query.getResultList();
        if(! result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }

    public Product getByNameAndUnitAndQuantity (String name,String unit,Double quantity) {
        TypedQuery<Product> query = entityManager.createQuery("SELECT p FROM Product p WHERE p.name = :name AND p.unit =:unit AND p.quantity = :quantity AND (p.deletedBy IS NULL AND p.deletedOn IS NULL) ",Product.class);
        query.setParameter("name", name);
        query.setParameter("unit", unit);
        query.setParameter("quantity", quantity);
        List<Product> result = query.getResultList();
        if(! result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }


    public List<Product> getAll (Integer programId) {
        TypedQuery<Product> query = entityManager.createQuery("SELECT p FROM Product p JOIN p.program pro WHERE pro.id =:programId AND p.deletedBy IS NULL AND p.deletedOn IS NULL ",Product.class);
        query.setParameter("programId",programId);
        List<Product> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }

    public List<Product> getByProgramId (Integer id) {
        TypedQuery<Product> query = entityManager.createQuery("SELECT p FROM Product p JOIN  p.program pro WHERE pro.id = :id AND p.deletedBy IS NULL AND p.deletedOn IS NULL ",Product.class);
        query.setParameter("id",id);
        List<Product> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }

    public List<Product> search (String pattern) {
        TypedQuery<Product> query = entityManager.createQuery("SELECT p FROM Product p WHERE p.name LIKE :pattern AND (p.deletedBy IS NULL AND p.deletedOn IS NULL) ",Product.class);
        query.setParameter("pattern",pattern);
        List<Product> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }

    public void update (Product product) {
        entityManager.merge(product);
        entityManager.flush();
    }

    public void delete (Product product) {
        entityManager.merge(product);
        entityManager.flush();
    }
}

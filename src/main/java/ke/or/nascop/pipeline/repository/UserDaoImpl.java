package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by robinson on 2/7/16.
 */
/*
 *The userDaoImpl class interacts with the database to affect
 * the underlying information in the database
*/
@Repository
public class UserDaoImpl implements UserDao {
    @PersistenceContext
    private EntityManager entityManager;

    public void save (User user) {
        entityManager.persist(user);
        entityManager.flush();
    }

    public User getById (int id) {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.id = :id AND (u.deletedBy IS NULL AND u.deletedOn IS NULL) ",User.class);
        query.setParameter("id",id);
        List<User> result = query.getResultList();
        if(result.isEmpty()) {
           return null;
        }

        return result.get(0);
    }

    public User getByIdNumber (String idNumber) {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.idNumber = :idNumber AND (u.deletedBy IS NULL AND u.deletedOn IS NULL)",User.class);
        query.setParameter("idNumber",idNumber);
        List<User> result = query.getResultList();
        if(result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }

    public User getByEmail (String email) {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email AND (u.deletedBy IS NULL AND u.deletedOn IS NULL)",User.class);
        query.setParameter("email",email);
        List<User> result = query.getResultList();
        if(result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }

    public List<User> getAll () {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u  WHERE (u.deletedBy IS NULL AND u.deletedOn IS NULL) ",User.class);
        List<User> users = query.getResultList();
        if(users ==null || users.isEmpty()) {
            return null;
        }

        return users;
    }

    public List<User> search (String pattern) {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.email LIKE :pattern OR u.firstname LIKE :pattern OR u.lastname LIKE :pattern AND (u.deletedBy IS NULL AND u.deletedOn IS NULL)",User.class);
        query.setParameter("pattern","%"+pattern+"%");
        List<User> users = query.getResultList();
        if(users.isEmpty()) {
            return null;
        }

        return users;
    }

    public void update (User user) {
        entityManager.merge(user);
        entityManager.flush();
    }

    public void delete (User user) {
        update(user);
    }
}

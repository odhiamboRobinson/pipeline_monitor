package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.Program;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by robinson on 2/20/16.
 */
/*
 *The ProgramDaoImpl interacts with the datasource
 * to perform crud operations on it.
 * (It implements the methods defined in the ProgramDao interface)
 */
@Repository
public class ProgramDaoImpl implements ProgramDao {
    @PersistenceContext
    private EntityManager entityManager;

    public void add (Program program) {
        entityManager.persist(program);
        entityManager.flush();
    }

    public Program getById (Integer id) {
        TypedQuery<Program> query = entityManager.createQuery("SELECT p FROM Program p WHERE p.id = :id AND (p.deletedBy IS NULL AND p.deletedOn IS NULL) ",Program.class);
        query.setParameter("id", id);
        List<Program> result = query.getResultList();
        if(! result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }

    public Program getByName (String name) {
        TypedQuery<Program> query = entityManager.createQuery("SELECT p FROM Program p WHERE p.name = :name AND (p.deletedBy IS NULL AND p.deletedOn IS NULL) ",Program.class);
        query.setParameter("name", name);
        List<Program> result = query.getResultList();
        if(! result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }


    public List<Program> getAll () {
        TypedQuery<Program> query = entityManager.createQuery("SELECT p FROM Program p WHERE p.deletedBy IS NULL AND p.deletedOn IS NULL ",Program.class);
        List<Program> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }

    public List<Program> search (String pattern) {
        TypedQuery<Program> query = entityManager.createQuery("SELECT p FROM Program p WHERE p.name LIKE :pattern AND (p.deletedBy IS NULL AND p.deletedOn IS NULL) ",Program.class);
        query.setParameter("pattern",pattern);
        List<Program> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }

    public void update (Program program) {
        entityManager.merge(program);
        entityManager.flush();
    }

    public void delete (Program program) {
        entityManager.merge(program);
        entityManager.flush();
    }
}

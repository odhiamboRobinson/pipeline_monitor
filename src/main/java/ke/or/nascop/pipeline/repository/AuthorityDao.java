package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.Authority;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by robinson on 2/7/16.
 */

/*
 *The authorityDao interface is used to offer an integration point between
 * the upper layer (authorityServiceImpl)
*/
@Transactional
public interface AuthorityDao {
    List<Authority> getAll ();
    List<Authority> getAuthoritiesByGroupId(int id);
    Authority getById(int id);
}

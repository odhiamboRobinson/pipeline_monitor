package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.wrappers.MonthYear;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by robinson on 3/6/16.
 */

@Repository
public class AnalysisDaoImpl implements AnalysisDao {
    @PersistenceContext
    EntityManager entityManager;

    public List<Object[]> mosByProduct(Integer forecastId,MonthYear monthYear){
        Query query = entityManager.createNativeQuery("select concat(p.name,' ',p.quantity,p.unit,' ',p.unit_price) as product ,s.total_months_of_stock from supply_plan s inner join products p on (s.product_id = p.id) where s.forecast_id = :forecastId AND month =:monthInstance AND year =:yearInstance");
        query.setParameter("forecastId",forecastId);
        query.setParameter("monthInstance",monthYear.getMonth());
        query.setParameter("yearInstance",monthYear.getYear());
        List<Object[]> resultList  = query.getResultList();
        if(resultList.isEmpty()) {
            return null;
        }

        return resultList;
    }
    public List<Object[]> mosPerProductOverTime (Integer forecastId,Integer productId){
        Query query = entityManager.createNativeQuery("select concat(p.name,' ',p.quantity,p.unit,' ',p.unit_price) as product ,s.total_months_of_stock,concat(s.month,' ',s.year) period from supply_plan s inner join products p on (s.product_id = p.id) where s.forecast_id = :forecastId AND s.product_id =:productId");
        query.setParameter("forecastId",forecastId);
        query.setParameter("productId",productId);
        List<Object[]> resultList  = query.getResultList();
        if(resultList.isEmpty()) {
            return null;
        }

        return resultList;
    }
    public List<Object[]> centralMosPerProduct(Integer forecastId,MonthYear monthYear){
        Query query = entityManager.createNativeQuery("select concat(p.name,' ',p.quantity,p.unit,' ',p.unit_price) as product ,s.months_of_stock_central_stores from supply_plan s inner join products p on (s.product_id = p.id) where s.forecast_id = :forecastId AND month =:monthInstance AND year =:yearInstance");
        query.setParameter("forecastId",forecastId);
        query.setParameter("monthInstance",monthYear.getMonth());
        query.setParameter("yearInstance",monthYear.getYear());
        List<Object[]> resultList  = query.getResultList();
        if(resultList.isEmpty()) {
            return null;
        }

        return resultList;
    }
    public List<Object[]> peripheralMosPerProduct(Integer forecastId,MonthYear monthYear){
        Query query = entityManager.createNativeQuery("select concat(p.name,' ',p.quantity,p.unit,' ',p.unit_price) as product ,s.months_of_stock_peripheral_stores from supply_plan s inner join products p on (s.product_id = p.id) where s.forecast_id = :forecastId AND month =:monthInstance AND year =:yearInstance");
        query.setParameter("forecastId",forecastId);
        query.setParameter("monthInstance",monthYear.getMonth());
        query.setParameter("yearInstance",monthYear.getYear());
        List<Object[]> resultList  = query.getResultList();
        if(resultList.isEmpty()) {
            return null;
        }

        return resultList;
    }
    public List<Object[]> forecastErrorPerProduct(Integer forecastId,List<String> instances) {
        Query query = entityManager.createNativeQuery("select s.product,(((sum(s.actual_consumption) - sum(s.forecastConsumption))/sum(s.actual_consumption))*100) as forecastError from (SELECT concat(p.name,' ',p.unit,' ',p.quantity,' ',p.unit_price) as product ,actual_consumption,forecastConsumption,month,year,concat(month,' ',year) as instance,forecast_id FROM pipeline.supply_plan sp inner join products p on (sp.product_id = p.id)) as s where instance In ("+buildInstance(instances)+") and forecast_id ="+forecastId+" group by s.product");
        List<Object[]> resultList  = query.getResultList();
        if(resultList.isEmpty()) {
            return null;
        }

        return resultList;
    }

    private String buildInstance (List<String> instances) {
        String str="";
        Integer count = 0;
        for(String instance : instances) {
            str+=instance;
            if((count+1) < instances.size()) {
                str+=",";
            }
            count++;
        }

        return str;
    }
}

package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.Forecast;
import ke.or.nascop.pipeline.wrappers.ForecastForm;
import ke.or.nascop.pipeline.wrappers.MonthYear;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by robinson on 2/28/16.
 */

@Repository
public class ForecastDaoImpl implements ForecastDao {
    @PersistenceContext
    private EntityManager entityManager;

    public void add (Forecast forecast) {
        entityManager.persist(forecast);
        entityManager.flush();
    }

    public Forecast getById (Integer id) {
        TypedQuery<Forecast> query = entityManager.createQuery("SELECT f FROM Forecast f WHERE f.id = :id AND (f.deletedBy IS NULL AND f.deletedOn IS NULL) ",Forecast.class);
        query.setParameter("id", id);
        List<Forecast> result = query.getResultList();
        if(! result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }

    public List<Forecast> getByProgram (Integer programId) {
        TypedQuery<Forecast> query = entityManager.createQuery("SELECT DISTINCT  f FROM Forecast f JOIN f.supplyPlans s JOIN s.product p JOIN p.program pro WHERE pro.id = :programId AND (f.deletedBy IS NULL AND f.deletedOn IS NULL) ",Forecast.class);
        query.setParameter("programId",programId);
        List<Forecast> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }

    public Forecast getByStartingAndEndingPeriod(Integer startingMonth,Integer startingYear,Integer endingMonth,Integer endingYear,Integer programId) {
        TypedQuery<Forecast> query = entityManager.createQuery("SELECT DISTINCT f FROM Forecast f JOIN f.supplyPlans s  JOIN s.product p JOIN p.program pro WHERE f.startingMonth = :startingMonth AND f.startingYear =:startingYear AND f.endingMonth = :endingMonth AND f.endingYear = :endingYear AND pro.id = :programId AND (f.deletedBy IS NULL AND f.deletedOn IS NULL) ",Forecast.class);
        query.setParameter("startingMonth",startingMonth);
        query.setParameter("startingYear",startingYear);
        query.setParameter("endingMonth", endingMonth);
        query.setParameter("endingYear", endingYear);
        query.setParameter("programId", programId);
        List<Forecast> result = query.getResultList();
        if(! result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }

    public List<MonthYear> getMonthYears (Integer forecastId) {
        TypedQuery<MonthYear> query = entityManager.createQuery("SELECT DISTINCT new ke.or.nascop.pipeline.wrappers.MonthYear(s.month,s.year) FROM Forecast f JOIN f.supplyPlans s WHERE f.id = :forecastId ORDER BY s.month,s.year",MonthYear.class);
        query.setParameter("forecastId",forecastId);
        List<MonthYear> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }

    public List<Forecast> getAll () {
        TypedQuery<Forecast> query = entityManager.createQuery("SELECT f FROM Forecast f WHERE f.deletedBy IS NULL AND f.deletedOn IS NULL ",Forecast.class);
        List<Forecast> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }

    public Forecast getCurrent(Integer programId) {
        TypedQuery<Forecast> query = entityManager.createQuery("SELECT f FROM Forecast f WHERE f.current = true AND f.deletedBy IS NULL AND f.deletedOn IS NULL ",Forecast.class);
        List<Forecast> result = query.getResultList();
        if(! result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }

    public void update (Forecast forecast) {
        entityManager.merge(forecast);
        entityManager.flush();
    }

    public void delete (Forecast forecast) {
        entityManager.merge(forecast);
        entityManager.flush();
    }
}

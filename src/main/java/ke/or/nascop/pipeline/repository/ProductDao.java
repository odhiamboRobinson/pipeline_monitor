package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.Product;

import java.util.List;

/**
 * Created by robinson on 2/20/16.
 */
/*The ProductDao interface declares methods
 *that must be implemented by a class interacting with the data source
 */
public interface ProductDao {
    void add (Product product);
    Product getById (Integer id);
    Product getByNameAndUnitAndQuantity(String name,String Unit,Double quantity);
    List<Product> getAll(Integer programId);
    List<Product> getByProgramId(Integer id);
    List<Product> search(String pattern);
    void update (Product product);
    void delete (Product product);
}

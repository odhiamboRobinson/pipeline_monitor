package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.model.UserGroup;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by robinson on 2/7/16.
 */
/*
 *The userGroupDao interface is used to offer an integration point between
 * the upper layer (user Group service impl)
*/
@Transactional
public interface UserGroupDao {
    void save (UserGroup userGroup);
    List<UserGroup> getAll ();
    List<User> getGroupMembersByGroupName (String name);
    UserGroup getById (int id);
    UserGroup getByName (String name);
    void update (UserGroup userGroup);
    void delete (UserGroup userGroup);
}

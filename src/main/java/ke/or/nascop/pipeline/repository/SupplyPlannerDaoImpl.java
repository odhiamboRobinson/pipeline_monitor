package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.SupplyPlan;
import ke.or.nascop.pipeline.wrappers.MonthYear;
import ke.or.nascop.pipeline.wrappers.MosGraph;
import ke.or.nascop.pipeline.wrappers.SupplyPlanJson;
import ke.or.nascop.pipeline.wrappers.SupplyPlanProduct;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 2/20/16.
 */
/*
 *The SupplyPlanPlannerDaoImpl interacts with the datasource
 * to perform crud operations on it.
 * (It implements the methods defined in the SupplyPlannerDao interface)
 */
@Repository
public class SupplyPlannerDaoImpl implements SupplyPlannerDao {
    @PersistenceContext
    private EntityManager entityManager;

    public void add (SupplyPlan supplyPlan) {
        entityManager.persist(supplyPlan);
        entityManager.flush();
    }

    public void add (List<SupplyPlan> supplyPlans) {
        for(SupplyPlan supplyPlan:supplyPlans) {
            add(supplyPlan);
        }
    }

    public SupplyPlan getById (Integer id) {
        TypedQuery<SupplyPlan> query = entityManager.createQuery("SELECT s FROM SupplyPlan s WHERE s.id = :id AND (s.deletedBy IS NULL AND s.deletedOn IS NULL) ",SupplyPlan.class);
        query.setParameter("id", id);
        List<SupplyPlan> result = query.getResultList();
        if(! result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }


    public List<SupplyPlanJson> getAll () {
        TypedQuery<SupplyPlanJson> query = entityManager.createQuery("SELECT new ke.or.nascop.pipeline.wrappers.SupplyPlanJson(s.id,s.forecastConsumption," +
                "            s.forecastFactor," +
                "            s.initialStockOnHandNationalStores," +
                "            s.initialStockOnHandSubNationalStores," +
                "            s.month," +
                "            s.year," +
                "            s.product," +
                "            s.openingStockBalance," +
                "            s.centralIssues," +
                "            s.expectedDeliveries," +
                "            s.actualConsumption," +
                "            s.desiredAdditionalQuantity," +
                "            s.calculatedClosingStock," +
                "            s.actualClosingStockCentralStores," +
                "            s.actualClosingStockPeripheralStores," +
                "            s.totalActualClosingStock," +
                "            s.monthsOfStockCentralStores," +
                "            s.monthsOfStockPeripheralStores," +
                "            s.totalMonthsOfStock," +
                "            s.actualConsumptionOverride," +
                "            s.centralConsumptionOverride," +
                "            s.expectedDeliveriesOverride," +
                "            s.actualClosingStockCentralStoresOverride," +
                "            s.actualClosingStockPeripheralStoresOverride," +
                "            s.maximumStockQuantity," +
                "            s.minimumStockQuantity) FROM SupplyPlan s WHERE s.deletedBy IS NULL AND s.deletedOn IS NULL ",SupplyPlanJson.class);
        List<SupplyPlanJson> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }


    public List<SupplyPlanProduct> getSummaryPerProduct(Integer forecastId) {
        Query query =entityManager.createNativeQuery("select distinct s.id,concat(p.name,' ',p.quantity,p.unit,' ',p.unit_price) as product from products p inner join supply_plan s on (p.id = s.product_id) where s.forecast_id  = :forecastId and s.prev_id is null");
        query.setParameter("forecastId",forecastId);

        List<Object[]> results = query.getResultList();
        List<SupplyPlanProduct> supplyPlanProducts = new ArrayList<SupplyPlanProduct>();
        for (Object[] row : results) {
            SupplyPlanProduct supplyPlanProduct = new SupplyPlanProduct();
            supplyPlanProduct.setId((Integer)row[0]);
            supplyPlanProduct.setProductName((String)row[1]);
            supplyPlanProducts.add(supplyPlanProduct);
        }
        return supplyPlanProducts;
    }

    public SupplyPlanProduct getSummary(Integer forecastId,Integer minMos,Integer maxMos) {
        Query query =entityManager.createNativeQuery("select sup.id,s.*,s2.*,s3.*,s4.* from supply_plan sup,(select count(*) as out_of_stock from supply_plan s inner join forecasts f on (f.id = s.forecast_id) where s.total_months_of_stock =0 and f.id = :forecastId) s,(select count(*) as below_min_mos from supply_plan s inner join forecasts f on (f.id = s.forecast_id) where s.total_months_of_stock < :minMos and s.total_months_of_stock > 0  and f.id = :forecastId) s2,(select count(*) as within_min_and_max_mos from supply_plan s inner join forecasts f on (f.id = s.forecast_id) where s.total_months_of_stock > :minMos and s.total_months_of_stock < :maxMos  and f.id = :forecastId) s3,(select count(*) as above_max_mos from supply_plan s inner join forecasts f on (f.id = s.forecast_id) where s.total_months_of_stock > :maxMos  and f.id = :forecastId) s4");
        query.setParameter("forecastId",forecastId);
        query.setParameter("minMos",minMos);
        query.setParameter("maxMos",maxMos);

        List<Object[]> results = query.getResultList();
        SupplyPlanProduct supplyPlanProduct = new SupplyPlanProduct();
        for (Object[] row : results) {
            supplyPlanProduct.setId((Integer)row[0]);
            BigInteger temp = (BigInteger)row[1];
            if(temp != null) {
                supplyPlanProduct.setStockOut(temp.longValue());
            } else {
                supplyPlanProduct.setStockOut(0l);
            }

            temp = (BigInteger)row[2];
            if(temp != null) {
                supplyPlanProduct.setBelowMinMOS(temp.longValue());
            } else {
                supplyPlanProduct.setBelowMinMOS(0l);
            }

            temp = (BigInteger)row[3];
            if(temp != null) {
                supplyPlanProduct.setWithinMinAndMaxMOS(temp.longValue());
            } else {
                supplyPlanProduct.setWithinMinAndMaxMOS(0l);
            }

            temp = (BigInteger)row[4];

            if(temp != null) {
                supplyPlanProduct.setAboveMaxMOS(temp.longValue());
            } else {
                supplyPlanProduct.setAboveMaxMOS(0l);
            }

        }
        return supplyPlanProduct;
    }

    public List<SupplyPlan> search (String pattern) {
        TypedQuery<SupplyPlan> query = entityManager.createQuery("SELECT s FROM SupplyPlan s WHERE s.name LIKE :pattern AND (s.deletedBy IS NULL AND s.deletedOn IS NULL) ",SupplyPlan.class);
        query.setParameter("pattern",pattern);
        List<SupplyPlan> result = query.getResultList();
        if(! result.isEmpty()) {
            return result;
        }

        return null;
    }

    public void update (SupplyPlan supplyPlan) {
        entityManager.merge(supplyPlan);
        entityManager.flush();
    }

    public void update (List<SupplyPlan> supplyPlans) {
        for(SupplyPlan supplyPlan:supplyPlans) {
            update(supplyPlan);
        }
    }

    public void delete (SupplyPlan supplyPlan) {
        entityManager.merge(supplyPlan);
        entityManager.flush();
    }

    public List<Object[]> mosByProduct (Integer productId,Integer programId,Integer forecastId,Integer period,Boolean programOverride) {
        String sql;
        if(!programOverride) {
          sql = "select s.total_months_of_stock,pro.minimum_months_of_stock,pro.maximum_months_of_stock,s.month,p.name,s.year from supply_plan s inner join products p on (s.product_id = p.id) inner join programs pro on (p.program_id = pro.id) where p.id = :productId AND p.program_id = :programId AND forecast_id = :forecastId ORDER BY s.id DESC,s.month LIMIT :period";
        } else {
          sql = "select s.total_months_of_stock,p.minimum_months_of_stock,p.maximum_months_of_stock,s.month,p.name,s.year from supply_plan s inner join products p on (s.product_id = p.id) inner join programs pro on (p.program_id = pro.id) where p.id = :productId AND p.program_id = :programId AND forecast_id = :forecastId ORDER BY s.id DESC,s.month LIMIT :period";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("productId",productId);
        query.setParameter("programId",programId);
        query.setParameter("forecastId",forecastId);
        query.setParameter("period",period);
        

        return query.getResultList();

    }
}

package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.Authority;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by robinson on 2/7/16.
 */
/*
 *The authorityDaoImpl class interacts with the database to affect
 * the underlying information in the database
*/
@Repository
public class AuthorityDaoImpl implements AuthorityDao {
    @PersistenceContext
    EntityManager entityManager;

    public List<Authority> getAll() {
        TypedQuery<Authority> query = entityManager.createQuery("SELECT a FROM Authority a ",Authority.class);
        List<Authority> userGroupAuthorities = query.getResultList();

        if(userGroupAuthorities.isEmpty()) {
            return null;
        }

        return userGroupAuthorities;
    }

    public List<Authority> getAuthoritiesByGroupId (int id) {
        TypedQuery<Authority> query = entityManager.createQuery("SELECT ug FROM UserGroup ug IN (ug.authorities) as a WHERE a.id = :id ",Authority.class);
        List<Authority> userGroupAuthorities = query.getResultList();

        if(userGroupAuthorities.isEmpty()) {
            return null;
        }

        return userGroupAuthorities;
    }

    public Authority getById(int id) {
        TypedQuery<Authority> query = entityManager.createQuery("SELECT a FROM Authority a WHERE a.id = :id ",Authority.class);
        query.setParameter("id",id);
        return query.getSingleResult();
    }

}

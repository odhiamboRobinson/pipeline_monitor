package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.Forecast;
import ke.or.nascop.pipeline.wrappers.MonthYear;

import java.util.List;

/**
 * Created by robinson on 2/28/16.
 */
public interface ForecastDao {
    void add (Forecast product);
    Forecast getById (Integer id);
    Forecast getByStartingAndEndingPeriod(Integer startingMonth,Integer startingYear,Integer endingMonth,Integer endingYear,Integer programId);
    List<Forecast> getByProgram(Integer programId);
    List<MonthYear> getMonthYears(Integer forecastId);
    Forecast getCurrent(Integer programId);
    List<Forecast> getAll();
    void update (Forecast product);
    void delete (Forecast product);
}

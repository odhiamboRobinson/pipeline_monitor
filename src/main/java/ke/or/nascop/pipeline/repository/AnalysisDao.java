package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.wrappers.MonthYear;

import java.util.List;

/**
 * Created by robinson on 3/6/16.
 */
public interface AnalysisDao {
    List<Object[]> mosByProduct(Integer forecastId,MonthYear monthYear);
    List<Object[]> mosPerProductOverTime (Integer forecastId,Integer productId);
    List<Object[]> centralMosPerProduct(Integer forecastId,MonthYear monthYear);
    List<Object[]> peripheralMosPerProduct(Integer forecastId,MonthYear monthYear);
    List<Object[]> forecastErrorPerProduct(Integer forecastId,List<String> instances);
}

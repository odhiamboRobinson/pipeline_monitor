package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by robinson on 2/7/16.
 */

/*
 *The userDao interface is used to offer an integration point between
 * the upper layer (userServiceImpl)
*/
public interface UserDao {
    void save (User user);
    User getById (int id);
    User getByIdNumber (String idNumber);
    User getByEmail (String email);
    List<User> getAll ();
    List<User> search (String pattern);
    void update (User user);
    void delete (User user);
}

package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.model.UserGroup;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

/**
 * Created by robinson on 2/7/16.
 */
/*
 *The userGroupDaoImpl class interacts with the database to affect
 * the underlying information in the database
*/
@Repository
public class UserGroupDaoImpl implements UserGroupDao {
    @PersistenceContext
    private EntityManager entityManager;

    public void save (UserGroup userGroup) {
        entityManager.persist(userGroup);
        entityManager.flush();
    }

    public List<UserGroup> getAll () {
        TypedQuery<UserGroup> query = entityManager.createQuery("SELECT ug FROM UserGroup ug WHERE (ug.deletedBy IS NULL AND ug.deletedOn IS NULL) ",UserGroup.class);
        List<UserGroup> userGroups = query.getResultList();
        if(userGroups.isEmpty()) {
            return null;
        }

        return userGroups;
    }

    public List<User> getGroupMembersByGroupName (String name) {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u, IN (u.userGroups) AS ug WHERE ug.name = :name AND (ug.deletedBy IS NULL AND ug.deletedOn IS NULL)",User.class);
        query.setParameter("name",name);
        List<User> users = query.getResultList();
        if(users.isEmpty()) {
            return null;
        }

        return users;
    }

    public UserGroup getById (int id) {
        TypedQuery<UserGroup> query = entityManager.createQuery("SELECT ug FROM UserGroup ug WHERE ug.id = :id AND (ug.deletedBy IS NULL AND ug.deletedOn IS NULL)",UserGroup.class);
        query.setParameter("id",id);
        List<UserGroup> result = query.getResultList();
        if(result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }

    public UserGroup getByName (String name) {
        TypedQuery<UserGroup> query = entityManager.createQuery("SELECT ug FROM UserGroup ug WHERE ug.name = :name AND (ug.deletedBy IS NULL AND ug.deletedOn IS NULL)",UserGroup.class);
        query.setParameter("name",name);
        List<UserGroup> result = query.getResultList();
        if(result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }

    public void update (UserGroup userGroup) {
        entityManager.merge(userGroup);
        entityManager.flush();
    }

    public void delete (UserGroup userGroup) {
        update(userGroup);
    }
}

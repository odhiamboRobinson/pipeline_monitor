package ke.or.nascop.pipeline.repository;

import ke.or.nascop.pipeline.model.Program;

import java.util.List;

/**
 * Created by robinson on 2/20/16.
 */
/*The ProgramDao interface declares methods
 *that must be implemented by a class interacting with the data source
 */
public interface ProgramDao {
    void add (Program program);
    Program getById (Integer id);
    Program getByName (String name);
    List<Program> getAll();
    List<Program> search(String pattern);
    void update (Program program);
    void delete (Program program);
}

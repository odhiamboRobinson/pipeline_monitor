package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.util.DuplicateEntityException;
import ke.or.nascop.pipeline.util.IllegalOperationException;
import ke.or.nascop.pipeline.wrappers.UserForm;
import ke.or.nascop.pipeline.wrappers.UserPassword;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

/**
 * Created by robinson on 2/7/16.
 */

public interface UserService {
    @Secured(value="ROLE_ADD_USER")
    void add (UserForm userForm)throws Exception;
    @Secured(value="ROLE_VIEW_USER")
    @Cacheable(value="users")
    List<User> getAll ();
    @Secured(value="ROLE_VIEW_USER")
    List<User> search (String pattern);
    @Secured(value="ROLE_VIEW_USER")
    @Cacheable(value="userById")
    User getById (int id);
    @Secured(value="ROLE_VIEW_USER")
    @Cacheable(value="userByEmail")
    User getByEmail (String email);
    @Secured(value="ROLE_VIEW_USER")
    @Cacheable(value="userByIdNumber")
    User getByIdNumber(String idNumber);
    @Secured(value="ROLE_EDIT_USER")
    void edit (UserForm userForm,Integer id);
    @Secured(value="ROLE_DELETE_USER")
    @CacheEvict(value = {"users","userByEmail","userByIdNumber","usersByPage","userById"})
    void delete (int id) throws IllegalOperationException;
    void changePassword (UserPassword userPassword);
    void recoverPassword (String email);
}

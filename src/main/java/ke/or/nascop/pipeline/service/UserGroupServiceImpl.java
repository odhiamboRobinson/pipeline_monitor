package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Authority;
import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.model.UserGroup;
import ke.or.nascop.pipeline.repository.*;
import ke.or.nascop.pipeline.util.*;
import ke.or.nascop.pipeline.wrappers.UserGroupForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by robinson on 2/8/16.
 */
@Service
@Transactional
public class UserGroupServiceImpl implements UserGroupService {
    private UserDao userDao;
    private UserGroupDao userGroupDao;
    private AuthorityDao authorityDao;

    @Autowired
    public void setUserDao (UserDao userDao) {
        this.userDao = userDao;
    }

    @Autowired
    public void setAuthorityDao (AuthorityDao authorityDao) {
        this.authorityDao = authorityDao;
    }

    @Autowired
    public void setUserGroupDao (UserGroupDao userGroupDao) {
        this.userGroupDao = userGroupDao;
    }

    public void add (UserGroupForm userGroupForm) throws DuplicateEntityException {
        UserGroup userGroup = userGroupDao.getByName(userGroupForm.getName());

        if(userGroup != null) {
            throw new DuplicateEntityException("User group with the following user group  : "+userGroupForm.getName()+" already exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);
        userGroup = new UserGroup();
        userGroup.setName(userGroupForm.getName());
        Set<Integer> ids = new HashSet<Integer>(userGroupForm.getAuthorityIds());
        Set<Authority> authorities = new HashSet<Authority>();
        for(Integer id : ids) {
            authorities.add(authorityDao.getById(id));
        }
        userGroup.setAuthorities(authorities);
        userGroup.setEnabled(userGroupForm.getEnabled());
        userGroup.setCreatedBy(currentUser.getId());
        userGroup.setCreatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        userGroupDao.save(userGroup);
    }

    public List<UserGroup> getAll () {
        return userGroupDao.getAll();
    }


    public List<User> getGroupMembersByGroupName (String name) {
        return userGroupDao.getGroupMembersByGroupName(name);
    }


    public UserGroup getById (int id) {
        return userGroupDao.getById(id);
    }

    public void edit (UserGroupForm userGroupForm,Integer id) throws DomainEntityNotFoundException {
        UserGroup userGroup = userGroupDao.getById(id);

        if(userGroup == null) {
            throw new DomainEntityNotFoundException("The user group does not  exists");
        }
        User currentUser = new CurrentUser().getUser(userDao);
        userGroup.setName(userGroupForm.getName());
        userGroupDao.update(userGroup);
        Set<Integer> authoritiesIds = new HashSet<Integer>(userGroupForm.getAuthorityIds());
        Set<Authority> authorities = new HashSet<Authority>();
        for(Integer authorityId : authoritiesIds) {
            authorities.add(authorityDao.getById(authorityId));
        }
        userGroup.setAuthorities(authorities);
        userGroup.setEnabled(userGroupForm.getEnabled());
        userGroup.setUpdatedBy(currentUser.getId());
        userGroup.setUpdatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));

        userGroupDao.update(userGroup);
    }

    public void delete (int id) throws UserNotFoundException,DomainEntityNotFoundException {
        UserGroup userGroup = userGroupDao.getById(id);

        if(userGroup == null) {
            throw new DomainEntityNotFoundException("The user group does not exists");
        }
        User currentUser = new CurrentUser().getUser(userDao);
        userGroup.setDeletedBy(currentUser.getId());
        userGroup.setDeletedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        userGroupDao.update(userGroup);
    }
}

package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Authority;
import ke.or.nascop.pipeline.util.DomainEntityNotFoundException;
import ke.or.nascop.pipeline.util.DuplicateEntityException;
import ke.or.nascop.pipeline.util.UserNotFoundException;
import org.springframework.security.access.annotation.Secured;
import java.util.List;

/**
 * Created by robinson on 2/8/16.
 */
/*
 *The authorityservice interface is used to offer an integration point between
 * the upper layer (authorityControler)
*/
public interface AuthorityService {
    @Secured(value = "ROLE_VIEW_AUTHORITY")
    List<Authority> getAll();
    @Secured(value = "ROLE_VIEW_AUTHORITY")
    List<Authority> getAll (int page);
    @Secured(value="ROLE_VIEW_AUTHORITY")
    List<Authority> getAuthoritiesByGroupId(int id);
    @Secured(value="ROLE_VIEW_AUTHORITY")
    List<Authority> getAuthoritiesByGroupId(int id,int page);
    @Secured(value="ROLE_VIEW_AUTHORITY")
    Authority getById(int id);
}

package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Program;
import ke.or.nascop.pipeline.wrappers.ProgramForm;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

/**
 * Created by robinson on 2/21/16.
 */
public interface ProgramService {
    @Secured("ROLE_ADD_PROGRAM")
    void add (ProgramForm programForm);
    List<Program> getAll ();
    @Secured("ROLE_VIEW_PROGRAM")
    Program getById(Integer id);
    @Secured("ROLE_EDIT_PROGRAM")
    void edit(ProgramForm programForm,Integer id);
    @Secured("ROLE_DELETE_PROGRAM")
    void delete(Integer id);
}

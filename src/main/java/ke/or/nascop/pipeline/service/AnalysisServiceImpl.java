package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.repository.AnalysisDao;
import ke.or.nascop.pipeline.util.Month;
import ke.or.nascop.pipeline.wrappers.DataPoint;
import ke.or.nascop.pipeline.wrappers.MonthYear;
import ke.or.nascop.pipeline.wrappers.MosGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 3/6/16.
 */
@Service
@Transactional
public class AnalysisServiceImpl implements AnalysisService {
    private AnalysisDao analysisDao;

    @Autowired
    public void setAnalysisDao (AnalysisDao analysisDao) {
        this.analysisDao = analysisDao;
    }

    public MosGraph mosByProduct(Integer forecastId,MonthYear monthYear) {
        return graphBuilder(analysisDao.mosByProduct(forecastId,monthYear),"bar","MOS BY PRODUCT FOR","#1A8CFF",monthYear);
    }
    public List<MosGraph> mosPerProductOverTime (Integer forecastId,Integer productId){
        List<MosGraph> mosGraphs = new ArrayList<MosGraph>();
        List<Object[]> results = analysisDao.mosPerProductOverTime(forecastId,productId);
        for(Object[] row : results) {
            MosGraph mosGraph = new MosGraph();
            mosGraph.setType("line");
            mosGraph.setColor("#00CCFF");
            mosGraph.setLegendText((String)row[0]);
            DataPoint dataPoint = new DataPoint();
            dataPoint.setX((Double)row[1]);
            //dataPoint.setX((String)row[2]);
            List<DataPoint> dataPoints = new ArrayList<DataPoint>();
            dataPoints.add(dataPoint);
            mosGraph.setDataPoints(dataPoints);
        }

        return mosGraphs;
    }
    public List<MosGraph> centralAndPeripheralMosPerProduct(Integer forecastId,MonthYear monthYear) {
        List<MosGraph> mosGraphs = new ArrayList<MosGraph>();
        mosGraphs.add(graphBuilder(analysisDao.centralMosPerProduct(forecastId, monthYear), "bar", "MOS BY CENTRAL STORES ", "#1A8CFF", monthYear));
        mosGraphs.add(graphBuilder(analysisDao.peripheralMosPerProduct(forecastId, monthYear),"bar","MOS BY PERIPHERAL STORES ","#B33C00",monthYear));
        return mosGraphs;
    }
    public MosGraph forecastErrorPerProduct(Integer forecastId,List<MonthYear> monthYears) {
        List<String> instances = new ArrayList<String>();
        for(MonthYear monthYear : monthYears) {
            instances.add(monthYear.toString());
        }
        return graphBuilder(analysisDao.forecastErrorPerProduct(forecastId, instances),"bar","FORECAST ERROR","#1A8CFF",null);
    }

    private MosGraph graphBuilder (List<Object[]> results,String type,String legend,String color,MonthYear monthYear) {
        List<Month> months = new ArrayList<Month>();
        months.add(new Month(1,"January"));
        months.add(new Month(2,"February"));
        months.add(new Month(3,"March"));
        months.add(new Month(4,"April"));
        months.add(new Month(5,"May"));
        months.add(new Month(6,"June"));
        months.add(new Month(7,"July"));
        months.add(new Month(8,"August"));
        months.add(new Month(9,"September"));
        months.add(new Month(10,"October"));
        months.add(new Month(11,"November"));
        months.add(new Month(12,"December"));

        MosGraph mosGraph = new MosGraph();
        List<DataPoint> dataPoints = new ArrayList<DataPoint>();
        if(results != null) {
            mosGraph.setType(type);
            String monthName = "";
            if(monthYear != null) {
                for(Month month:months) {
                    if(month.getIndex() == monthYear.getMonth()) {
                        monthName = month.getName()+" ";
                    }
                }
                mosGraph.setLegendText(legend+" "+monthName+monthYear.getYear());
            }

            mosGraph.setLegendText(legend);
            mosGraph.setColor(color);
            for (Object[] row : results) {
                DataPoint dataPoint = new DataPoint();
                dataPoint.setLabel((String) row[0]);
                dataPoint.setY((Double) row[1]);
                dataPoints.add(dataPoint);
            }
            mosGraph.setDataPoints(dataPoints);
        }

        return mosGraph;
    }
}

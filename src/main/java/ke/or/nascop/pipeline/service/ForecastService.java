package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Forecast;
import ke.or.nascop.pipeline.wrappers.ForecastForm;
import ke.or.nascop.pipeline.wrappers.MonthYear;
import org.springframework.security.access.annotation.Secured;

import java.util.List;
import java.util.Set;


/**
 * Created by robinson on 2/28/16.
 */
public interface ForecastService {
    @Secured("ROLE_ADD_FORECAST")
    void add (ForecastForm forecastForm,Integer programId);
    @Secured("ROLE_VIEW_ANALYSIS")
    Set<MonthYear> getMonthYears(Integer forecastId);
    @Secured("ROLE_VIEW_FORECAST")
    ForecastForm getById (Integer id);
    @Secured("ROLE_VIEW_FORECAST")
    Forecast getForecast(Integer id);
    @Secured("ROLE_VIEW_FORECAST")
    List<ForecastForm> getByProgram (Integer id);
    @Secured("ROLE_EDIT_FORECAST")
    void edit(ForecastForm forecastForm,Integer id);
    @Secured("ROLE_DELETE_FORECAST")
    void delete(Integer id);
    Forecast getCurrentForecast(Integer programId);
}

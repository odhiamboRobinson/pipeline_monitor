package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.wrappers.MonthYear;
import ke.or.nascop.pipeline.wrappers.MosGraph;

import java.util.List;

/**
 * Created by robinson on 3/6/16.
 */
public interface AnalysisService {
    MosGraph mosByProduct(Integer forecastId,MonthYear monthYear);
    List<MosGraph> mosPerProductOverTime (Integer forecastId,Integer productId);
    List<MosGraph> centralAndPeripheralMosPerProduct(Integer forecastId,MonthYear monthYear);
    MosGraph forecastErrorPerProduct(Integer forecastId,List<MonthYear> monthYears);
}

package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Forecast;
import ke.or.nascop.pipeline.model.Product;
import ke.or.nascop.pipeline.model.SupplyPlan;
import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.repository.ForecastDao;
import ke.or.nascop.pipeline.repository.ProductDao;
import ke.or.nascop.pipeline.repository.UserDao;
import ke.or.nascop.pipeline.util.Calculator;
import ke.or.nascop.pipeline.util.CurrentUser;
import ke.or.nascop.pipeline.util.DuplicateEntityException;
import ke.or.nascop.pipeline.wrappers.ForecastForm;
import ke.or.nascop.pipeline.wrappers.MonthYear;
import ke.or.nascop.pipeline.wrappers.ProductForecast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by robinson on 2/28/16.
 */
@Service
@Transactional
public class ForecastServiceImpl implements ForecastService {
    private ForecastDao forecastDao;
    private UserDao userDao;
    private ProductDao productDao;

    @Autowired
    public void setForecastDao(ForecastDao forecastDao) {
        this.forecastDao = forecastDao;
    }
    @Autowired
    public void setUserDao(UserDao userDao) {this.userDao = userDao;}
    @Autowired
    public void setProductDao(ProductDao productDao) {this.productDao = productDao;}

    public void add(ForecastForm forecastForm,Integer programId) {
        Integer NUM_MONTHS = 12;
        Integer START_MONTH = 1;
        Forecast forecast = forecastDao.getByStartingAndEndingPeriod(forecastForm.getStartingMonth(), forecastForm.getStartingYear(), forecastForm.getEndingMonth(), forecastForm.getEndingYear(),programId);

        if(forecast != null) {
            throw new DuplicateEntityException("Forecast already exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);

        forecast = new Forecast();
        forecast.setEnabled(forecastForm.getEnabled());
        forecast.setStartingMonth(forecastForm.getStartingMonth());
        forecast.setStartingYear(forecastForm.getStartingYear());
        forecast.setEndingMonth(forecastForm.getEndingMonth());
        forecast.setEndingYear(forecastForm.getEndingYear());
        if(forecastForm.getCurrent()) {
           Forecast currentForecast = forecastDao.getCurrent(programId);
            if(currentForecast != null) {
                currentForecast.setCurrent(false);
                forecastDao.update(currentForecast);
            }
        }
        forecast.setCurrent(forecastForm.getCurrent());
        forecast.setCreatedBy(currentUser.getId());
        forecast.setCreatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));

        List<ProductForecast> productForecasts = forecastForm.getProductForecasts();
        for(ProductForecast productForecast : productForecasts) {
            Double forecastFactor = productForecast.getForecastFactor();
            Double forecastValue = productForecast.getForecastConsumption();
            Product product = productDao.getById(productForecast.getProductId());
            Integer year =  forecastForm.getStartingYear();
            Integer month = forecastForm.getStartingMonth();
            Integer endingYear = forecastForm.getEndingYear();
            Integer endingMonth = forecastForm.getEndingMonth();
            Integer years = endingYear - year;
            Integer months = (years*NUM_MONTHS)-(month-endingMonth)+1;
            SupplyPlan supplyPlanHead = new SupplyPlan();
            supplyPlanHead.setForecastFactor(forecastFactor);
            supplyPlanHead.setInitialStockOnHandNationalStores(productForecast.getInitialStockOnHandNationalStores());
            supplyPlanHead.setInitialStockOnHandSubNationalStores(productForecast.getInitialStockOnHandSubNationalStores());
            SupplyPlan supplyPlanPtr = supplyPlanHead;
            for(int count = 0;count< months;count++) {
                supplyPlanPtr.setForecastConsumption(forecastValue);
                supplyPlanPtr.setProduct(product);
                supplyPlanPtr.setMonth(month);
                supplyPlanPtr.setYear(year);
                supplyPlanPtr.setCreatedBy(currentUser.getId());
                supplyPlanPtr.setCreatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
                supplyPlanPtr.setEnabled(true);
                if((count+1) != months) {
                    SupplyPlan next = new SupplyPlan();
                    next.setPrev(supplyPlanPtr);
                    supplyPlanPtr.setNext(next);
                }
                supplyPlanPtr  = supplyPlanPtr.getNext();
                forecastValue+=(forecastValue*forecastFactor);
                if(month == NUM_MONTHS) {
                    month = START_MONTH;
                    year++;
                } else {
                    month++;
                }
            }

            forecast.setSupplyPlans(new HashSet<SupplyPlan>(Calculator.calculate(supplyPlanHead)));
        }

        forecastDao.add(forecast);
    }

    public ForecastForm getById(Integer id) {
        Forecast forecast = forecastDao.getById(id);
        ForecastForm forecastForm = new ForecastForm();
        forecastForm.setStartingMonth(forecast.getStartingMonth());
        forecastForm.setStartingYear(forecastForm.getStartingYear());
        forecastForm.setEndingMonth(forecast.getEndingMonth());
        forecastForm.setEndingYear(forecastForm.getEndingYear());
        Set<SupplyPlan> supplyPlans = forecast.getSupplyPlans();
        List<ProductForecast> productForecasts = new ArrayList<ProductForecast>();

        for(SupplyPlan supplyPlan : supplyPlans) {
            ProductForecast productForecast = new ProductForecast();
            productForecast.setProductId(supplyPlan.getId());
            productForecast.setForecastFactor(supplyPlan.getForecastFactor());
            productForecast.setForecastConsumption(supplyPlan.getForecastConsumption());
            productForecast.setInitialStockOnHandSubNationalStores(supplyPlan.getInitialStockOnHandSubNationalStores());
            productForecast.setInitialStockOnHandNationalStores(supplyPlan.getInitialStockOnHandNationalStores());
            productForecasts.add(productForecast);
        }

        forecastForm.setProductForecasts(productForecasts);

        return forecastForm;
    }

    public Forecast getForecast(Integer id) {
        return forecastDao.getById(id);
    }

    public List<ForecastForm> getByProgram(Integer programId) {
        List<Forecast> forecasts = forecastDao.getByProgram(programId);
        List<ForecastForm> forecastForms = new ArrayList<ForecastForm>();
        if(forecasts != null) {
            for (Forecast forecast :forecasts) {
                ForecastForm forecastForm = new ForecastForm();
                forecastForm.setId(forecast.getId());
                forecastForm.setStartingMonth(forecast.getStartingMonth());
                forecastForm.setStartingYear(forecast.getStartingYear());
                forecastForm.setEndingMonth(forecast.getEndingMonth());
                forecastForm.setEndingYear(forecast.getEndingYear());
                forecastForms.add(forecastForm);
            }
        }

        return forecastForms;
    }

    public Set<MonthYear> getMonthYears(Integer forecastId) {
        List<MonthYear> monthYears = forecastDao.getMonthYears(forecastId);
        Collections.sort(monthYears);
        return new HashSet<MonthYear>(monthYears);
    }

    public Forecast getCurrentForecast(Integer programId) {
        return forecastDao.getCurrent(programId);
    }

    public void edit (ForecastForm forecastForm,Integer id) {
        //to be implemented
    }

    public void delete (Integer id) {
        //to be implemented
    }
}

package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Program;
import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.model.UserGroup;
import ke.or.nascop.pipeline.repository.ProgramDao;
import ke.or.nascop.pipeline.repository.UserDao;
import ke.or.nascop.pipeline.repository.UserGroupDao;
import ke.or.nascop.pipeline.util.*;
import ke.or.nascop.pipeline.wrappers.UserForm;
import ke.or.nascop.pipeline.wrappers.UserPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by robinson on 2/7/16.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    private UserDao userDao;
    private UserGroupDao userGroupDao;
    private ProgramDao programDao;

    @Autowired
    public void setUserDao (UserDao userDao) {
        this.userDao = userDao;
    }

    @Autowired
    public void setUserGroupDao (UserGroupDao userGroupDao) {
        this.userGroupDao = userGroupDao;
    }

    @Autowired
    public void setProgramDao (ProgramDao programDao) {
        this.programDao = programDao;
    }


    public void add (UserForm userForm) throws Exception {
        User userInfo = userDao.getByEmail(userForm.getEmail());
        if(userInfo != null) {
            throw new DuplicateEntityException("user with the following email : "+userForm.getEmail()+" already exists");
        }
        User currentUser = new CurrentUser().getUser(userDao);
        Set<UserGroup> userGroups = new HashSet<UserGroup>();
        List<Integer> userGroupIds = userForm.getUserGroupIds();
        for (Integer userGroupId : userGroupIds) {
            userGroups.add(userGroupDao.getById(userGroupId));
        }

        Set<Program> programs = new HashSet<Program>();
        List<Integer> programIds = userForm.getProgramIds();
        for (Integer programId : programIds) {
            programs.add(programDao.getById(programId));
        }

        User user = new User();
        user.setEmail(userForm.getEmail());
        user.setIdNumber(userForm.getIdNumber());
        user.setOtherNames(userForm.getOtherNames());
        user.setPhoneNumber(userForm.getPhoneNumber());
        user.setUserGroups(userGroups);
        user.setPrograms(programs);
        user.setSirName(userForm.getSirName());
        user.setEnabled(userForm.getEnabled());
        String password = randomString();
        user.setPassword(new BCryptPasswordEncoder().encode(password));
        user.setCreatedBy(currentUser.getId());
        user.setCreatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        userDao.save(user);

        //send email
        String message = "Dear "+userForm.getSirName()+" \n";
        message+="You have been registered to use the pipeline monitoring system with the following credentials \n";
        message+="EMAIL :- "+userForm.getEmail();
        message+="PASSWORD :- "+password;
        Email.send("REGISTRATION TO PIPELINE MONITORING SYSTEM",userForm.getEmail(),message);

    }

    public List<User> getAll() {
        return userDao.getAll();
    }

    public List<User> search (String pattern) {
        return userDao.search(pattern);
    }

    public User getById (int id) {
        return userDao.getById(id);
    }


    public User getByEmail(String email) {
        return userDao.getByEmail(email);

    }

    public User getByIdNumber(String idNumber) {
        return userDao.getByIdNumber(idNumber);

    }

    public void edit (UserForm userForm,Integer id) throws UserNotFoundException {
        User user = userDao.getById(id);

        if(user == null) {
            throw new UserNotFoundException("The desired user does not  exists");
        }
        User currentUser = new CurrentUser().getUser(userDao);
        Set<UserGroup> userGroups = new HashSet<UserGroup>();
        List<Integer> userGroupIds = userForm.getUserGroupIds();
        for (Integer userGroupId : userGroupIds) {
            userGroups.add(userGroupDao.getById(userGroupId));
        }

        Set<Program> programs = new HashSet<Program>();
        List<Integer> programIds = userForm.getProgramIds();
        for (Integer programId : programIds) {
            programs.add(programDao.getById(programId));
        }

        user.setEnabled(userForm.getEnabled());
        user.setSirName(userForm.getSirName());
        user.setOtherNames(userForm.getOtherNames());
        user.setPhoneNumber(userForm.getPhoneNumber());
        user.setIdNumber(userForm.getIdNumber());
        user.setPrograms(programs);
        user.setEmail(userForm.getEmail());
        user.setUserGroups(userGroups);
        user.setEnabled(userForm.getEnabled());
        user.setUpdatedBy(currentUser.getId());
        user.setUpdatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));

        userDao.update(user);
    }


    public void delete (int id) throws IllegalOperationException {
        User user = userDao.getById(id);

        if(user == null) {
            throw new UserNotFoundException("The desired user does not  exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);
        int userId = currentUser.getId();

        if( userId == user.getId()) {
            throw new IllegalOperationException("You cannot delete your own account");
        }

        user.setDeletedBy(userId);
        user.setDeletedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        userDao.delete(user);
    }

    public void changePassword (UserPassword userPassword) {
        //check if password is correct before allowing it to be changed
        User user = new CurrentUser().getUser(userDao);
        user.setPassword(new BCryptPasswordEncoder().encode(userPassword.getPassword()));
        user.setUpdatedBy(user.getId());
        user.setUpdatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));

        userDao.update(user);

    }

    public void recoverPassword (String email) {
        User user = userDao.getByEmail(email);

    }

    private String randomString () {
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        int len = 10;
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ ) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }

}



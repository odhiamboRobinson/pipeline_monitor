package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.model.UserGroup;
import ke.or.nascop.pipeline.util.DomainEntityNotFoundException;
import ke.or.nascop.pipeline.util.DuplicateEntityException;
import ke.or.nascop.pipeline.wrappers.UserGroupForm;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

/**
 * Created by robinson on 2/8/16.
 */
public interface UserGroupService {
    @Secured(value="ROLE_ADD_USER_GROUP")
    void add (UserGroupForm userGroupForm) throws DuplicateEntityException;
    @Secured(value="ROLE_VIEW_USER_GROUP")
    @Cacheable(value="userGroups")
    List<UserGroup> getAll ();
    @Secured(value="ROLE_VIEW_USER_GROUP")
    @Cacheable(value="groupMembers")
    List<User> getGroupMembersByGroupName (String name);
    @Secured(value="ROLE_VIEW_USER_GROUP")
    @Cacheable(value="userGroupById")
    UserGroup getById (int id);
    @Secured(value="ROLE_EDIT_USER_GROUP")
    void edit (UserGroupForm userGroupForm,Integer id) throws DomainEntityNotFoundException;
    @Secured(value="ROLE_DELETE_USER_GROUP")
    void delete (int id)throws DomainEntityNotFoundException;
}

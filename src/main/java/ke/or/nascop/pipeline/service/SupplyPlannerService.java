package ke.or.nascop.pipeline.service;


import ke.or.nascop.pipeline.model.Forecast;
import ke.or.nascop.pipeline.model.Product;
import ke.or.nascop.pipeline.model.Program;
import ke.or.nascop.pipeline.wrappers.*;
import org.springframework.security.access.annotation.Secured;

import java.util.List;
import java.util.Set;

/**
 * Created by robinson on 2/21/16.
 */
public interface SupplyPlannerService {
    @Secured("ROLE_VIEW_SUPPLY_PLAN")
    List<SupplyPlanJson> getAll ();
    @Secured("ROLE_VIEW_SUPPLY_PLAN")
    List<SupplyPlanJson> getById(Integer id);
    @Secured("ROLE_VIEW_SUPPLY_PLAN")
    SupplyPlanProduct getSummary(Integer forecastId,Integer programId);
    @Secured("ROLE_VIEW_SUPPLY_PLAN")
    List<SupplyPlanProduct> getSummaryPerProduct(Integer forecastId);
    @Secured("ROLE_EDIT_SUPPLY_PLAN")
    void edit(SupplyPlanForm stockForm,Integer id);
    @Secured("ROLE_VIEW_SUPPLY_PLAN")
    List<MosGraph> mosByProduct(Product product,Integer programId,Integer forecastId,Integer period);
    @Secured("ROLE_DELETE_SUPPLY_PLAN")
    void delete(Integer id);
    @Secured("ROLE_EDIT_SUPPLY_PLAN")
    void override(SupplyPlanOverride supplyPlanOverride,Integer id);
}

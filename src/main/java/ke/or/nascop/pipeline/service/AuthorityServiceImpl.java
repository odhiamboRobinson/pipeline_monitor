package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Authority;
import ke.or.nascop.pipeline.repository.AuthorityDao;
import ke.or.nascop.pipeline.util.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * Created by robinson on 2/8/16.
 */
@Service
@Transactional
public class AuthorityServiceImpl implements AuthorityService {
    @Autowired
    private AuthorityDao authorityDao;

    private static final int PAGE_CHUNKS = 20;

    public void setAuthorityDaoImpl(AuthorityDao authorityDao) {
        this.authorityDao = authorityDao;
    }

    public List<Authority> getAll () {
        return authorityDao.getAll();
    }

    public List<Authority> getAll (int page) {
        HashMap<String,Object> hashMap = new HashMap<String, Object>();
        hashMap.put("UserGroupAuthorities",getAll());
        return  (List<Authority>)(Object)Pager.createPaging(hashMap,page);
    }

    public List<Authority> getAuthoritiesByGroupId(int id) {
        return authorityDao.getAuthoritiesByGroupId(id);
    }

    public List<Authority> getAuthoritiesByGroupId(int id,int page) {
        HashMap<String,Object> hashMap = new HashMap<String, Object>();
        hashMap.put("UserGroupAuthorities",getAll());
        return  (List<Authority>)(Object)Pager.createPaging(hashMap,page);
    }

    public Authority getById(int id) {
        return authorityDao.getById(id);
    }
}

package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Product;
import ke.or.nascop.pipeline.model.Program;
import ke.or.nascop.pipeline.model.SupplyPlan;
import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.repository.ProductDao;
import ke.or.nascop.pipeline.repository.ProgramDao;
import ke.or.nascop.pipeline.repository.SupplyPlannerDao;
import ke.or.nascop.pipeline.repository.UserDao;
import ke.or.nascop.pipeline.util.Calculator;
import ke.or.nascop.pipeline.util.CurrentUser;
import ke.or.nascop.pipeline.util.DomainEntityNotFoundException;
import ke.or.nascop.pipeline.util.Month;
import ke.or.nascop.pipeline.wrappers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by robinson on 2/21/16.
 */
/*
 *The SupplyPlannerServiceImpl implements the business logic of the
 * application in addition to executing data access methods from the
 * underlying dao layer
 */
@Service
@Transactional
public class SupplyPlannerServiceImpl implements SupplyPlannerService {

    private SupplyPlannerDao supplyPlannerDao;
    private UserDao userDao;
    private ProgramDao programDao;
    private List<Month> months;

    @Autowired
    public void setSupplyPlannerDao(SupplyPlannerDao supplyPlannerDao) {
        this.supplyPlannerDao = supplyPlannerDao;
    }
    @Autowired
    public void setUserDao(UserDao userDao) {this.userDao = userDao;}
    @Autowired
    public void setProgramDao(ProgramDao programDao) {this.programDao = programDao;}

    public SupplyPlannerServiceImpl() {
        months = new ArrayList<Month>();
        months.add(new Month(1,"January"));
        months.add(new Month(2,"February"));
        months.add(new Month(3,"March"));
        months.add(new Month(4,"April"));
        months.add(new Month(5,"May"));
        months.add(new Month(6,"June"));
        months.add(new Month(7,"July"));
        months.add(new Month(8,"August"));
        months.add(new Month(9,"September"));
        months.add(new Month(10,"October"));
        months.add(new Month(11,"November"));
        months.add(new Month(12,"December"));
    }

    public List<SupplyPlanJson> getAll () {
        return supplyPlannerDao.getAll();
    }

    public List<SupplyPlanJson> getById (Integer id) {
        SupplyPlan supplyPlan = supplyPlannerDao.getById(id);
        List<SupplyPlanJson> supplyPlanJsons = new ArrayList<SupplyPlanJson>();
        while(supplyPlan != null) {

            SupplyPlanJson supplyPlanJson = new SupplyPlanJson (
            supplyPlan.getId(),
            supplyPlan.getForecastConsumption(),
            supplyPlan.getForecastFactor(),
            supplyPlan.getInitialStockOnHandNationalStores(),
            supplyPlan.getInitialStockOnHandSubNationalStores(),
            supplyPlan.getMonth(),
            supplyPlan.getYear(),
            supplyPlan.getProduct(),
            supplyPlan.getOpeningStockBalance(),
            supplyPlan.getCentralIssues(),
            supplyPlan.getExpectedDeliveries(),
            supplyPlan.getActualConsumption(),
            supplyPlan.getDesiredAdditionalQuantity(),
            supplyPlan.getCalculatedClosingStock(),
            supplyPlan.getActualClosingStockCentralStores(),
            supplyPlan.getActualClosingStockPeripheralStores(),
            supplyPlan.getTotalActualClosingStock(),
            supplyPlan.getMonthsOfStockCentralStores(),
            supplyPlan.getMonthsOfStockPeripheralStores(),
            supplyPlan.getTotalMonthsOfStock(),
            supplyPlan.getActualConsumptionOverride(),
            supplyPlan.getCentralConsumptionOverride(),
            supplyPlan.getExpectedDeliveriesOverride(),
            supplyPlan.getActualClosingStockCentralStoresOverride(),
            supplyPlan.getActualClosingStockPeripheralStoresOverride(),
            supplyPlan.getMaximumStockQuantity(),
            supplyPlan.getMinimumStockQuantity()
            );

            supplyPlanJsons.add(supplyPlanJson);
            supplyPlan = supplyPlan.getNext();
        }

        return supplyPlanJsons;

    }

    public List<SupplyPlanProduct> getSummaryPerProduct(Integer forecastId) {
        return supplyPlannerDao.getSummaryPerProduct(forecastId);
    }

    public  SupplyPlanProduct getSummary(Integer forecastId,Integer programId) {
        return supplyPlannerDao.getSummary(forecastId,programDao.getById(programId).getMinimumMonthsOfStock(),programDao.getById(programId).getMaximumMonthsOfStock());
    }

    public void edit (SupplyPlanForm supplyPlanForm,Integer id) {
        Double FLAG = -1.0;
        SupplyPlan supplyPlan = supplyPlannerDao.getById(id);
        if(supplyPlan == null) {
            throw new DomainEntityNotFoundException("Supply Plan does not exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);
        supplyPlan.setUpdatedBy(currentUser.getId());
        supplyPlan.setUpdatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        Double expectedDeliveries = supplyPlanForm.getExpectedDeliveries();
        if(expectedDeliveries == null || !expectedDeliveries.equals(FLAG)) {
            supplyPlan.setExpectedDeliveries(expectedDeliveries);
        }
        Double actualConsumption = supplyPlanForm.getActualConsumption();
        if(actualConsumption == null || !actualConsumption.equals(FLAG)) {
            supplyPlan.setActualConsumption(actualConsumption);
        }

        Double actualClosingStockCentralStores = supplyPlanForm.getActualClosingStockCentralStores();
        if(actualClosingStockCentralStores == null || !actualClosingStockCentralStores.equals(FLAG)) {
            supplyPlan.setActualClosingStockCentralStores(actualClosingStockCentralStores);
        }

        Double actualClosingStockPeripheralStores = supplyPlanForm.getActualClosingStockPeripheralStores();
        if(actualClosingStockPeripheralStores == null || !actualClosingStockPeripheralStores.equals(FLAG)) {
            supplyPlan.setActualClosingStockPeripheralStores(actualClosingStockPeripheralStores);
        }

        Double centralIssues = supplyPlanForm.getCentralIssues();
        if(centralIssues == null || !centralIssues.equals(FLAG)) {
            supplyPlan.setCentralIssues(centralIssues);
        }

        supplyPlannerDao.update(Calculator.calculate(supplyPlan));
    }

    public void override(SupplyPlanOverride supplyPlanOverride,Integer id) {
        SupplyPlan supplyPlan = supplyPlannerDao.getById(id);
        if(supplyPlan == null) {
            throw new DomainEntityNotFoundException("Supply Plan does not exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);
        supplyPlan.setUpdatedBy(currentUser.getId());
        supplyPlan.setUpdatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));

        supplyPlan.setActualConsumptionOverride(supplyPlanOverride.getActualConsumptionOverride());
        supplyPlan.setExpectedDeliveriesOverride(supplyPlanOverride.getExpectedDeliveriesOverride());
        supplyPlan.setActualClosingStockCentralStoresOverride(supplyPlanOverride.getActualClosingStockCentralStoresOverride());
        supplyPlan.setActualClosingStockPeripheralStoresOverride(supplyPlanOverride.getActualClosingStockPeripheralStoresOverride());
        supplyPlannerDao.update(Calculator.calculate(supplyPlan));
    }

    public void delete (Integer id) {
        SupplyPlan supplyPlan = supplyPlannerDao.getById(id);
        if(supplyPlan == null) {
            throw new DomainEntityNotFoundException("Supply Plan does not exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);

        while(supplyPlan != null) {
            supplyPlan.setDeletedBy(currentUser.getId());
            supplyPlan.setDeletedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            supplyPlannerDao.delete(supplyPlan);
            supplyPlan = supplyPlan.getNext();
        }
    }

    public List<MosGraph> mosByProduct(Product product,Integer programId,Integer forecastId,Integer period) {
        Boolean programOverride = false;
        if(product.getMinimumMonthsOfStock() != null || product.getMaximumMonthsOfStock() != null) {
            programOverride = true;
        }

       List<Object[]> results = supplyPlannerDao.mosByProduct(product.getId(),programId,forecastId,period,programOverride);
       List<MosGraph> mosGraphData = new ArrayList<MosGraph>();
       for(Object[] row : results) {
           for(Object[] row2 : results) {
               MosGraph mosGraph = new MosGraph();
               List<DataPoint> dataPoints = new ArrayList<DataPoint>();
               if(row2[0] != null) {
                   DataPoint dataPoint = new DataPoint();
                   dataPoint.setX((Math.round((Double)row2[0] * 100.0 ) / 100.0));
                   for(Month month : months) {
                       if(month.getIndex() == row2[1] ) {
                           dataPoint.setLabel(month.getName());
                       }
                   }
                   dataPoints.add(dataPoint);
               }

               mosGraph.setType("line");
               mosGraph.setColor("green");
               mosGraph.setLegendText("Total MOS Over Time ("+product.getName()+")");
               mosGraph.setDataPoints(dataPoints);
               mosGraphData.add(mosGraph);
           }

           for(Object[] row2 : results) {
               MosGraph mosGraph = new MosGraph();
               List<DataPoint> dataPoints = new ArrayList<DataPoint>();
               if(row2[2] != null) {
                   DataPoint dataPoint = new DataPoint();
                   dataPoint.setX((Math.round((Integer)row2[2] * 100.0 ) / 100.0));
                   for(Month month : months) {
                       if(month.getIndex() == row2[1] ) {
                           dataPoint.setLabel(month.getName());
                       }
                   }
                   dataPoints.add(dataPoint);
               }

               mosGraph.setType("line");
               mosGraph.setColor("red");
               mosGraph.setDataPoints(dataPoints);
               mosGraphData.add(mosGraph);
           }

           for(Object[] row2 : results) {
               MosGraph mosGraph = new MosGraph();
               List<DataPoint> dataPoints = new ArrayList<DataPoint>();
               if(row2[3] != null) {
                   DataPoint dataPoint = new DataPoint();
                   dataPoint.setX((Math.round((Integer)row2[3] * 100.0 ) / 100.0));
                   for(Month month : months) {
                       if(month.getIndex() == row2[1] ) {
                           dataPoint.setLabel(month.getName());
                       }
                   }
                   dataPoints.add(dataPoint);
               }

               mosGraph.setType("line");
               mosGraph.setColor("green");
               mosGraph.setDataPoints(dataPoints);
               mosGraphData.add(mosGraph);
           }
       }
       return mosGraphData;
    }

}
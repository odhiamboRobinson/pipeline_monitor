package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.*;
import ke.or.nascop.pipeline.repository.ProgramDao;
import ke.or.nascop.pipeline.repository.UserDao;
import ke.or.nascop.pipeline.util.CurrentUser;
import ke.or.nascop.pipeline.util.DomainEntityNotFoundException;
import ke.or.nascop.pipeline.util.DuplicateEntityException;
import ke.or.nascop.pipeline.wrappers.ProgramForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by robinson on 2/21/16.
 */
/*
 *The ProgramServiceImpl implements the business logic of the
 * application in addition to executing data access methods from the
 * underlying dao layer
 */
@Service
@Transactional
public class ProgramServiceImpl implements  ProgramService {
    private ProgramDao programDao;
    private UserDao userDao;

    @Autowired
    public void setProgramDao (ProgramDao programDao) {
        this.programDao = programDao;
    }
    @Autowired
    public void setUserDao (UserDao userDao) {
        this.userDao = userDao;
    }

    public void add (ProgramForm programForm) {
        Program program = programDao.getByName(programForm.getName());

        if(program != null) {
            throw new DuplicateEntityException("Program already exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);
        program = new Program();
        Integer userId = currentUser.getId();
        Timestamp timeCreated = new Timestamp(Calendar.getInstance().getTimeInMillis());
        program.setName(programForm.getName());
        program.setMinimumMonthsOfStock(programForm.getMinimumMonthsOfStock());
        program.setMaximumMonthsOfStock(programForm.getMaximumMonthsOfStock());
        program.setCreatedBy(userId);
        program.setCreatedOn(timeCreated);
        program.setEnabled(programForm.getEnabled());
        programDao.add(program);
    }

    public List<Program> getAll () {
        return programDao.getAll();
    }

    public Program getById (Integer id) {
        return programDao.getById(id);
    }

    public void edit (ProgramForm programForm,Integer id) {
        Program program = programDao.getById(id);

        if(program == null) {
            throw new DomainEntityNotFoundException("Program does not exist");
        }

        Timestamp timeUpdated = new Timestamp(Calendar.getInstance().getTimeInMillis());
        User currentUser = new CurrentUser().getUser(userDao);
        Integer userId = currentUser.getId();
        program.setName(programForm.getName());
        program.setMinimumMonthsOfStock(programForm.getMinimumMonthsOfStock());
        program.setMaximumMonthsOfStock(programForm.getMaximumMonthsOfStock());
        program.setUpdatedBy(userId);
        program.setUpdatedOn(timeUpdated);
        program.setEnabled(programForm.getEnabled());
    }

    public void delete (Integer id) {
        Program program = programDao.getById(id);
        if(program == null) {
            throw new DomainEntityNotFoundException("Program does not exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);
        Integer userId = currentUser.getId();
        Timestamp timeDeleted = new Timestamp(Calendar.getInstance().getTimeInMillis());
        program.setDeletedBy(userId);
        program.setDeletedOn(timeDeleted);
        programDao.delete(program);
    }

}

package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Product;
import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.repository.ProductDao;
import ke.or.nascop.pipeline.repository.ProgramDao;
import ke.or.nascop.pipeline.repository.UserDao;
import ke.or.nascop.pipeline.util.Credentials;
import ke.or.nascop.pipeline.util.CurrentUser;
import ke.or.nascop.pipeline.util.DomainEntityNotFoundException;
import ke.or.nascop.pipeline.util.DuplicateEntityException;
import ke.or.nascop.pipeline.wrappers.ProductForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by robinson on 2/21/16.
 */
/*
 *The ProductServiceImpl implements the business logic of the
 * application in addition to executing data access methods from the
 * underlying dao layer
 */
@Service
@Transactional
public class ProductServiceImpl implements  ProductService {
    private ProductDao productDao;
    private UserDao userDao;
    private ProgramDao programDao;

    @Autowired
    public void setUserDao (UserDao userDao) {
        this.userDao = userDao;
    }

    @Autowired
    public void setProductDao (ProductDao productDao) {
        this.productDao = productDao;
    }

    @Autowired
    public void setProgramDao (ProgramDao programDao) {
        this.programDao = programDao;
    }

    public void add (ProductForm productForm,Integer programId) {
        Product product = productDao.getByNameAndUnitAndQuantity(productForm.getName(),productForm.getUnit(),productForm.getQuantity());

        if(product != null) {
            throw new DuplicateEntityException("Product already exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);
        product = new Product();
        product.setName(productForm.getName());
        product.setUnit(productForm.getUnit());
        product.setQuantity(productForm.getQuantity());
        product.setUnitPrice(productForm.getUnitPrice());
        product.setMinimumMonthsOfStock(productForm.getMinimumMonthsOfStock());
        product.setMaximumMonthsOfStock(productForm.getMaximumMonthsOfStock());
        product.setCreatedBy(currentUser.getId());
        product.setProgram(programDao.getById(programId));//This should change so that it is injected from session
        product.setCreatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        product.setEnabled(productForm.getEnabled());
        productDao.add(product);
    }

    public void add (Set<ProductForm> productForms,Integer programId) {
        for(ProductForm productForm : productForms) {
            add(productForm,programId);
        }
    }

    public List<Product> getAll (Integer programId) {
        return productDao.getAll(programId);
    }

    public Product getById (Integer id) {
        return productDao.getById(id);
    }

    public void edit (ProductForm productForm,Integer id) {
        Product product = productDao.getById(id);
        if(product == null) {
            throw new DomainEntityNotFoundException("Product does not exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);
        product.setName(productForm.getName());
        product.setUnit(productForm.getUnit());
        product.setQuantity(productForm.getQuantity());
        product.setUnitPrice(productForm.getUnitPrice());
        product.setMinimumMonthsOfStock(productForm.getMinimumMonthsOfStock());
        product.setMaximumMonthsOfStock(productForm.getMaximumMonthsOfStock());
        product.setUpdatedBy(currentUser.getId());
        product.setUpdatedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        product.setEnabled(productForm.getEnabled());
        productDao.update(product);
    }

    public void edit (HashMap<Integer,ProductForm> productForms) {
        Set<Integer> ids = productForms.keySet();
        List<ProductForm> productFormValues = new ArrayList<ProductForm>(productForms.values());
        int idx = 0;

        for(Integer id : ids) {
            edit(productFormValues.get(idx),id);
            idx++;
        }
    }

    public void delete (Integer id) {
        Product product = productDao.getById(id);
        if(product == null) {
            throw new DomainEntityNotFoundException("Product does not exists");
        }

        User currentUser = new CurrentUser().getUser(userDao);
        product.setDeletedBy(currentUser.getId());
        product.setDeletedOn(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        productDao.delete(product);
    }

    public void  delete (Set<Integer> ids) {
        for(Integer id : ids) {
            delete(id);
        }
    }

    public List<Product> getByProgramId(Integer id) {
        return  productDao.getByProgramId(id);
    }

}

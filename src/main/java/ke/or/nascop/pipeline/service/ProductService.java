package ke.or.nascop.pipeline.service;

import ke.or.nascop.pipeline.model.Product;
import ke.or.nascop.pipeline.wrappers.ProductForm;
import org.springframework.security.access.annotation.Secured;

import java.util.HashMap;
import java.util.List;
import java.util.Set;


/**
 * Created by robinson on 2/21/16.
 */
/*
 *The ProductService provides an interface for
 * upper layers (controllers) to communicate with the application
 * logic.
 */
public interface ProductService {
    @Secured("ROLE_ADD_PRODUCT")
    void add (Set<ProductForm> productForm,Integer programId);
    @Secured("ROLE_ADD_PRODUCT")
    void add (ProductForm productForm,Integer programId);
    @Secured("ROLE_VIEW_PRODUCT")
    List<Product> getAll (Integer programId);
    @Secured("ROLE_VIEW_PRODUCT")
    Product getById(Integer id);
    @Secured("ROLE_VIEW_PRODUCT")
    List<Product> getByProgramId(Integer id);
    @Secured("ROLE_EDIT_PRODUCT")
    void edit(ProductForm productForm,Integer id);
    @Secured("ROLE_DELETE_PRODUCT")
    void delete(Integer id);
    @Secured("ROLE_DELETE_PRODUCT")
    void delete(Set<Integer> ids);
}

package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.wrappers.Message;
import ke.or.nascop.pipeline.wrappers.UserForm;
import ke.or.nascop.pipeline.wrappers.UserPassword;
import ke.or.nascop.pipeline.service.UserService;
import ke.or.nascop.pipeline.util.RestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by robinson on 2/8/16.
 */

/*
 *The user controller is used to implement the user specific functions
 * Like add/registration,edit,list,info,delete
 */
@RestController
@RequestMapping(value="/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value="/add",method= RequestMethod.POST)
    private ResponseEntity<Message> add (@Valid @RequestBody UserForm userForm, BindingResult result) {
        if(result.hasErrors()) {
            throw new RestException(1,"Error! ","Check the fields for required data ");
        }

        try {
            userService.add(userForm);
        } catch (Exception e) {
            throw new RestException(1,e.getMessage(),e.getStackTrace().toString());
        }
        Message msg = new Message();
        msg.setType("info");
        msg.setMessage("Successfully added user");
        msg.setUrl("/user/view/table");
       return new ResponseEntity<Message>(msg, HttpStatus.OK);
    }

    @RequestMapping(value="/edit/password",method= RequestMethod.POST)
    private ResponseEntity<Message>changePassword (@Valid @RequestBody UserPassword userPassword, BindingResult result) {
        if(result.hasErrors()) {
            throw new RestException(1,"Error! ","Check the fields for required data ");
        }

        userService.changePassword(userPassword);
        Message msg = new Message();
        msg.setType("info");
        msg.setMessage("Successfully edited user password");
        msg.setUrl("/user/list");
        return new ResponseEntity<Message>(msg, HttpStatus.OK);
    }

    @RequestMapping(value="/list",method=RequestMethod.GET)
    private List<User> getUsers () {
        return userService.getAll();
    }

    @RequestMapping(value="/{id}/edit",method=RequestMethod.POST)
    private ResponseEntity<Message> edit (@PathVariable int id,@Valid @RequestBody UserForm userForm, BindingResult result) {
        if(result.hasErrors()) {
            throw new RestException(1,"Error! ","Check the fields for required data ");
        }

        userService.edit(userForm,id);
        Message msg = new Message();
        msg.setType("info");
        msg.setMessage("Successfully edited user");
        msg.setUrl("/user/view/table");
        return new ResponseEntity<Message>(msg, HttpStatus.OK);
    }

    @RequestMapping(value="/{idNumber}/info",method=RequestMethod.POST)
    private User info (@PathVariable String idNumber) {
        return userService.getByIdNumber(idNumber);
    }

    @RequestMapping(value="/{id}/delete",method = RequestMethod.GET)
    private ResponseEntity<String> delete (@PathVariable int id) {
        userService.delete(id);
        return new ResponseEntity<String>("Successfully deleted user ", HttpStatus.OK);
    }

    @RequestMapping(value="/recovery/password",method= RequestMethod.POST)
    private ResponseEntity<String> changePassword (@RequestBody String email) {
        userService.recoverPassword(email);
        return new ResponseEntity<String>("Details have been sent for recovery ", HttpStatus.OK);
    }

}

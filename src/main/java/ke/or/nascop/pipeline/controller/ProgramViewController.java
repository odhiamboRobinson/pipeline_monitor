package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.model.Program;
import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.service.ProgramService;
import ke.or.nascop.pipeline.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by robinson on 2/8/16.
 */
/*
 *The user view controller
 *Is used to map to methods that return html objects to the browser
 */
@Controller
@RequestMapping(value="/program/view")
public class ProgramViewController {
    @Autowired
    private ProgramService programService;
    @Autowired
    private UserService userService;

    @RequestMapping(value="/add/form",method = RequestMethod.GET)
    private ModelAndView addForm () {
        ModelAndView mv = new ModelAndView();
        mv.addObject("route","/program/add");
        mv.setViewName("forms/program");

        return mv;
    }
    @RequestMapping(value="/{id}/edit/form",method = RequestMethod.GET)
    private ModelAndView editForm (@PathVariable int id) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("program",programService.getById(id));
        mv.addObject("route","/program/"+id+"/edit");
        mv.setViewName("forms/program");

        return mv;
    }

    @RequestMapping(value="/{id}/info",method = RequestMethod.GET)
    private ModelAndView userInfo (@PathVariable int id) {
        ModelAndView mv = new ModelAndView();
        Program program = programService.getById(id);
        mv.addObject("program",program);
        if(program.getUpdatedBy() != null) {
            User updatedBy = userService.getById(program.getUpdatedBy());
            if(updatedBy != null) {
                mv.addObject("updateBy",updatedBy.getSirName());
            } else {
                mv.addObject("updateBy","N/A");
            }
        } else {
            mv.addObject("updateBy","N/A");
        }


        User createdBy = userService.getById(program.getCreatedBy());
        if(createdBy != null) {
            mv.addObject("createdBy",createdBy.getSirName());
        } else {
            mv.addObject("createdBy","N/A");
        }

        mv.setViewName("composites/program");

        return mv;
    }

    @RequestMapping(value="/table",method = RequestMethod.GET)
    private ModelAndView table() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("tables/programs");

        return mv;
    }

    @RequestMapping(value="/{id}/mininav",method = RequestMethod.GET)
    private ModelAndView mininav(@PathVariable String idNumber) {
        ModelAndView mv = new ModelAndView();
        User user = userService.getByIdNumber(idNumber);
        mv.addObject("id", user.getId());
        mv.setViewName("miniNav/users");

        return mv;
    }


}

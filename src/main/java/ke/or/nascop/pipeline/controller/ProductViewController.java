package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.model.Product;
import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.service.ProductService;
import ke.or.nascop.pipeline.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by robinson on 2/8/16.
 */
/*
 *The user view controller
 *Is used to map to methods that return html objects to the browser
 */
@Controller
@RequestMapping(value="/product/view")
public class ProductViewController {
    @Autowired
    private ProductService productService;
    @Autowired
    private UserService userService;

    @RequestMapping(value="/add/form",method = RequestMethod.GET)
    private ModelAndView addForm () {
        ModelAndView mv = new ModelAndView();
        mv.addObject("route","/product/add");
        mv.setViewName("forms/product");

        return mv;
    }
    @RequestMapping(value="/{id}/edit/form",method = RequestMethod.GET)
    private ModelAndView editForm (@PathVariable int id) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("product",productService.getById(id));
        mv.addObject("route","/product/"+id+"/edit");
        mv.setViewName("forms/editProduct");

        return mv;
    }

    @RequestMapping(value="/{id}/info",method = RequestMethod.GET)
    private ModelAndView userInfo (@PathVariable int id) {
        ModelAndView mv = new ModelAndView();
        Product product = productService.getById(id);
        mv.addObject("product",product);
        if(product.getUpdatedBy() != null) {
            User updatedBy = userService.getById(product.getUpdatedBy());
            if(updatedBy != null) {
                mv.addObject("updateBy",updatedBy.getSirName());
            } else {
                mv.addObject("updateBy","N/A");
            }
        } else {
            mv.addObject("updateBy","N/A");
        }


        User createdBy = userService.getById(product.getCreatedBy());
        if(createdBy != null) {
            mv.addObject("createdBy",createdBy.getSirName());
        } else {
            mv.addObject("createdBy","N/A");
        }

        mv.setViewName("composites/product");

        return mv;
    }

    @RequestMapping(value="/table",method = RequestMethod.GET)
    private ModelAndView table() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("tables/products");
        return mv;
    }


}

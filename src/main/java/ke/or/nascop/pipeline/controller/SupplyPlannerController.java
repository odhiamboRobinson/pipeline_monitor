package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.service.SupplyPlannerService;
import ke.or.nascop.pipeline.util.RestException;
import ke.or.nascop.pipeline.wrappers.SupplyPlanForm;
import ke.or.nascop.pipeline.wrappers.SupplyPlanJson;
import ke.or.nascop.pipeline.wrappers.SupplyPlanProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by getty on 2/21/16.
 */

/*
 *The supply plan controller is used to map
 * to methods that execute business logic for the organization unit module
 */
@RestController
@RequestMapping("/supplyplan")
public class SupplyPlannerController {
    @Autowired
    SupplyPlannerService supplyPlannerService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    private List<SupplyPlanJson> list() {
        return supplyPlannerService.getAll();
    }

    @RequestMapping(value = "/{id}/list", method = RequestMethod.GET)
    private List<SupplyPlanJson> list(@PathVariable Integer id) {
        return supplyPlannerService.getById(id);
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    private ResponseEntity<String> edit(@Valid @RequestBody SupplyPlanForm supplyPlanForm, @PathVariable Integer id, BindingResult result) {
        if (result.hasErrors()) {
            String message = "";
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                message += error.getDefaultMessage() + " : ";
            }
            throw new RestException(400, "Failed to edit new stock", message);
        }

        supplyPlannerService.edit(supplyPlanForm, id);
        return new ResponseEntity<String>("Successfully added a stock", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
    private ResponseEntity<String> delete(@PathVariable Integer id) {
        supplyPlannerService.delete(id);
        return new ResponseEntity<String>("Successfully deleted a stock", HttpStatus.OK);
    }
}

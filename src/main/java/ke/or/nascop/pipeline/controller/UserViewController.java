package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.service.ProgramService;
import ke.or.nascop.pipeline.service.UserGroupService;
import ke.or.nascop.pipeline.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by robinson on 2/8/16.
 */
/*
 *The user view controller
 *Is used to map to methods that return html objects to the browser
 */
@Controller
@RequestMapping(value="/user/view")
public class UserViewController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserGroupService userGroupService;

    @Autowired
    private ProgramService programService;

    @RequestMapping(value="/add/form",method = RequestMethod.GET)
    private ModelAndView addForm () {
        ModelAndView mv = new ModelAndView();
        mv.addObject("userGroups",userGroupService.getAll());
        mv.addObject("programs",programService.getAll());
        mv.addObject("route","/user/add");
        mv.setViewName("forms/user");

        return mv;
    }
    @RequestMapping(value="/{id}/edit/form",method = RequestMethod.GET)
    private ModelAndView editForm (@PathVariable int id) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("user",userService.getById(id));
        mv.addObject("route","/user/"+id+"/edit");
        mv.addObject("programs",programService.getAll());
        mv.addObject("userGroups",userGroupService.getAll());
        mv.setViewName("forms/user");

        return mv;
    }

    @RequestMapping(value="/{id}/info",method = RequestMethod.GET)
    private ModelAndView userInfo (@PathVariable int id) {
        ModelAndView mv = new ModelAndView();
        User user = userService.getById(id);
        mv.addObject("user",user);
        if(user.getUpdatedBy() != null) {
            User updatedBy = userService.getById(user.getUpdatedBy());
            if(updatedBy != null) {
                mv.addObject("updateBy",updatedBy.getSirName());
            } else {
                mv.addObject("updateBy","N/A");
            }
        } else {
            mv.addObject("updateBy","N/A");
        }


        User createdBy = userService.getById(user.getCreatedBy());
        if(createdBy != null) {
            mv.addObject("createdBy",createdBy.getSirName());
        } else {
            mv.addObject("createdBy","N/A");
        }

        mv.setViewName("composites/user");

        return mv;
    }

    @RequestMapping(value="/{id}/delete/form",method = RequestMethod.DELETE)
    private ModelAndView deleteForm (@PathVariable int id)
    {
        ModelAndView mv = new ModelAndView();

        mv.setViewName("userForm");

        return mv;
    }

    @RequestMapping(value="/table",method = RequestMethod.GET)
    private ModelAndView table() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("tables/users");

        return mv;
    }

    @RequestMapping(value="/{idNumber}/mininav",method = RequestMethod.GET)
    private ModelAndView mininav(@PathVariable String idNumber) {
        ModelAndView mv = new ModelAndView();
        User user = userService.getByIdNumber(idNumber);
        mv.addObject("id", user.getId());
        mv.setViewName("miniNav/users");

        return mv;
    }

    @RequestMapping(value = "/{id}/password/form", method = RequestMethod.GET)
    private ModelAndView editForm()
    {
        ModelAndView mv = new ModelAndView();
        mv.addObject("route", "/changePassword/edit");
        mv.setViewName("forms/changePassword");
        return mv;
    }



}

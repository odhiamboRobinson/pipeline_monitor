package ke.or.nascop.pipeline.controller;


import ke.or.nascop.pipeline.service.ForecastService;
import ke.or.nascop.pipeline.service.ProductService;
import ke.or.nascop.pipeline.util.Credentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by robinson on 2/26/16.
 */
@Controller
@RequestMapping("/forecast/view")
public class ForecastViewController {
    @Autowired
    ForecastService forecastService;
    @Autowired
    ProductService productService;
    @Autowired
    Credentials credentials;

    @RequestMapping("/add/form")
    public ModelAndView createForm() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("products",productService.getByProgramId(credentials.getProgram().getId()));
        mv.setViewName("forms/forecast");
        return mv;
    }

    @RequestMapping("/{id}/edit/form")
    public ModelAndView editForm(@PathVariable Integer id) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("products",productService.getByProgramId(credentials.getProgram().getId()));
        mv.addObject("forecastForm",forecastService.getById(id));
        mv.setViewName("forms/forecast");
        return mv;
    }

    @RequestMapping("/table")
    public ModelAndView table() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("tables/forecasts");
        return mv;
    }
}

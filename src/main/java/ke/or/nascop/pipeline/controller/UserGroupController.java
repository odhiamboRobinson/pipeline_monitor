package ke.or.nascop.pipeline.controller;


import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.model.UserGroup;
import ke.or.nascop.pipeline.service.UserGroupService;
import ke.or.nascop.pipeline.util.RestException;
import ke.or.nascop.pipeline.wrappers.UserGroupForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by gety on 2/15/2016.
 */
/*
 *The user group view controller
 *Is used to map to methods that return html objects to the browser
 */
@RestController
@RequestMapping(value="/group")
public class UserGroupController {

    @Autowired
    private UserGroupService userGroupService;

    @RequestMapping(value="/add",method= RequestMethod.POST)
    private ResponseEntity<String> add (@Valid @RequestBody UserGroupForm userGroupForm, BindingResult result) {
        if(result.hasErrors()) {
            throw new RestException(1,"Error! ","Check the fields for required data ");
        }

        userGroupService.add(userGroupForm);
        return new ResponseEntity<String>("Successfully added user group ", HttpStatus.OK);
    }

    @RequestMapping(value="/list",method=RequestMethod.GET)
    private List<UserGroup> getUserGroups () {
        return userGroupService.getAll();
    }

    @RequestMapping(value="/{id}/edit",method=RequestMethod.POST)
    private ResponseEntity<String> edit (@PathVariable int id, @Valid @RequestBody UserGroupForm userGroupForm, BindingResult result) {
        if(result.hasErrors()) {
            throw new RestException(1,"Error! ","Check the fields for required data! ");
        }
        userGroupService.edit(userGroupForm,id);
        return new ResponseEntity<String>("Successfully edited user group ", HttpStatus.OK);
    }

    @RequestMapping(value="/{id}/delete")
    private ResponseEntity<String> delete (@PathVariable int id) {
        userGroupService.delete(id);
        return new ResponseEntity<String>("Successfully deleted user group ", HttpStatus.OK);
    }

}

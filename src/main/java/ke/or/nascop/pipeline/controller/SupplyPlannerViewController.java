package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.service.ProductService;
import ke.or.nascop.pipeline.service.SupplyPlannerService;
import ke.or.nascop.pipeline.util.Credentials;
import ke.or.nascop.pipeline.util.Month;
import ke.or.nascop.pipeline.wrappers.SupplyPlanJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * Created by robinson on 2/28/16.
 */

@Controller
@RequestMapping("/supplyplan/view")
public class SupplyPlannerViewController {
    private List<Month> months;
    @Autowired
    ProductService productService;
    @Autowired
    SupplyPlannerService supplyPlannerService;
    @Autowired
    Credentials credentials;

    public SupplyPlannerViewController() {
        months = new ArrayList<Month>();
        months.add(new Month(1,"Jan"));
        months.add(new Month(2,"Feb"));
        months.add(new Month(3,"Mar"));
        months.add(new Month(4,"Apr"));
        months.add(new Month(5,"May"));
        months.add(new Month(6,"Jun"));
        months.add(new Month(7,"Jul"));
        months.add(new Month(8,"Aug"));
        months.add(new Month(9,"Sep"));
        months.add(new Month(10,"Oct"));
        months.add(new Month(11,"Nov"));
        months.add(new Month(12,"Dec"));
    }

    @RequestMapping("/{id}/form")
    public ModelAndView form(@PathVariable Integer id) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("supplyPlanProducts",supplyPlannerService.getSummaryPerProduct(credentials.getForecast().getId()));
        mv.addObject("months",this.months);
        Set<Integer> availableMonths = new HashSet<Integer>();
        Set<Integer> availableYears = new HashSet<Integer>();
        List<SupplyPlanJson> supplyPlanJsons = supplyPlannerService.getById(id);
        for(SupplyPlanJson supplyPlanJson : supplyPlanJsons) {
            availableMonths.add(supplyPlanJson.getMonth());
            availableYears.add(supplyPlanJson.getYear());
        }
        mv.addObject("program",credentials.getProgram());
        mv.addObject("supplyPlans",supplyPlanJsons);
        mv.addObject("availableMonths",availableMonths);
        mv.addObject("availableYears",availableYears);
        mv.addObject("id",id);
        mv.setViewName("forms/supplyPlanner");
        return mv;
    }

    @RequestMapping("/table")
    public ModelAndView table() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("supplyPlans",supplyPlannerService.getSummaryPerProduct(credentials.getForecast().getId()));
        mv.setViewName("tables/supplyPlan");
        return mv;
    }
}

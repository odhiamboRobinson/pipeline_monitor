package ke.or.nascop.pipeline.controller;


import ke.or.nascop.pipeline.service.ProductService;
import ke.or.nascop.pipeline.service.ForecastService;
import ke.or.nascop.pipeline.util.Credentials;
import ke.or.nascop.pipeline.util.Month;
import ke.or.nascop.pipeline.util.RestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 3/8/16.
 */
@Controller
@RequestMapping("/analysis/view")
public class AnalysisViewController {
    @Autowired
    Credentials credentials;
    @Autowired
    ForecastService forecastService;
    @Autowired
    ProductService productService;
    private List<Month> months;

    public AnalysisViewController() {
        months = new ArrayList<Month>();
        months.add(new Month(1,"January"));
        months.add(new Month(2,"February"));
        months.add(new Month(3,"March"));
        months.add(new Month(4,"April"));
        months.add(new Month(5,"May"));
        months.add(new Month(6,"June"));
        months.add(new Month(7,"July"));
        months.add(new Month(8,"August"));
        months.add(new Month(9,"September"));
        months.add(new Month(10,"October"));
        months.add(new Month(11,"November"));
        months.add(new Month(12,"December"));
    }

    @RequestMapping("/composite")
    public ModelAndView composite() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("composites/analysis");
        return mv;
    }

    @RequestMapping("{type}/periods")
    public ModelAndView periods(@PathVariable String type) {
        ModelAndView mv = new ModelAndView();
        if(type.equals("products")) {
            mv.addObject("multiple",false);
            mv.addObject("loadingText","loading MOS by Product Graph");
            mv.addObject("route","analysis/mos");
        } else if (type.equals("forecast")){
            mv.addObject("multiple",true);
            mv.addObject("loadingText","loading Forecast Error Graph");
            mv.addObject("route","analysis/forecasterror");
        } else if (type.equals("stores")){
            mv.addObject("multiple",false);
            mv.addObject("loadingText","loading MOS by Central & per ..");
            mv.addObject("route","analysis/mos/centralandperipheral");
        } else {
            throw new RestException(400,"Illegal path variable ","An illegal path variable was set expected either product,forecast or stores");
        }
        mv.addObject("months",months);
        mv.addObject("monthYears", forecastService.getMonthYears(credentials.getForecast().getId()));
        mv.setViewName("forms/monthYearSelector");
        return mv;
    }

    @RequestMapping("/products")
    public ModelAndView products() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("products",productService.getByProgramId(credentials.getProgram().getId()));
        mv.setViewName("forms/productSelector");
        return mv;
    }
}

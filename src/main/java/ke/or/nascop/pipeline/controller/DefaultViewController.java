package ke.or.nascop.pipeline.controller;


import ke.or.nascop.pipeline.model.Program;
import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.service.*;
import ke.or.nascop.pipeline.util.Credentials;
import ke.or.nascop.pipeline.util.UserNotFoundException;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

/**
 * Created by robinson on 2/8/16.
 */

/*
 *The default view controller is used to map
 * to classes that do not need any authorization
 * to execute.
 * It also maps to methods that initialize the application
 */
@Controller
public class DefaultViewController {
    @Autowired
    ProgramService programService;
    @Autowired
    ProductService productService;
    @Autowired
    Credentials credentials;
    @Autowired
    UserService userService;
    @Autowired
    SupplyPlannerService supplyPlannerService;
    @Autowired
    ForecastService forecastService;

    @RequestMapping(value="/")
    public ModelAndView index () {
        ModelAndView mv = new ModelAndView();
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication auth = context.getAuthentication();
        Object obj  =  auth.getPrincipal();
        UserDetails userDetails = null;

        if(obj instanceof UserDetails) {
            userDetails = (UserDetails) obj;
        } else {
            throw new UserNotFoundException("Could not access the logged in user");
        }
        User user = userService.getByEmail(userDetails.getUsername());

        if(credentials.getProgram() == null) {
            Set<Program> programs = user.getPrograms();
            if(programs.size() == 1) {
                for(Program program : programs) {
                    credentials.setProgram(program);
                }
                credentials.setForecast(forecastService.getCurrentForecast(credentials.getProgram().getId()));
                mv.addObject("tiles",supplyPlannerService.getSummary(credentials.getForecast().getId(),credentials.getProgram().getId()));
                mv.addObject("programName", WordUtils.capitalize(credentials.getProgram().getName()));
            } else {
                mv.addObject("programs",programs);
            }
        } else {
            if(credentials.getForecast() != null) {
                mv.addObject("tiles",supplyPlannerService.getSummary(credentials.getForecast().getId(),credentials.getProgram().getId()));
            } else {
                credentials.setForecast(forecastService.getCurrentForecast(credentials.getProgram().getId()));
            }
            mv.addObject("programName", WordUtils.capitalize(credentials.getProgram().getName()));
        }

        mv.setViewName("init/index");
        return mv;
    }

    @RequestMapping(value="/auth/error")
    public ModelAndView signInError() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("init/auth");
        mv.addObject("errorMessage","Error : failed to log in please check your credentials");
        return mv;
    }

    @RequestMapping(value="/auth")
    public ModelAndView auth() {
        ModelAndView mv = new ModelAndView();
        try {
            if(SecurityContextHolder.getContext() != null) {
                SecurityContext context = SecurityContextHolder.getContext();
                if(context.getAuthentication() != null) {
                    Authentication authentication =  context.getAuthentication();
                    if(authentication.getPrincipal() != null) {
                        mv.setViewName("init/index");
                    } else {
                        mv.setViewName("init/auth");
                    }
                } else {
                    mv.setViewName("init/auth");
                }

            } else {
                mv.setViewName("init/auth");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value="/signup")
    public ModelAndView signUpForm() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("init/signup");
        return mv;
    }

    @RequestMapping(value="/signin")
    public ModelAndView signInForm() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("init/signin");
        return mv;
    }

    @RequestMapping(value="/view/addNew/mininav",method = RequestMethod.POST)
    public ModelAndView addMiniNav (@RequestParam String formPath) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("url",formPath);
        mv.setViewName("miniNav/addNew");
        return mv;
    }
    @RequestMapping(value = "/view/mininav",method = RequestMethod.POST)
    public ModelAndView miniNav (@RequestParam("urlEdit") String urlEdit,@RequestParam("urlInfo") String urlInfo) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("urlInfo",urlInfo);
        mv.addObject("urlEdit",urlEdit);
        mv.setViewName("miniNav/genNav");
        return mv;
    }
}

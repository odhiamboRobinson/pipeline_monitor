package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.service.AnalysisService;
import ke.or.nascop.pipeline.util.Credentials;
import ke.or.nascop.pipeline.util.RestException;
import ke.or.nascop.pipeline.wrappers.MonthYear;
import ke.or.nascop.pipeline.wrappers.MonthYearList;
import ke.or.nascop.pipeline.wrappers.MosGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 3/6/16.
 */
@RestController
@RequestMapping("/analysis")
public class AnalysisController {
    @Autowired
    private AnalysisService analysisService;
    @Autowired
    private Credentials credentials;

    @RequestMapping("/forecasterror")
    public List<MosGraph> forecastError(@RequestBody @Valid MonthYearList monthYearList) {
        if(credentials != null && credentials.getForecast() != null) {
            List graphs = new ArrayList<MosGraph>();
            graphs.add(analysisService.forecastErrorPerProduct(credentials.getForecast().getId(),monthYearList.getMonthYears()));
            return graphs;
        }

        throw new RestException(400,"Failed to fetch forecast error chart","Failure was due to the forecast not being set");
    }
    @RequestMapping("/mos")
    public List<MosGraph> mos(@RequestBody @Valid MonthYear monthYear) {
        if(credentials != null && credentials.getForecast() != null) {
            List graphs = new ArrayList<MosGraph>();
            graphs.add(analysisService.mosByProduct(credentials.getForecast().getId(), monthYear));
            return graphs;
        }

        throw new RestException(400,"Failed to fetch mos chart","Failure was due to the forecast not being set");
    }
    @RequestMapping("/mos/centralandperipheral")
    public List<MosGraph> mosCentralAndPeripheral(@RequestBody @Valid MonthYear monthYear) {
        if(credentials != null && credentials.getForecast() != null) {
            return analysisService.centralAndPeripheralMosPerProduct(credentials.getForecast().getId(),monthYear);
        }

        throw new RestException(400,"Failed to fetch mos central and peripheral chart","Failure was due to the forecast not being set");
    }

//    @RequestMapping("/product/overtime")
//    public List<MosGraph> productOvertime(@RequestParam Integer productId) {
//        if(credentials != null && credentials.getForecast() != null) {
//            return analysisService.centralAndPeripheralMosPerProduct(credentials.getForecast().getId(),monthYear);
//        }
//
//        throw new RestException(400,"Failed to fetch mos central and peripheral chart","Failure was due to the forecast not being set");
//    }
}

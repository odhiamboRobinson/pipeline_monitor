package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.model.Program;
import ke.or.nascop.pipeline.service.ProgramService;
import ke.or.nascop.pipeline.util.Credentials;
import ke.or.nascop.pipeline.util.RestException;
import ke.or.nascop.pipeline.wrappers.Message;
import ke.or.nascop.pipeline.wrappers.ProgramForm;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.awt.*;
import java.util.List;

/**
 * Created by getty on 2/21/16.
 */

/*
 *The program controller is used to map
 * to methods that execute business logic for the organization unit module
 */
@RestController
@RequestMapping("/program")
public class ProgramController {
    @Autowired
    ProgramService programService;
    @Autowired
    Credentials credentials;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    private ResponseEntity<String> add(@Valid @RequestBody ProgramForm programForm, BindingResult result) {
        if (result.hasErrors()) {
            String message = "";
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                message += error.getDefaultMessage() + " : ";
            }
            throw new RestException(400, "Failed to add new program", message);
        }

        programService.add(programForm);
        return new ResponseEntity<String>("Successfully added a program", HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    private List<Program> list() {
        return programService.getAll();
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    private ResponseEntity<String> edit(@Valid @RequestBody ProgramForm programForm, @PathVariable Integer id, BindingResult result) {
        if (result.hasErrors()) {
            String message = "";
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                message += error.getDefaultMessage() + " : ";
            }
            throw new RestException(400, "Failed to edit new program", message);
        }

        programService.edit(programForm, id);
        return new ResponseEntity<String>("Successfully added a program", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    private ResponseEntity<String> delete(@PathVariable Integer id) {
        programService.delete(id);
        return new ResponseEntity<String>("Successfully deleted a program", HttpStatus.OK);
    }

    @RequestMapping(value = "/activate",method = RequestMethod.POST)
    public ResponseEntity<Message> activateProgram (@RequestParam Integer programId) {
        credentials.setProgram(programService.getById(programId));
        Message msg = new Message();
        msg.setType("info");
        msg.setMessage("Welcome to the : "+ WordUtils.capitalize(credentials.getProgram().getName()));
        msg.setUrl("/");
        return new ResponseEntity<Message>(msg,HttpStatus.OK);
    }
}

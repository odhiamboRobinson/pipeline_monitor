package ke.or.nascop.pipeline.controller;


import ke.or.nascop.pipeline.service.ForecastService;
import ke.or.nascop.pipeline.service.ProductService;
import ke.or.nascop.pipeline.service.SupplyPlannerService;
import ke.or.nascop.pipeline.util.Credentials;
import ke.or.nascop.pipeline.util.RestException;
import ke.or.nascop.pipeline.wrappers.ForecastForm;
import ke.or.nascop.pipeline.wrappers.Message;
import ke.or.nascop.pipeline.wrappers.MosGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by robinson on 2/26/16.
 */
@RestController
@RequestMapping("/forecast")
public class ForecastController  {
    @Autowired
    ForecastService forecastService;
    @Autowired
    Credentials credentials;
    @Autowired
    ProductService productService;
    @Autowired
    SupplyPlannerService supplyPlannerService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    private ResponseEntity<Message> createForecast(@Valid @RequestBody ForecastForm forecastForm, BindingResult result) {
        if (result.hasErrors()) {
            String message = "";
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                message += error.getDefaultMessage() + " : ";
            }
            throw new RestException(400, "Failed to add new forecast", message);
        }
        forecastService.add(forecastForm,credentials.getProgram().getId());
        Message msg = new Message();
        msg.setType("info");
        msg.setMessage("Successfully added user");
        msg.setUrl("/forecast/view/table");
        return new ResponseEntity<Message>(msg, HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    private List<ForecastForm> list() {
       List<ForecastForm> forecasts = forecastService.getByProgram(credentials.getProgram().getId());
       for(ForecastForm forecast :forecasts) {
           if(credentials != null && credentials.getForecast() != null && forecast.getId() == credentials.getForecast().getId()) {
               forecast.setInActive(true);
           }
       }

       return forecasts;

    }

    @RequestMapping(value = "/{id}/activate", method = RequestMethod.GET)
    private List<ForecastForm> activate(@PathVariable Integer id) {
        credentials.setForecast(forecastService.getForecast(id));
        return list();
    }

    @RequestMapping(value = "/ls", method = RequestMethod.GET)
    private List<MosGraph> data() {

       List<MosGraph> ls =  supplyPlannerService.mosByProduct(productService.getById(1),1,16,12);
        return ls;
    }
}

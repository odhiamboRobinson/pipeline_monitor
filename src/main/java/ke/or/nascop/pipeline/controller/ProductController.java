package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.model.Product;
import ke.or.nascop.pipeline.service.ProductService;
import ke.or.nascop.pipeline.util.Credentials;
import ke.or.nascop.pipeline.util.RestException;
import ke.or.nascop.pipeline.wrappers.ProductForm;
import ke.or.nascop.pipeline.wrappers.ProductList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;

/**
 * Created by getty on 2/8/16.
 */

/*
 *The product controller is used to map
 * to methods that execute business logic for the organization unit module
 */


@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    Credentials credentials;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    private ResponseEntity<String> add(@RequestBody ProductList productList, BindingResult result) {
        if (result.hasErrors()) {
            String message = "";
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                message += error.getDefaultMessage() + " : ";
            }
            throw new RestException(400, "Failed to add new product", message);
        }
        productService.add(new HashSet<ProductForm>(productList.getProductForms()),credentials.getProgram().getId());
        return new ResponseEntity<String>("Successfully added a product", HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    private List<Product> list() {
        return productService.getAll(credentials.getProgram().getId());
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    private ResponseEntity<String> edit(@Valid @RequestBody ProductForm productForm, @PathVariable Integer id, BindingResult result) {
        if (result.hasErrors()) {
            String message = "";
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                message += error.getDefaultMessage() + " : ";
            }
            throw new RestException(400, "Failed to edit new product", message);
        }

        productService.edit(productForm,id);
        return new ResponseEntity<String>("Successfully edited a product", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
    private ResponseEntity<String> delete(@PathVariable Integer id) {
        productService.delete(id);
        return new ResponseEntity<String>("Successfully deleted a product", HttpStatus.OK);
    }
}
package ke.or.nascop.pipeline.controller;

import ke.or.nascop.pipeline.model.Authority;
import ke.or.nascop.pipeline.service.AuthorityService;
import ke.or.nascop.pipeline.service.UserGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * Created by gety on 2/15/2016.
 */

/*
 *The organisation unit view controller
 *Is used to map to methods that return html objects to the browser
 */
@Controller
@RequestMapping(value="/group/view")
public class UserGroupViewController {
  @Autowired
  private UserGroupService userGroupService;
  @Autowired
  private AuthorityService authorityService;

  @RequestMapping(value="/add/form")
  private ModelAndView addForm () {
      ModelAndView mv = new ModelAndView();
      mv.addObject("route","/group/add");
      mv.addObject("authorities",authorityService.getAll());
      mv.setViewName("forms/group");
      return mv;
  }

  @RequestMapping(value="/{id}/edit/form")
  private ModelAndView editForm (@PathVariable int id) {
     ModelAndView mv = new ModelAndView();
      mv.addObject("route","/group/"+id+"/edit");
     mv.addObject("authorities", authorityService.getAll());
     mv.addObject("userGroup",userGroupService.getById(id));
     mv.setViewName("forms/group");

     return mv;
  }

  @RequestMapping(value="/table")
  private ModelAndView table() {
     ModelAndView mv = new ModelAndView();
     mv.setViewName("tables/groups");
     return mv;
  }
}

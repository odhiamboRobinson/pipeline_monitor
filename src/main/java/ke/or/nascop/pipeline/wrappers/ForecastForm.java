package ke.or.nascop.pipeline.wrappers;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 2/27/16.
 */
@JsonIgnoreProperties(value = {"productForecasts", "enabled"},allowSetters = true)
public class ForecastForm {
    private Integer id;
    @NotNull
    private Integer startingYear;
    @NotNull
    private Integer startingMonth;
    @NotNull
    private Integer endingYear;
    @NotNull
    private Integer endingMonth;
    @NotNull
    private List<ProductForecast> productForecasts;
    @NotNull
    private Boolean enabled;
    @NotNull
    private Boolean current;
    private Boolean inActive;

    public ForecastForm() {
        this.productForecasts = new ArrayList<ProductForecast>();
    }

    public void setProductForecasts(List<ProductForecast> productForecasts) {
        this.productForecasts.addAll(productForecasts);
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public List<ProductForecast> getProductForecasts() {
        return this.productForecasts;
    }

    public void setStartingYear(Integer startingYear) {
        this.startingYear = startingYear;
    }

    public Integer getStartingYear() {
        return this.startingYear;
    }

    public void setStartingMonth(Integer startingMonth) {
        this.startingMonth = startingMonth;
    }

    public Integer getStartingMonth() {
        return this.startingMonth;
    }

    public void setEndingMonth(Integer endingMonth) {
        this.endingMonth = endingMonth;
    }

    public Integer getEndingMonth() {
        return this.endingMonth;
    }

    public void setEndingYear(Integer endingYear) {
        this.endingYear = endingYear;
    }

    public Integer getEndingYear() {
        return this.endingYear;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setCurrent(Boolean current) {
        this.current = current;
    }

    public Boolean getCurrent() {
        return this.current;
    }

    @JsonIgnore
    public void setInActive(Boolean inActive) {
        this.inActive = inActive;
    }

    @JsonProperty
    public Boolean getInActive() {
        return this.inActive;
    }
}

package ke.or.nascop.pipeline.wrappers;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 2/16/16.
 */
public class UserForm {
    @Size(min=2,max=60,message="Sir name must be between 2 and 60 characters")
    protected String sirName;
    @Size(min=2,max=150,message="Please fill this field to ease the identification of your account")
    protected String otherNames;
    @Email(message="Please enter a valid email")
    protected String email;
    @Size(min=10,message="Please enter your phone number")
    protected String phoneNumber;
    @NotNull
    protected String idNumber;
    @NotNull
    private List<Integer> programIds;
    @NotNull
    private Boolean enabled;
    @NotNull
    protected List<Integer> userGroupIds;

    public UserForm() {
        userGroupIds = new ArrayList<Integer>();
        programIds = new ArrayList<Integer>();
    }

    public void setSirName (String sirName) {
        this.sirName = sirName;
    }

    public String  getSirName () {
        return this.sirName;
    }

    public void setOtherNames (String otherNames) {
        this.otherNames = otherNames;
    }

    public String getOtherNames () {
        return this.otherNames;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public String getEmail () {
        return this.email;
    }

    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber () {
        return this.phoneNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdNumber() {
        return this.idNumber;
    }

    public void setEnabled (Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled () {
        return this.enabled;
    }

    public void setUserGroupIds (List<Integer> organisationUnitId) {
        this.userGroupIds.addAll(organisationUnitId);
    }

    public List<Integer> getUserGroupIds () {
        return this.userGroupIds;
    }

    public void setProgramIds (List<Integer> programIds) {
        this.programIds.addAll(programIds);
    }

    public List<Integer> getProgramIds () {
        return this.programIds;
    }
}

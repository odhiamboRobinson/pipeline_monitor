package ke.or.nascop.pipeline.wrappers;

import javax.validation.constraints.NotNull;

/**
 * Created by robinson on 2/27/16.
 */
public class ProductForecast {
    @NotNull
    private Integer productId;
    @NotNull
    private Double forecastFactor;
    @NotNull
    private Double forecastConsumption;
    @NotNull
    private Double initialStockOnHandNationalStores;
    @NotNull
    private Double initialStockOnHandSubNationalStores;

    public void setProductId(Integer productId) {
        this.productId = productId;
    }


    public Integer getProductId() {
        return this.productId;
    }

    public void setForecastFactor(Double forecastFactor) {
        this.forecastFactor = forecastFactor;
    }

    public Double getForecastFactor() {
        return this.forecastFactor;
    }

    public void setForecastConsumption(Double forecastConsumption) {
        this.forecastConsumption = forecastConsumption;
    }

    public Double getForecastConsumption() {
        return this.forecastConsumption;
    }

    public void setInitialStockOnHandNationalStores(Double initialStockOnHandNationalStores) {
        this.initialStockOnHandNationalStores = initialStockOnHandNationalStores;
    }

    public Double getInitialStockOnHandNationalStores() {
        return this.initialStockOnHandNationalStores;
    }

    public void setInitialStockOnHandSubNationalStores(Double initialStockOnHandSubNationalStores) {
        this.initialStockOnHandSubNationalStores = initialStockOnHandSubNationalStores;
    }

    public Double getInitialStockOnHandSubNationalStores() {
        return this.initialStockOnHandSubNationalStores;
    }

}

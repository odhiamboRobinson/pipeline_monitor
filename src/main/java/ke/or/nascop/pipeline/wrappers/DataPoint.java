package ke.or.nascop.pipeline.wrappers;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by robinson on 3/2/16.
 */
public class DataPoint {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double x;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double y;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String label;

    public void setX (Double x) {
        this.x = x;
    }

    public Double getX () {
        return this.x;
    }

    public void setY (Double y) {
        this.y = y;
    }

    public Double getY () {
        return this.y;
    }

    public void setLabel (String label) {
        this.label = label;
    }

    public String getLabel () {
        return this.label;
    }
}

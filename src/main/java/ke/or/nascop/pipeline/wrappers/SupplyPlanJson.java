package ke.or.nascop.pipeline.wrappers;

import ke.or.nascop.pipeline.model.Product;


/**
 * Created by robinson on 2/28/16.
 */
public class SupplyPlanJson {
    private Integer id;
    private Double forecastConsumption;
    private Double forecastFactor;
    private Double initialStockOnHandNationalStores;
    private Double initialStockOnHandSubNationalStores;
    private Integer month;
    private Integer year;
    private Product product;
    private Double openingStockBalance;
    private Double centralIssues;
    private Double expectedDeliveries;
    private Double actualConsumption;
    private Double desiredAdditionalQuantity;
    private Double calculatedClosingStock;
    private Double actualClosingStockCentralStores;
    private Double actualClosingStockPeripheralStores;
    private Double totalActualClosingStock;
    private Double monthsOfStockCentralStores;
    private Double monthsOfStockPeripheralStores;
    private Double totalMonthsOfStock;
    private Boolean actualConsumptionOverride;
    private Boolean centralConsumptionOverride;
    private Boolean expectedDeliveriesOverride;
    private Boolean actualClosingStockCentralStoresOverride;
    private Boolean actualClosingStockPeripheralStoresOverride;
    private Double maximumStockQuantity;
    private Double minimumStockQuantity;


    public SupplyPlanJson(
            Integer id,
            Double forecastConsumption,
            Double forecastFactor,
            Double initialStockOnHandNationalStores,
            Double initialStockOnHandSubNationalStores,
            Integer month,
            Integer year,
            Product product,
            Double openingStockBalance,
            Double centralIssues,
            Double expectedDeliveries,
            Double actualConsumption,
            Double desiredAdditionalQuantity,
            Double calculatedClosingStock,
            Double actualClosingStockCentralStores,
            Double actualClosingStockPeripheralStores,
            Double totalActualClosingStock,
            Double monthsOfStockCentralStores,
            Double monthsOfStockPeripheralStores,
            Double totalMonthsOfStock,
            Boolean actualConsumptionOverride,
            Boolean centralConsumptionOverride,
            Boolean expectedDeliveriesOverride,
            Boolean actualClosingStockCentralStoresOverride,
            Boolean actualClosingStockPeripheralStoresOverride,
            Double maximumStockQuantity,
            Double minimumStockQuantity
    ){

        this.id = id;
        if(forecastConsumption != null){this.forecastConsumption = Math.round( forecastConsumption * 100.0 ) / 100.0;}else { this.forecastConsumption= null;}
        if(forecastFactor != null){this.forecastFactor = Math.round( forecastFactor * 100.0 ) / 100.0;}else {this.forecastFactor = null;}
        if(initialStockOnHandNationalStores != null){this.initialStockOnHandNationalStores = Math.round(initialStockOnHandNationalStores * 100.0 ) / 100.0;}else {this.initialStockOnHandNationalStores = null;}
        if(initialStockOnHandSubNationalStores != null){this.initialStockOnHandSubNationalStores = Math.round(initialStockOnHandSubNationalStores * 100.0 ) / 100.0;}else {this.initialStockOnHandSubNationalStores = null;}
        this.month = month;
        this.year = year;
        this.product = product;
        if(openingStockBalance != null){this.openingStockBalance = Math.round(openingStockBalance * 100.0 ) / 100.0;}else {this. openingStockBalance = null;}
        if(centralIssues != null){this.centralIssues = Math.round(centralIssues * 100.0 ) / 100.0;}else {this.centralIssues = null;}
        if(expectedDeliveries != null){this.expectedDeliveries = Math.round(expectedDeliveries * 100.0 ) / 100.0;}else {this. expectedDeliveries = null;}
        if(actualConsumption != null){this.actualConsumption = Math.round(actualConsumption * 100.0 ) / 100.0;}else {this.actualConsumption = null;}
        if(desiredAdditionalQuantity != null){this.desiredAdditionalQuantity = Math.round(desiredAdditionalQuantity * 100.0 ) / 100.0;}else {this.desiredAdditionalQuantity = null;}
        if(calculatedClosingStock != null){this.calculatedClosingStock = Math.round(calculatedClosingStock * 100.0 ) / 100.0;}else {this. calculatedClosingStock = null;}
        if(actualClosingStockCentralStores != null){this.actualClosingStockCentralStores = Math.round(actualClosingStockCentralStores * 100.0 ) / 100.0;}else {this.actualClosingStockCentralStores = null;}
        if(actualClosingStockPeripheralStores != null){this.actualClosingStockPeripheralStores = Math.round(actualClosingStockPeripheralStores * 100.0 ) / 100.0;}else {this.actualClosingStockPeripheralStores = null;}
        if(totalActualClosingStock != null){this.totalActualClosingStock = Math.round(totalActualClosingStock * 100.0 ) / 100.0;}else {this. totalActualClosingStock = null;}
        if(monthsOfStockCentralStores != null){this.monthsOfStockCentralStores = Math.round(monthsOfStockCentralStores * 100.0 ) / 100.0;}else {this.monthsOfStockCentralStores = null;}
        if(monthsOfStockPeripheralStores != null){this.monthsOfStockPeripheralStores = Math.round(monthsOfStockPeripheralStores * 100.0 ) / 100.0;}else {this.monthsOfStockPeripheralStores  = null;}
        if(totalMonthsOfStock != null){this.totalMonthsOfStock = Math.round(totalMonthsOfStock * 100.0 ) / 100.0;}else {this.totalMonthsOfStock = null;}
        this.actualConsumptionOverride = actualConsumptionOverride;
        this.centralConsumptionOverride = centralConsumptionOverride;
        this.expectedDeliveriesOverride = expectedDeliveriesOverride;
        this.actualClosingStockCentralStoresOverride = actualClosingStockCentralStoresOverride;
        this.actualClosingStockPeripheralStoresOverride = actualClosingStockPeripheralStoresOverride;
        this.maximumStockQuantity = maximumStockQuantity;
        this.minimumStockQuantity = minimumStockQuantity;

    }


    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setForecastConsumption(Double forecastConsumption) {
        this.forecastConsumption = forecastConsumption;
    }

    public Double getForecastConsumption() {
        return this.forecastConsumption;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getMonth() {
        return this.month;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getYear() {
        return this.year;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setCentralIssues(Double centralIssues) {
        this.centralIssues = this.centralIssues;
    }

    public Double getCentralIssues() {
        return this.centralIssues;
    }

    public void setExpectedDeliveries(Double expectedDeliveries) {
        this.expectedDeliveries = expectedDeliveries;
    }

    public Double getOpeningStockBalance() {
        return this.openingStockBalance;
    }

    public void setOpeningStockBalance(Double openingStockBalance) {
        this.openingStockBalance = openingStockBalance;
    }

    public Double getExpectedDeliveries() {
        return this.expectedDeliveries;
    }

    public void setActualConsumption(Double actualConsumption) {
        this.actualConsumption = actualConsumption;
    }

    public Double getActualConsumption() {
        return this.actualConsumption;
    }


    public Double getDesiredAdditionalQuantity() {
        return this.desiredAdditionalQuantity;
    }

    public void setDesiredAdditionalQuantity(Double desiredAdditionalQuantity) {
        this.desiredAdditionalQuantity = desiredAdditionalQuantity;
    }

    public Double getCalculatedClosingStock() {
        return this.calculatedClosingStock;
    }

    public void setCalculatedClosingStock(Double calculatedClosingStock) {
        this.calculatedClosingStock = calculatedClosingStock;
    }

    public Double getTotalActualClosingStock() {
        return this.totalActualClosingStock;
    }

    public void setTotalActualClosingStock(Double totalActualClosingStock) {
        this.totalActualClosingStock = totalActualClosingStock;
    }

    public void setActualClosingStockCentralStores(Double actualClosingStockCentralStores) {
        this.actualClosingStockCentralStores = actualClosingStockCentralStores;
    }


    public Double getActualClosingStockCentralStores() {
        return this.actualClosingStockCentralStores;
    }

    public void setActualClosingStockPeripheralStores(Double actualClosingStockPeripheralStores) {
        this.actualClosingStockPeripheralStores = actualClosingStockPeripheralStores;
    }

    public Double getActualClosingStockPeripheralStores() {
        return this.actualClosingStockPeripheralStores;
    }


    public Double getMonthsOfStockCentralStores() {
        return this.monthsOfStockCentralStores;
    }

    public void setMonthsOfStockCentralStores(Double monthsOfStockCentralStores) {
        this.monthsOfStockCentralStores = monthsOfStockCentralStores;
    }

    public Double getMonthsOfStockPeripheralStores() {
        return this.monthsOfStockPeripheralStores;
    }

    public void setMonthsOfStockPeripheralStores(Double monthsOfStockPeripheralStores) {
        this.monthsOfStockPeripheralStores = monthsOfStockPeripheralStores;
    }


    public Double getTotalMonthsOfStock() {
        return this.totalMonthsOfStock;
    }

    public void setTotalMonthsOfStock(Double totalMonthsOfStock) {
        this.totalMonthsOfStock = totalMonthsOfStock;
    }

    public void setActualConsumptionOverride(Boolean actualConsumptionOverride) {
        this.actualConsumptionOverride = actualConsumptionOverride;
    }


    public Boolean getActualConsumptionOverride() {
        return this.actualConsumptionOverride;
    }

    public void setCentralConsumptionOverride(Boolean centralConsumptionOverride) {
        this.centralConsumptionOverride = centralConsumptionOverride;
    }


    public Boolean getCentralConsumptionOverride() {
        return this.centralConsumptionOverride;
    }

    public void setExpectedDeliveriesOverride(Boolean expectedDeliveriesOverride) {
        this.expectedDeliveriesOverride = expectedDeliveriesOverride;
    }


    public Boolean getExpectedDeliveriesOverride() {
        return this.expectedDeliveriesOverride;
    }

    public void setActualClosingStockCentralStoresOverride(Boolean actualClosingStockCentralStoresOverride) {
        this.actualClosingStockCentralStoresOverride = actualClosingStockCentralStoresOverride;
    }


    public Boolean getActualClosingStockCentralStoresOverride() {
        return this.actualClosingStockCentralStoresOverride;
    }

    public void setActualClosingStockPeripheralStoresOverride(Boolean actualClosingStockPeripheralStoresOverride) {
        this.actualClosingStockPeripheralStoresOverride = actualClosingStockPeripheralStoresOverride;
    }

    public Boolean getActualClosingStockPeripheralStoresOverride() {
        return this.actualClosingStockPeripheralStoresOverride;
    }

    public void setMaximumStockQuantity(Double maximumStockQuantity) {
        this.maximumStockQuantity = maximumStockQuantity;
    }

    public Double getMaximumStockQuantity() {
        return this.maximumStockQuantity;
    }

    public void setMinimumStockQuantity(Double minimumStockQuantity) {
        this.minimumStockQuantity = minimumStockQuantity;
    }

    public Double getMinimumStockQuantity() {
        return this.minimumStockQuantity;
    }

    public void setForecastFactor(Double forecastFactor) {
        this.forecastFactor = forecastFactor;
    }

    public Double getForecastFactor() {
        return this.forecastFactor;
    }

    public void setInitialStockOnHandNationalStores(Double initialStockOnHandNationalStores) {
        this.initialStockOnHandNationalStores = initialStockOnHandNationalStores;
    }

    public Double getInitialStockOnHandNationalStores() {
        return this.initialStockOnHandNationalStores;
    }

    public void setInitialStockOnHandSubNationalStores(Double initialStockOnHandSubNationalStores) {
        this.initialStockOnHandSubNationalStores = initialStockOnHandSubNationalStores;
    }

    public Double getInitialStockOnHandSubNationalStores() {
        return this.initialStockOnHandSubNationalStores;
    }
}

package ke.or.nascop.pipeline.wrappers;


import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 2/21/16.
 */
public class UserGroupForm {
    @NotNull
    private String name;
    @NotNull
    private List<Integer> authorityIds;
    @NotNull
    private Boolean enabled;

    public UserGroupForm () {
        authorityIds = new ArrayList<Integer>();
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getName () {
        return this.name;
    }

    public void setAuthorityIds (List<Integer> authorityIds) {
        this.authorityIds.addAll(authorityIds);
    }

    public List<Integer> getAuthorityIds () {
        return this.authorityIds;
    }

    public void setEnabled (Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled () {
        return this.enabled;
    }
}

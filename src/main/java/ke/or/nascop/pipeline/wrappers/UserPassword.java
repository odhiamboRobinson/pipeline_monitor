package ke.or.nascop.pipeline.wrappers;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by robinson on 2/16/16.
 */
public class UserPassword {
    @Size(min=5,message="Password must be more than five characters")
    private String password;
    @Size(min=5,message="Password must be more than five characters")
    private String newPassword;
    @NotBlank
    @NotNull
    private String confirmPassword;

    public void setPassword (String password) {
        this.password = password;
    }

    public String getPassword () {
        return this.password;
    }

    public void setNewPassword (String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword () {
        return this.newPassword;
    }

    public void setConfirmPassword (String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getConfirmPassword () {
        return this.confirmPassword;
    }
}

package ke.or.nascop.pipeline.wrappers;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by robinson on 3/2/16.
 */
public class MosGraph {
    private String type;
    private Boolean showInLegend = true;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String legendText;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String color;
    private Set<DataPoint> dataPoints;

    public  MosGraph() {
        this.dataPoints = new HashSet<DataPoint>();
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setShowInLegend (Boolean showInLegend) {
        this.showInLegend = showInLegend;
    }

    public Boolean getShowInLegend() {
        return this.showInLegend;
    }

    public void setLegendText(String legendText) {
        this.legendText = legendText;
    }

    public String getLegendText () {
        return this.legendText;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor () {
        return this.color;
    }

    public void setDataPoints (Collection<DataPoint> dataPoints) {
        this.dataPoints.addAll(dataPoints);
    }

    public  Set<DataPoint> getDataPoints () {
        return this.dataPoints;
    }
}

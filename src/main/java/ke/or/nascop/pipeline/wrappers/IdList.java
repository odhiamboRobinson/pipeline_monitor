package ke.or.nascop.pipeline.wrappers;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 2/21/16.
 */
public class IdList {
    @NotNull
    private List<Integer> ids;

    public IdList() {
        this.ids = new ArrayList<Integer>();
    }

    public void setIds(List<Integer> ids) {
        this.ids.addAll(ids);
    }

    public List<Integer> getIds() {
        return this.ids;
    }
}

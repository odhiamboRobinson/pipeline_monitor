package ke.or.nascop.pipeline.wrappers;

/**
 * Created by robinson on 2/21/16.
 */
/*
 *The SupplyPlanForm wrapper is used to receive input data from
 * forms and apply validation through javax and hibernate
 * validation
 */
public class SupplyPlanForm {
    private Double expectedDeliveries;
    private Double actualConsumption;
    private Double actualClosingStockCentralStores;
    private Double centralIssues;
    private Double actualClosingStockPeripheralStores;

    public void setExpectedDeliveries(Double expectedDeliveries) {
        this.expectedDeliveries = expectedDeliveries;
    }

    public Double getExpectedDeliveries() {
        return this.expectedDeliveries;
    }

    public void setActualConsumption(Double actualConsumption) {
        this.actualConsumption = actualConsumption;
    }

    public Double getActualConsumption() {
        return this.actualConsumption;
    }

    public void setActualClosingStockCentralStores(Double actualClosingStockCentralStores) {
        this.actualClosingStockCentralStores = actualClosingStockCentralStores;
    }

    public Double getActualClosingStockCentralStores() {
        return this.actualClosingStockCentralStores;
    }

    public void setActualClosingStockPeripheralStores(Double actualClosingStockPeripheralStores) {
        this.actualClosingStockPeripheralStores = actualClosingStockPeripheralStores;
    }

    public Double getActualClosingStockPeripheralStores() {
        return this.actualClosingStockPeripheralStores;
    }

    public Double getCentralIssues() {
        return this.centralIssues;
    }

    public void setCentralIssues(Double centralIssues) {
        this.centralIssues = centralIssues;
    }
}

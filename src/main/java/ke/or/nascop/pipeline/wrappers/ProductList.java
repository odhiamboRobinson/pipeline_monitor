package ke.or.nascop.pipeline.wrappers;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 2/21/16.
 */
public class ProductList {
    @NotNull
    private List<ProductForm> productForms;

    public ProductList() {
        this.productForms = new ArrayList<ProductForm>();
    }

    public void setProductForms(List<ProductForm> productForms) {
        this.productForms.addAll(productForms);
    }

    public List<ProductForm> getProductForms() {
        return this.productForms;
    }
}

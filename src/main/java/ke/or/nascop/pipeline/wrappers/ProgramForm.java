package ke.or.nascop.pipeline.wrappers;


import javax.validation.constraints.NotNull;
/**
 * Created by robinson on 2/21/16.
 */
/*
 *The ProgramForm wrapper is used to receive input data from
 * forms and apply validation through javax and hibernate
 * validation
 */
public class ProgramForm {
    @NotNull
    private String name;
    @NotNull
    private Boolean enabled;
    @NotNull
    private Integer minimumMonthsOfStock;
    @NotNull
    private Integer maximumMonthsOfStock;

    public void setName (String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setEnabled (Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled () {
        return this.enabled;
    }

    public void setMinimumMonthsOfStock(Integer minimumMonthsOfStock) {
        this.minimumMonthsOfStock = minimumMonthsOfStock;
    }

    public Integer getMinimumMonthsOfStock() {
        return this.minimumMonthsOfStock;
    }

    public void setMaximumMonthsOfStock(Integer maximumMonthsOfStock) {
        this.maximumMonthsOfStock = maximumMonthsOfStock;
    }

    public Integer getMaximumMonthsOfStock() {
        return this.maximumMonthsOfStock;
    }

}

package ke.or.nascop.pipeline.wrappers;

import javax.validation.constraints.NotNull;

/**
 * Created by robinson on 2/21/16.
 */
/*
 *The ProductForm wrapper is used to receive input data from
 * forms and apply validation through javax and hibernate
 * validation
 */
public class ProductForm {
    @NotNull
    private String name;
    @NotNull
    private String unit;
    @NotNull
    private Double quantity;
    @NotNull
    private Double unitPrice;
    private Integer minimumMonthsOfStock;
    private Integer maximumMonthsOfStock;

    private Boolean enabled;

    public void setName (String name) {
        this.name = name;
    }

    public String getName () {
        return this.name;
    }

    public void setUnit (String unit) {
        this.unit = unit;
    }

    public String getUnit () {
        return this.unit;
    }

    public void setQuantity (Double quantity) {
        this.quantity = quantity;
    }

    public Double getQuantity () {
        return this.quantity;
    }

    public void setUnitPrice (Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getUnitPrice () {
        return this.unitPrice;
    }

    public void setEnabled (Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled () {
        return this.enabled;
    }

    public void setMinimumMonthsOfStock (Integer minimumMonthsOfStock) {
        this.minimumMonthsOfStock = minimumMonthsOfStock;
    }

    public void setMaximumMonthsOfStock (Integer maximumMonthsOfStock) {
        this.maximumMonthsOfStock = maximumMonthsOfStock;
    }

    public Integer getMinimumMonthsOfStock () {
        return this.minimumMonthsOfStock;
    }

    public Integer getMaximumMonthsOfStock () {
        return this.maximumMonthsOfStock;
    }

    @Override
    public boolean equals (Object obj) {
        try {
            ProductForm other = (ProductForm) obj;
            if (other.name.equals(this.name) &&
                    other.unit.equalsIgnoreCase(this.unit) &&
                    other.quantity.equals(this.quantity)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

}

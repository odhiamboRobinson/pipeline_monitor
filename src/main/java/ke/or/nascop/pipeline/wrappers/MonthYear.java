package ke.or.nascop.pipeline.wrappers;

import javax.validation.constraints.NotNull;

/**
 * Created by robinson on 3/6/16.
 */
public class MonthYear implements Comparable<MonthYear> {
    @NotNull
    private Integer month;
    @NotNull
    private Integer year;

    public MonthYear () {}
    public MonthYear(Integer month,Integer year) {
        this.month = month;
        this.year = year;
    }

    public int compareTo (MonthYear monthYear) {
            if(this.month < monthYear.month) {
                return -1;
            } else  if (this.month > monthYear.month) {
                return 1;
            } else {
                if( this.year < monthYear.year) {
                    return -1;
                } else if (this.year > monthYear.year) {
                    return 1;
                } else {
                    return 0;
                }
            }
    }

    public boolean equals (Object obj) {
        try {
            MonthYear other = (MonthYear)obj;
            if(other.month == this.month && other.year == this.year) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "'"+this.month+" "+this.year+"'";
    }
}

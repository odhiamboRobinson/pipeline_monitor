package ke.or.nascop.pipeline.wrappers;

/**
 * Created by robinson on 2/27/16.
 */
public class SupplyPlanOverride {
    private Boolean actualConsumptionOverride;
    private Boolean centralConsumptionOverride;
    private Boolean expectedDeliveriesOverride;
    private Boolean actualClosingStockCentralStoresOverride;
    private Boolean actualClosingStockPeripheralStoresOverride;

    public void setActualConsumptionOverride(Boolean actualConsumptionOverride) {
        this.actualConsumptionOverride = actualConsumptionOverride;
    }

    public Boolean getActualConsumptionOverride() {
        return this.actualConsumptionOverride;
    }

    public void setCentralConsumptionOverride(Boolean centralConsumptionOverride) {
        this.centralConsumptionOverride = centralConsumptionOverride;
    }

    public Boolean getCentralConsumptionOverride() {
        return this.centralConsumptionOverride;
    }

    public void setExpectedDeliveriesOverride(Boolean expectedDeliveriesOverride) {
        this.expectedDeliveriesOverride = expectedDeliveriesOverride;
    }

    public Boolean getExpectedDeliveriesOverride() {
        return this.expectedDeliveriesOverride;
    }

    public void setActualClosingStockCentralStoresOverride(Boolean actualClosingStockCentralStoresOverride) {
        this.actualClosingStockCentralStoresOverride = actualClosingStockCentralStoresOverride;
    }

    public Boolean getActualClosingStockCentralStoresOverride() {
        return this.actualClosingStockCentralStoresOverride;
    }

    public void setActualClosingStockPeripheralStoresOverride(Boolean actualClosingStockPeripheralStoresOverride) {
        this.actualClosingStockPeripheralStoresOverride = actualClosingStockPeripheralStoresOverride;
    }

    public Boolean getActualClosingStockPeripheralStoresOverride() {
        return this.actualClosingStockPeripheralStoresOverride;
    }
}

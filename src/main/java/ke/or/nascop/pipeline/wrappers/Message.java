package ke.or.nascop.pipeline.wrappers;

/**
 * Created by robinson on 3/3/16.
 */
public class Message {
    private String type;
    private String message;
    private String url;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
}

package ke.or.nascop.pipeline.wrappers;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by robinson on 3/6/16.
 */
public class MonthYearList {
    @NotNull
    List<MonthYear> monthYears;

    public MonthYearList () {
        monthYears = new ArrayList<MonthYear>();
    }

    public void setMonthYears (List<MonthYear> monthYears) {
        this.monthYears.addAll(monthYears);
    }

    public List<MonthYear> getMonthYears() {
        return monthYears;
    }
}

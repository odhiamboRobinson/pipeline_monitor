package ke.or.nascop.pipeline.wrappers;

/**
 * Created by robinson on 2/29/16.
 */
public class SupplyPlanProduct {
    private Integer id;
    private String productName;
    private Long stockOut;
    private Long belowMinMOS;
    private Long withinMinAndMaxMOS;
    private Long aboveMaxMOS;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setStockOut(Long stockOut) {
        this.stockOut = stockOut;
    }

    public Long getStockOut() {
        return this.stockOut;
    }

    public void setBelowMinMOS(Long belowMinMOS) {
        this.belowMinMOS = belowMinMOS;
    }

    public Long getBelowMinMOS() {
        return this.belowMinMOS;
    }

    public void setWithinMinAndMaxMOS(Long withinMinAndMaxMOS) {
        this.withinMinAndMaxMOS = withinMinAndMaxMOS;
    }

    public Long getWithinMinAndMaxMOS() {
        return this.withinMinAndMaxMOS;
    }

    public void setAboveMaxMOS(Long aboveMaxMOS) {
        this.aboveMaxMOS = aboveMaxMOS;
    }

    public Long getAboveMaxMOS() {
        return this.aboveMaxMOS;
    }

}

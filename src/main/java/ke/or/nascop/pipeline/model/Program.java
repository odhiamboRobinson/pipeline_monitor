package ke.or.nascop.pipeline.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by robinson on 2/20/16.
 */

/*
  *The program entity is used to store various programs
  * that are being tracked by the pipeline. It has a one
  * to one mapping with an underlying database table called
  * programs
 */

@Entity
@Table(name="programs")
public class Program extends Model {
    private Integer id;
    private String name;
    private Integer minimumMonthsOfStock;
    private Integer maximumMonthsOfStock;
    private Integer startingMonth;
    private Integer startingYear;


    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public Integer getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }


    public void setMinimumMonthsOfStock(Integer minimumMonthsOfStock) {
        this.minimumMonthsOfStock = minimumMonthsOfStock;
    }

    public void setMaximumMonthsOfStock(Integer maximumMonthsOfStock) {
        this.maximumMonthsOfStock = maximumMonthsOfStock;
    }

    @Column(name="maximum_months_of_stock")
    public Integer getMaximumMonthsOfStock() {
        return this.maximumMonthsOfStock;
    }

    @Column(name="minimum_months_of_stock")
    public Integer getMinimumMonthsOfStock() {
        return this.minimumMonthsOfStock;
    }

    public void setStartingMonth(Integer startingMonth) {
        this.startingMonth = startingMonth;
    }

    @Column(name="starting_month")
    public Integer getStartingMonth() {
        return this.startingMonth;
    }

    public void setStartingYear(Integer startingYear) {
        this.startingYear = startingYear;
    }

    @Column(name="starting_year")
    public Integer getStartingYear() {
        return this.startingYear;
    }


    @Override
    public boolean equals (Object obj) {
        try {
            if(obj instanceof Program) {
                Program other = (Program) obj;
                if (other.name.equals(this.name)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    @Override
    public String toString() {
        String string;
        string = "PROGRAM :- program id : "+this.id+" name : "+this.name+" minimum months of stock : "+
                this.minimumMonthsOfStock+" maximum months of stock : "+this.maximumMonthsOfStock+" starting month : "+this.startingMonth+" starting year : "+this.startingYear+super.toString();
        return string;
    }

}

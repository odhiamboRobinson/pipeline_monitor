package ke.or.nascop.pipeline.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by tovo on 2/7/16.
 */
/*
 *The user class has a one to one mapping with the underlying mysql
 * database (a model in the database)
 */
@Entity
@Table(name="users")
public class User extends Model {
    private Integer id;
    private String sirName;
    private String otherNames;
    private String email;
    private String phoneNumber;
    private String idNumber;
    @JsonIgnore
    private String password;
    private Set<UserGroup> userGroups;
    private Set<Program> programs;

    public User () {
        userGroups = new HashSet<UserGroup>();
        programs = new HashSet<Program>();
    }

    public void setId (Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public Integer getId () {
        return this.id;
    }

    public void setSirName (String sirName) {
        this.sirName = sirName;
    }

    @Column(nullable =false)
    public String getSirName () {
        return this.sirName;
    }

    public void setOtherNames (String otherNames) {
        this.otherNames = otherNames;
    }

    @Column(nullable = false)
    public String getOtherNames () {
        return this.otherNames;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    @Column(nullable = false)
    public String getEmail () {
        return this.email;
    }

    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(nullable = false)
    public String getPhoneNumber () {
        return this.phoneNumber;
    }


    public void setIdNumber (String idNumber) {
        this.idNumber = idNumber;
    }

    @Column(nullable = false)
    public String getIdNumber () {
        return this.idNumber;
    }

    public void setPassword (String password) {
       this.password = password;
    }

    @Column(nullable = false)
    public String getPassword () {
        return this.password;
    }

    public void setUserGroups (Set<UserGroup> userGroups) {
        this.userGroups.addAll(userGroups);
    }
    @ManyToMany
    @JoinTable(name="group_members",joinColumns = @JoinColumn(name="user_id"),inverseJoinColumns = @JoinColumn(name="group_id"))
    public Set<UserGroup> getUserGroups () {
        return userGroups;
    }

    public void setPrograms (Set<Program> programs) {
        this.programs.addAll(programs);
    }
    @ManyToMany
    @JoinTable(name="user_programs",joinColumns = @JoinColumn(name="user_id"),inverseJoinColumns = @JoinColumn(name="program_id"))
    public Set<Program> getPrograms () {
        return programs;
    }


    @Override
    public boolean equals (Object obj) {
        try {
            User other = (User) obj;
            if (other.email.equalsIgnoreCase(this.email) ||
                other.idNumber.equals(this.otherNames)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String toString() {
        String string;
        string = "USER :- user id : "+this.id+" surname : "+this.sirName+" other names : "+this.otherNames+" E-mail : "+
                this.email+" phone number : "+this.phoneNumber+" id number (national id/passport no) : "+this.idNumber+super.toString();
        return string;
    }
}

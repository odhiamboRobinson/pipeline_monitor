package ke.or.nascop.pipeline.model;

import javax.persistence.*;

/**
 * Created by robinson on 2/20/16.
 */

/*
 *The SupplyPlan entity is used to store supply plan related information
 * such as the actual consumption etc. It has a one to one
 * correspondence with the supply_plans table in the underlying
 * database
 */

@Entity
@Table(name="supply_plan")
public class SupplyPlan extends Model implements Comparable<SupplyPlan> {
    private Integer id;
    private Double forecastConsumption;
    private Double forecastFactor;
    private Double initialStockOnHandNationalStores;
    private Double initialStockOnHandSubNationalStores;
    private Integer month;
    private Integer year;
    private Product product;
    private SupplyPlan next;
    private SupplyPlan prev;
    private Double openingStockBalance;
    private Double centralIssues;
    private Double expectedDeliveries;
    private Double actualConsumption;
    private Double desiredAdditionalQuantity;
    private Double calculatedClosingStock;
    private Double actualClosingStockCentralStores;
    private Double actualClosingStockPeripheralStores;
    private Double totalActualClosingStock;
    private Double monthsOfStockCentralStores;
    private Double monthsOfStockPeripheralStores;
    private Double totalMonthsOfStock;
    private Boolean actualConsumptionOverride = false;
    private Boolean centralConsumptionOverride = false;
    private Boolean expectedDeliveriesOverride = false;
    private Boolean actualClosingStockCentralStoresOverride = false;
    private Boolean actualClosingStockPeripheralStoresOverride = false;
    private Double maximumStockQuantity;
    private Double minimumStockQuantity;


    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public Integer getId() {
        return this.id;
    }

    public void setForecastConsumption(Double forecastConsumption) {
        this.forecastConsumption = forecastConsumption;
    }

    public Double getForecastConsumption() {
        return this.forecastConsumption;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getMonth() {
        return this.month;
    }

    public void setYear(Integer year) {
       this.year = year;
    }

    public Integer getYear() {
        return this.year;
    }

    public void setNext(SupplyPlan next) {
        this.next = next;
    }

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="next_id")
    public SupplyPlan getNext() {
        return this.next;
    }

    @OneToOne
    @JoinColumn(name="prev_id")
    public SupplyPlan getPrev() {
        return this.prev;
    }

    public void setPrev(SupplyPlan prev) {
        this.prev = prev;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @ManyToOne
    @JoinColumn(name="product_id")
    public Product getProduct() {
        return this.product;
    }

    public void setCentralIssues(Double centralIssues) {
        this.centralIssues = centralIssues;
    }

    public Double getCentralIssues() {
        return this.centralIssues;
    }

    public void setExpectedDeliveries(Double expectedDeliveries) {
        this.expectedDeliveries = expectedDeliveries;
    }

    @Column(name="opening_stock_balance")
    public Double getOpeningStockBalance() {
        return this.openingStockBalance;
    }

    public void setOpeningStockBalance(Double openingStockBalance) {
        this.openingStockBalance = openingStockBalance;
    }

    @Column(name="expected_deliveries")
    public Double getExpectedDeliveries() {
        return this.expectedDeliveries;
    }

    public void setActualConsumption(Double actualConsumption) {
        this.actualConsumption = actualConsumption;
    }

    @Column(name="actual_consumption")
    public Double getActualConsumption() {
        return this.actualConsumption;
    }

    @Column(name="desired_additional_quantity")
    public Double getDesiredAdditionalQuantity() {
        return this.desiredAdditionalQuantity;
    }

    public void setDesiredAdditionalQuantity(Double desiredAdditionalQuantity) {
        this.desiredAdditionalQuantity = desiredAdditionalQuantity;
    }

    @Column(name="calculated_closing_stock")
    public Double getCalculatedClosingStock() {
        return this.calculatedClosingStock;
    }

    public void setCalculatedClosingStock(Double calculatedClosingStock) {
        this.calculatedClosingStock = calculatedClosingStock;
    }

    @Column(name="total_actual_closing_stock")
    public Double getTotalActualClosingStock() {
        return this.totalActualClosingStock;
    }

    public void setTotalActualClosingStock(Double totalActualClosingStock) {
        this.totalActualClosingStock = totalActualClosingStock;
    }

    public void setActualClosingStockCentralStores(Double actualClosingStockCentralStores) {
        this.actualClosingStockCentralStores = actualClosingStockCentralStores;
    }

    @Column(name="actual_closing_stock_central_stores")
    public Double getActualClosingStockCentralStores() {
        return this.actualClosingStockCentralStores;
    }

    public void setActualClosingStockPeripheralStores(Double actualClosingStockPeripheralStores) {
        this.actualClosingStockPeripheralStores = actualClosingStockPeripheralStores;
    }

    public Double getActualClosingStockPeripheralStores() {
        return this.actualClosingStockPeripheralStores;
    }


    @Column(name="months_of_stock_central_stores")
    public Double getMonthsOfStockCentralStores() {
        return this.monthsOfStockCentralStores;
    }

    public void setMonthsOfStockCentralStores(Double monthsOfStockCentralStores) {
        this.monthsOfStockCentralStores = monthsOfStockCentralStores;
    }

    @Column(name="months_of_stock_peripheral_stores")
    public Double getMonthsOfStockPeripheralStores() {
        return this.monthsOfStockPeripheralStores;
    }

    public void setMonthsOfStockPeripheralStores(Double monthsOfStockPeripheralStores) {
        this.monthsOfStockPeripheralStores = monthsOfStockPeripheralStores;
    }

    @Column(name="total_months_of_stock")
    public Double getTotalMonthsOfStock() {
        return this.totalMonthsOfStock;
    }

    public void setTotalMonthsOfStock(Double totalMonthsOfStock) {
        this.totalMonthsOfStock = totalMonthsOfStock;
    }

    public void setActualConsumptionOverride(Boolean actualConsumptionOverride) {
        this.actualConsumptionOverride = actualConsumptionOverride;
    }

    @Column(name="actual_consumption_override")
    public Boolean getActualConsumptionOverride() {
        return this.actualConsumptionOverride;
    }

    public void setCentralConsumptionOverride(Boolean centralConsumptionOverride) {
        this.centralConsumptionOverride = centralConsumptionOverride;
    }

    @Column(name="central_consumption_override")
    public Boolean getCentralConsumptionOverride() {
        return this.centralConsumptionOverride;
    }

    public void setExpectedDeliveriesOverride(Boolean expectedDeliveriesOverride) {
        this.expectedDeliveriesOverride = expectedDeliveriesOverride;
    }

    @Column(name="expected_deliveries_override")
    public Boolean getExpectedDeliveriesOverride() {
        return this.expectedDeliveriesOverride;
    }

    public void setActualClosingStockCentralStoresOverride(Boolean actualClosingStockCentralStoresOverride) {
        this.actualClosingStockCentralStoresOverride = actualClosingStockCentralStoresOverride;
    }

    @Column(name="actual_closing_stock_central_stores_override")
    public Boolean getActualClosingStockCentralStoresOverride() {
        return this.actualClosingStockCentralStoresOverride;
    }

    public void setActualClosingStockPeripheralStoresOverride(Boolean actualClosingStockPeripheralStoresOverride) {
        this.actualClosingStockPeripheralStoresOverride = actualClosingStockPeripheralStoresOverride;
    }

    @Column(name="actual_closing_stock_peripheral_stores_override")
    public Boolean getActualClosingStockPeripheralStoresOverride() {
        return this.actualClosingStockPeripheralStoresOverride;
    }

    public void setMaximumStockQuantity(Double maximumStockQuantity) {
        this.maximumStockQuantity = maximumStockQuantity;
    }

    @Column(name="maximum_stock_quantity")
    public Double getMaximumStockQuantity() {
        return this.maximumStockQuantity;
    }

    public void setMinimumStockQuantity(Double minimumStockQuantity) {
        this.minimumStockQuantity = minimumStockQuantity;
    }

    @Column(name="minimum_stock_quantity")
    public Double getMinimumStockQuantity() {
        return this.minimumStockQuantity;
    }

    public void setForecastFactor(Double forecastFactor) {
        this.forecastFactor = forecastFactor;
    }

    @Column(name="forecast_factor")
    public Double getForecastFactor() {
        return this.forecastFactor;
    }

    public void setInitialStockOnHandNationalStores(Double initialStockOnHandNationalStores) {
        this.initialStockOnHandNationalStores = initialStockOnHandNationalStores;
    }

    @Column(name="initial_stock_on_hand_national_stores")
    public Double getInitialStockOnHandNationalStores() {
        return this.initialStockOnHandNationalStores;
    }

    public void setInitialStockOnHandSubNationalStores(Double initialStockOnHandSubNationalStores) {
        this.initialStockOnHandSubNationalStores = initialStockOnHandSubNationalStores;
    }

    @Column(name="initial_stock_on_hand_sub_national_stores")
    public Double getInitialStockOnHandSubNationalStores() {
        return this.initialStockOnHandSubNationalStores;
    }


    @Override
    public boolean equals (Object obj) {
        try {
            SupplyPlan other = (SupplyPlan) obj;
            if (other.month.equals(this.month) && other.year.equals(this.year)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int compareTo (SupplyPlan other) {
        int comparison = other.year.compareTo(this.year);

        if( comparison != 0) {
            return comparison;
        } else {
            return other.month.compareTo(this.month);
        }
    }

    @Override
    public String toString() {
        String string;
        string = "STOCK :- stock id : "+this.id+" expected deliveries : "+this.expectedDeliveries+" actual consumption : "+
                this.actualConsumption+" actual closing stock central stores : "+this.actualClosingStockCentralStores +
                " :actual closing stock peripheral stores : "+this.actualClosingStockPeripheralStores+super.toString();
        return string;
    }
}

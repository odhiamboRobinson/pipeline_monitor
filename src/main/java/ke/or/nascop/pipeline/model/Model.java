package ke.or.nascop.pipeline.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by robinson on 2/20/16.
 */
/*
 *The model class is used to give common functionality
 *to other model classes
*/

@MappedSuperclass
public class Model {
    private Integer updatedBy;
    private Timestamp updatedOn;
    private Integer createdBy;
    private Timestamp createdOn;
    private Integer deletedBy;
    private Timestamp deletedOn;
    private Boolean enabled;


    public void setCreatedBy (Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name="created_by",nullable = false,updatable = false)
    public Integer getCreatedBy() {
        return this.createdBy;
    }


    public void setCreatedOn (Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Column(name="created_on",nullable = false,updatable = false)
    public Timestamp getCreatedOn() {
        return this.createdOn;
    }


    public void setDeletedBy (Integer deletedBy) {
        this.deletedBy = deletedBy;
    }

    @Column(name="deleted_by")
    public Integer getDeletedBy() {
        return this.deletedBy;
    }


    public void setDeletedOn (Timestamp deletedOn) {
        this.deletedOn = deletedOn;
    }

    @Column(name="deleted_on")
    public Timestamp getDeletedOn() {
        return this.deletedOn;
    }

    public void setUpdatedBy (Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name="updated_by")
    public Integer getUpdatedBy() {
        return this.updatedBy;
    }


    public void setUpdatedOn (Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Column(name="updated_on")
    public Timestamp getUpdatedOn() {
        return this.updatedOn;
    }

    public void setEnabled (Boolean enabled) {
        this.enabled = enabled;
    }

    @Column(nullable = false)
    public Boolean getEnabled() {
        return this.enabled;
    }

    @Override
    public String toString() {
        String string;
        String updatedBy,updatedOn,deletedBy,deletedOn;

        if(this.updatedBy == null){
            updatedBy = "N/A";
        } else {
            updatedBy = this.updatedBy+"";
        }

        if(this.updatedOn == null){
            updatedOn = "N/A";
        } else {
            updatedOn = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(this.updatedOn);
        }

        if(this.deletedBy == null){
            deletedBy = "N/A";
        } else {
            deletedBy = this.deletedBy+"";
        }

        if(this.deletedOn == null){
            deletedOn = "N/A";
        } else {
            deletedOn= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(this.deletedOn);
        }

        string = " created by : "+this.createdBy+" createdOn : "+ new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(createdOn)+
                 " updated by : "+updatedBy+" updatedOn : "+ updatedOn +
                 " deleted by : "+deletedBy+" deletedOn : "+ deletedOn;
        return string;
    }
}

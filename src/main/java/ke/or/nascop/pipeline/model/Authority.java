package ke.or.nascop.pipeline.model;


import javax.persistence.*;

/**
 * Created by tovo on 2/7/16.
 */
/*
 *The authority class has a one to one mapping with the underlying mysql
 * database (a model in the database)
 */
@Entity
@Table(name="authorities")
public class Authority extends Model {
    private Integer id;
    private String authority;
    private String description;
    private String category;

    public void setId (Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public Integer getId () {
        return this.id;
    }

    public void setAuthority (String authority) {
        this.authority = authority;
    }

    public String getAuthority () {
        return this.authority;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getDescription () {
        return this.description;
    }

    public void setCategory (String category) {
        this.category = category;
    }

    public String getCategory () {
        return this.category;
    }


    @Override
    public boolean equals (Object obj) {
        try {
            Authority other = (Authority) obj;
            if (other.authority.equals(this.authority)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String toString() {
        String string;
        string = "AUTHORITY :- authority id : "+this.id+" authority : "+this.authority+super.toString();
        return string;
    }
}

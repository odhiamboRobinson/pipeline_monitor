package ke.or.nascop.pipeline.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by robinson on 2/28/16.
 */
@Entity
@Table(name="forecasts")
public class Forecast extends Model {
    private Integer id;
    private Integer startingMonth;
    private Integer startingYear;
    private Integer endingMonth;
    private Integer endingYear;
    private Boolean current = false;
    private Set<SupplyPlan> supplyPlans;

    public Forecast() {
        supplyPlans = new HashSet<SupplyPlan>();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public Integer getId() {
        return this.id;
    }

    public void setStartingMonth(Integer startingMonth) {
        this.startingMonth = startingMonth;
    }

    @Column(name="starting_month")
    public Integer getStartingMonth() {
        return this.startingMonth;
    }

    public void setStartingYear(Integer startingYear) {
        this.startingYear = startingYear;
    }

    @Column(name="starting_year")
    public Integer getStartingYear() {
        return this.startingYear;
    }

    public void setEndingMonth(Integer endingMonth) {
        this.endingMonth = endingMonth;
    }

    @Column(name="ending_month")
    public Integer getEndingMonth() {
        return this.endingMonth;
    }

    public void setEndingYear(Integer endingYear) {
        this.endingYear = endingYear;
    }

    public void setCurrent(Boolean current) {
        if(current != null) {
            this.current = current;
        }
    }

    public Boolean getCurrent() {
        return this.current;
    }

    @Column(name="ending_year")
    public Integer getEndingYear() {
        return this.endingYear;
    }

    public void setSupplyPlans (Set<SupplyPlan> supplyPlans) {
        this.supplyPlans.addAll(supplyPlans);
    }

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name="forecast_id")
    public Set<SupplyPlan> getSupplyPlans() {
        return this.supplyPlans;
    }
}

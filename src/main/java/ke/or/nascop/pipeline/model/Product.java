package ke.or.nascop.pipeline.model;

/**
 * Created by robinson on 2/20/16.
 */

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/*
 *The OrganisationUnit class has a one to one mapping with the underlying mysql
 * database (a model in the database)
 */

@Entity
@Table(name="products")
public class Product extends Model {
    private Integer id;
    private String name;
    private String unit;
    private Double quantity;
    private Double unitPrice;
    private Integer minimumMonthsOfStock;
    private Integer maximumMonthsOfStock;
    private Boolean productMonthsOfStockOverride = false;
    private Program program;

    public void setId (Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public Integer getId () {
        return this.id;
    }

    public void setName (String name) {
        this.name = name;
    }

    @Column(nullable = false)
    public String getName () {
        return this.name;
    }

    public void setUnit (String unit) {
        this.unit = unit;
    }

    public String getUnit () {
        return this.unit;
    }

    public void setQuantity (Double quantity) {
        this.quantity = quantity;
    }

    @Column(nullable = false)
    public Double getQuantity () {
        return this.quantity;
    }

    public void setUnitPrice (double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Column(name="unit_price",nullable = false)
    public Double getUnitPrice () {
        return this.unitPrice;
    }

    public void setMinimumMonthsOfStock(Integer minimumMonthsOfStock) {
        this.minimumMonthsOfStock = minimumMonthsOfStock;
    }

    public void setMaximumMonthsOfStock(Integer maximumMonthsOfStock) {
        this.maximumMonthsOfStock = maximumMonthsOfStock;
    }

    @Column(name="maximum_months_of_stock")
    public Integer getMaximumMonthsOfStock() {
        return this.maximumMonthsOfStock;
    }

    @Column(name="minimum_months_of_stock")
    public Integer getMinimumMonthsOfStock() {
        return this.minimumMonthsOfStock;
    }

    public void setProgram (Program program) {
        this.program = program;
    }

    @ManyToOne
    @JoinColumn(name="program_id")
    public Program getProgram () {
        return this.program;
    }

    public void setProductMonthsOfStockOverride(Boolean productMonthsOfStockOverride) {
        this.productMonthsOfStockOverride = productMonthsOfStockOverride;
    }

    public Boolean getProductMonthsOfStockOverride() {
        return this.productMonthsOfStockOverride;
    }


    @Override
    public boolean equals (Object obj) {
        try {
            Product other = (Product) obj;
            if (other.name.equals(this.name) &&
                other.unit.equalsIgnoreCase(this.unit) &&
                other.quantity.equals(this.quantity)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    @Override
    public String toString() {
        String string;
        string = "PRODUCT :- product id : "+this.id+" name : "+this.name+" unit : "+this.unit+" quantity : "+this.quantity+
                " unit price : "+this.unitPrice+super.toString();
        return string;
    }
}

package ke.or.nascop.pipeline.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by tovo on 2/7/16.
 */
/*
 *The user group class has a one to one mapping with the underlying mysql
 * database (a model in the database)
 */
@Entity
@Table(name="groups")
public class UserGroup extends Model {
    private Integer id;
    private String name;
    private Set<Authority> authorities;
    public  UserGroup () {
        authorities = new HashSet<Authority>();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public Integer getId () {
        return this.id;
    }

    public void setName (String name) {
        this.name = name;
    }

    @Column(name = "group_name",nullable = false)
    public String getName () {
        return this.name;
    }


    public void setAuthorities (Set<Authority> authorities) {
        this.authorities.addAll(authorities) ;
    }

    @ManyToMany
    @JoinTable(name="group_authorities",joinColumns=@JoinColumn(name="group_id"),inverseJoinColumns = @JoinColumn(name="authority_id"))
    public Set<Authority> getAuthorities () {
        return authorities;
    }

    @Override
    public boolean equals (Object obj) {
        try {
            UserGroup other = (UserGroup) obj;
            if(other.name.equalsIgnoreCase(this.name) &&
               other.authorities.containsAll(this.authorities)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String toString() {
        String string;
        string = "USER GROUP :- user group id : "+this.id+" name : "+this.name+super.toString();
        return string;
    }
}

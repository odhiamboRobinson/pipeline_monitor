package ke.or.nascop.pipeline.util;

import ke.or.nascop.pipeline.model.User;
import ke.or.nascop.pipeline.repository.UserDao;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by robinson on 2/21/16.
 */
/*
 *The CurrentUser utility class is used to get the logged in user credentials
* */
public class CurrentUser {

    public User getUser(UserDao userDao) throws UserNotFoundException {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication auth = context.getAuthentication();
        Object obj  =  auth.getPrincipal();
        UserDetails userDetails = null;

        if(obj instanceof UserDetails) {
            userDetails = (UserDetails) obj;
        } else {
            throw new UserNotFoundException("Could not access the logged in user");
        }

        return userDao.getByEmail(userDetails.getUsername());
    }
}

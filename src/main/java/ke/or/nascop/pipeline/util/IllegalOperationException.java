package ke.or.nascop.pipeline.util;

/**
 * Created by robinson on 2/8/16.
 */
public class IllegalOperationException extends RestException{
    public IllegalOperationException (String message) {
        super(100,"Error",message);
    }
}

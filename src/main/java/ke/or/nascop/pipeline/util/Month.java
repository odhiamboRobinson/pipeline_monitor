package ke.or.nascop.pipeline.util;

/**
 * Created by robinson on 2/29/16.
 */
public class Month {
    private Integer index;
    private String name;

    public Month (Integer index,String name) {
        this.index = index;
        this.name = name;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return this.index;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}

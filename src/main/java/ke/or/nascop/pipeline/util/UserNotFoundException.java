package ke.or.nascop.pipeline.util;

/**
 * Created by robinson on 2/7/16.
 */
public class UserNotFoundException extends RestException {
    public UserNotFoundException (String message) {
        super(100,message,"Error");
    }
}

package ke.or.nascop.pipeline.util;

/**
 * Created by robinson on 2/16/16.
 */
public class UserPasswordException extends RestException {
    public UserPasswordException(String message) {
        super(100,message,"Error");
    }
}

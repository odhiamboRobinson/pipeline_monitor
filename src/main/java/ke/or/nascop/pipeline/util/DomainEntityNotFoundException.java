package ke.or.nascop.pipeline.util;

/**
 * Created by robinson on 2/8/16.
 */
public class DomainEntityNotFoundException extends RestException {
    public DomainEntityNotFoundException (String message) {
        super(100,"Error",message);
    }
}

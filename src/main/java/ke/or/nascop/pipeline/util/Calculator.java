package ke.or.nascop.pipeline.util;

import ke.or.nascop.pipeline.model.Product;
import ke.or.nascop.pipeline.model.Program;
import ke.or.nascop.pipeline.model.SupplyPlan;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by robinson on 2/28/16.
 */
public class Calculator {
    //make calculator class static by privatising its default constructor
    private Calculator() {}

    public static List<SupplyPlan> calculate (SupplyPlan supplyPlan) {
        int MAX_SIZE = 2;
        ArrayList<SupplyPlan> supplyPlans = new ArrayList<SupplyPlan>();
        SupplyPlan supplyPlanPtr = supplyPlan;
        Double openingStockBalance;
        if(supplyPlan.getInitialStockOnHandSubNationalStores() != null && supplyPlan.getInitialStockOnHandNationalStores() != null) {
            openingStockBalance = supplyPlan.getInitialStockOnHandNationalStores()+supplyPlan.getInitialStockOnHandSubNationalStores();
        } else {
           openingStockBalance = supplyPlan.getOpeningStockBalance();
        }


        while(supplyPlanPtr != null) {
            Double actualConsumption = supplyPlanPtr.getActualConsumption();
            Double centralIssues = supplyPlanPtr.getCentralIssues();
            Double forecastConsumption = supplyPlanPtr.getForecastConsumption();
            Boolean isActualConsumptionOverride = supplyPlanPtr.getActualConsumptionOverride();
            Boolean isCentralConsumptionOverride = supplyPlanPtr.getCentralConsumptionOverride();
            Double averageConsumption,sum = 0.0;
            int count = 0,size = 1;
            supplyPlanPtr.setOpeningStockBalance(openingStockBalance);

            //Determine the consumption value to use
            SupplyPlan supplyPlanPrevPtr = supplyPlanPtr.getPrev();
            if(actualConsumption != null && !isActualConsumptionOverride) {
                sum+=actualConsumption;
                while(count < MAX_SIZE && supplyPlanPrevPtr != null) {
                    if( supplyPlanPrevPtr.getActualConsumption() != null) {
                        sum += supplyPlanPrevPtr.getActualConsumption();
                        size++;
                    }
                    supplyPlanPrevPtr = supplyPlanPrevPtr.getPrev();
                    count++;
                }
            } else if (centralIssues != null && !isCentralConsumptionOverride) {
                sum+=centralIssues;
                while(count < MAX_SIZE && supplyPlanPrevPtr != null) {
                    if( supplyPlanPrevPtr.getCentralIssues() != null) {
                        sum += supplyPlanPrevPtr.getCentralIssues();
                        size++;
                    }
                    supplyPlanPrevPtr = supplyPlanPrevPtr.getPrev();
                    count++;
                }
            } else {
                sum+=forecastConsumption;
                while(count < MAX_SIZE && supplyPlanPrevPtr != null) {
                    if( supplyPlanPrevPtr.getForecastConsumption() != null) {
                        sum += supplyPlanPrevPtr.getForecastConsumption();
                        size++;
                    }
                    supplyPlanPrevPtr = supplyPlanPrevPtr.getPrev();
                    count++;
                }
            }

            averageConsumption = sum/size;
            Product product = supplyPlan.getProduct();
            Integer productMinimumMonthsOfStock = product.getMinimumMonthsOfStock();
            Integer productMaximumMonthsOfStock = product.getMaximumMonthsOfStock();
            Boolean isProductMonthsOfStockOverride = product.getProductMonthsOfStockOverride();
            Program program = product.getProgram();
            Integer programMaximumMonthsOfStock = program.getMaximumMonthsOfStock();
            Integer programMinimumMonthsOfStock = program.getMinimumMonthsOfStock();
            Integer minimumMonthsOfStock,maximumMonthsOfStock;


            if(productMinimumMonthsOfStock != null && !isProductMonthsOfStockOverride) {
                minimumMonthsOfStock = productMinimumMonthsOfStock;
            } else {
                minimumMonthsOfStock = programMinimumMonthsOfStock;
            }

            if(productMaximumMonthsOfStock != null && !isProductMonthsOfStockOverride) {
                maximumMonthsOfStock = productMaximumMonthsOfStock;
            } else {
                maximumMonthsOfStock =  programMaximumMonthsOfStock;
            }

            //calculate maximum stock quantity
            supplyPlanPtr.setMaximumStockQuantity(maximumMonthsOfStock*averageConsumption);
            supplyPlanPtr.setMinimumStockQuantity(minimumMonthsOfStock * averageConsumption);

            Double expectedDeliveries = supplyPlanPtr.getExpectedDeliveries();
            Boolean isExpectedDeliveriesOverride = supplyPlanPtr.getExpectedDeliveriesOverride();
            Double calculatedClosingStock;

            //calculate calculated closing stock
            if(expectedDeliveries != null && !isExpectedDeliveriesOverride) {
                if(actualConsumption != null) {
                    calculatedClosingStock = openingStockBalance+expectedDeliveries-actualConsumption;
                } else {
                    calculatedClosingStock = openingStockBalance+expectedDeliveries-supplyPlanPtr.getForecastConsumption();
                }
            } else {
                if(actualConsumption != null) {
                    calculatedClosingStock = openingStockBalance-actualConsumption;
                } else {
                    calculatedClosingStock = openingStockBalance-supplyPlanPtr.getForecastConsumption();
                }
            }

            if(calculatedClosingStock < 0) {calculatedClosingStock = 0.0;}
            supplyPlanPtr.setCalculatedClosingStock(calculatedClosingStock);

            //calculate desired additional quantity
            Double desiredAdditionalQuantity = supplyPlanPtr.getMaximumStockQuantity() - supplyPlanPtr.getCalculatedClosingStock();
            if(desiredAdditionalQuantity < 0) {desiredAdditionalQuantity = 0.0;}
            supplyPlanPtr.setDesiredAdditionalQuantity(desiredAdditionalQuantity);

            Double actualClosingStock = null;
            Double actualClosingStockPeripheralStores = supplyPlanPtr.getActualClosingStockPeripheralStores();
            Double actualClosingStockCentralStores = supplyPlanPtr.getActualClosingStockCentralStores();
            Boolean isActualClosingStockCentralStoresOverride = supplyPlanPtr.getActualClosingStockCentralStoresOverride();
            Boolean isActualClosingStockPeripheralStoresOverride = supplyPlanPtr.getActualClosingStockPeripheralStoresOverride();
            Double totalMonthsOfStock = null;

            //calculate actual closing stock
            if(actualClosingStockCentralStores != null && !isActualClosingStockCentralStoresOverride && actualClosingStockPeripheralStores != null && !isActualClosingStockPeripheralStoresOverride) {
                actualClosingStock = actualClosingStockCentralStores + actualClosingStockPeripheralStores;
                totalMonthsOfStock =actualClosingStock/averageConsumption;
            } else if (actualClosingStockCentralStores != null && !isActualClosingStockCentralStoresOverride){
                actualClosingStock = actualClosingStockCentralStores;
                totalMonthsOfStock =actualClosingStock/averageConsumption;
            } else if (actualClosingStockPeripheralStores != null && !isActualClosingStockPeripheralStoresOverride) {
                actualClosingStock = actualClosingStockPeripheralStores;
                totalMonthsOfStock =actualClosingStock/averageConsumption;
            }

            //calculate months of stock central stocks
            if(actualClosingStockCentralStores != null && !isActualClosingStockCentralStoresOverride) {
                supplyPlanPtr.setMonthsOfStockCentralStores(actualClosingStockCentralStores/averageConsumption);
            } else {
                supplyPlanPtr.setMonthsOfStockCentralStores(null);
            }

            //calculate months of stock peripheral stock
            if(actualClosingStockPeripheralStores != null && !isActualClosingStockPeripheralStoresOverride) {
                supplyPlanPtr.setMonthsOfStockPeripheralStores(actualClosingStockPeripheralStores / averageConsumption);
            } else {
                supplyPlanPtr.setMonthsOfStockPeripheralStores(null);
            }

            // calculate total months of stock
            if(totalMonthsOfStock == null) {
                totalMonthsOfStock = calculatedClosingStock/averageConsumption;
            }

            if(totalMonthsOfStock < 0 ){totalMonthsOfStock = 0.0;}

            supplyPlanPtr.setTotalMonthsOfStock(totalMonthsOfStock);
            supplyPlanPtr.setTotalActualClosingStock(actualClosingStock);

            if(actualClosingStock != null) {
                openingStockBalance = actualClosingStock;
            } else {
                openingStockBalance = calculatedClosingStock;
            }
            supplyPlans.add(supplyPlanPtr);
            supplyPlanPtr = supplyPlanPtr.getNext();
        }

        return supplyPlans;
    }
}

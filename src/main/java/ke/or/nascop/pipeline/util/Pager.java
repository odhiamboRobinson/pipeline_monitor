package ke.or.nascop.pipeline.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by robinson on 2/8/16.
 */
public class Pager {
    private static final int PAGE_CHUNKS = 20;
    public static List<Object> createPaging(HashMap hashMap ,int page) {
        List list = (List) hashMap.values();
        int totalPages = (int)Math.ceil(list.size()/PAGE_CHUNKS);

        if(page > totalPages) {
            page = totalPages;
        }

        Integer lastRecord = page * PAGE_CHUNKS;
        Integer record = lastRecord - PAGE_CHUNKS;

        if(lastRecord > list.size()) {
            lastRecord = list.size();
        }

        if(record < 0) {
            record = 0;
        }

        return list.subList(record,lastRecord);
    }
}

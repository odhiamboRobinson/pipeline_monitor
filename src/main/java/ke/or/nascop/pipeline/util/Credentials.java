package ke.or.nascop.pipeline.util;

import ke.or.nascop.pipeline.model.Forecast;
import ke.or.nascop.pipeline.model.Program;

/**
 * Created by robinson on 3/1/16.
 */
public class Credentials {
    private Program program;
    private Forecast forecast;

    public void setProgram(Program program) {
        this.program = program;
    }

    public Program getProgram () {
        return this.program;
    }

    public void setForecast (Forecast forecast) {
        this.forecast = forecast;
    }

    public Forecast getForecast () {
        return this.forecast;
    }
}

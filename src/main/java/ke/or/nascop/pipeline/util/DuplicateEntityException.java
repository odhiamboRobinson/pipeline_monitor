package ke.or.nascop.pipeline.util;

/**
 * Created by robinson on 2/7/16.
 */
public class DuplicateEntityException extends RestException {

    public DuplicateEntityException (String message) {
        super(100,"Error",message);
    }
}

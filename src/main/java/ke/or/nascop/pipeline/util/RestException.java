package ke.or.nascop.pipeline.util;

/**
 * Created by robinson on 2/3/16.
 */
public class RestException extends RuntimeException {
    private int code;
    private String message;
    private String detailedMessage;

    public RestException(int code, String message, String detailedMessage) {
        this.code = code;
        this.message = message;
        this.detailedMessage = detailedMessage;
    }

    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public String getDetailedMessage() {
        return this.detailedMessage;
    }
}
